package com.makesure

import androidx.lifecycle.SavedStateHandle
import com.makesure.presentation.screens.register.RegisterViewModel
import com.makesure.presentation.screens.register.RegisterViewModel.RegisterState
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class RegisterViewModelTests {
    private lateinit var sut: RegisterViewModel
    private lateinit var savedStateHandle: SavedStateHandle

    @Before
    fun `set up`() {
        savedStateHandle = SavedStateHandle()
        sut = RegisterViewModel(savedStateHandle)
    }

    @Test
    fun `init is correct`() {
        assertEquals(RegisterState.NUMBER, sut.uiState.state)
    }

    @Test
    fun `change state to code enter`() {
        sut.phoneEntered()
        assertEquals(RegisterState.CODE, sut.uiState.state)
    }

    @Test
    fun `code entered state`() {
        sut.codeEntered()
        assertEquals(RegisterState.EMAIL, sut.uiState.state)
    }

    @Test
    fun `code length is always less than 6`() {
        val validCode = "123456"
        val invalidCode = validCode + "123"

        sut.updateCode(validCode)
        sut.updateCode(invalidCode)

        assertEquals(validCode, sut.uiState.code)
    }
}
