package com.makesure

import android.content.Context
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.ImageProxy
import com.makesure.data.repositories.ImageRepository
import com.makesure.data.repositories.UserRepository
import com.makesure.presentation.screens.main.bottomNavScreens.scan.ScanViewModel
import io.mockk.coEvery
import io.mockk.coJustRun
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkConstructor
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.io.File

class ScanViewModelUnitTests {
    @get:Rule
    var mainCoroutineRule = MainDispatcherRule()

    private lateinit var sut: ScanViewModel

    private lateinit var context: Context
    private lateinit var userRepository: UserRepository
    private lateinit var imageRepository: ImageRepository

    @Before
    fun `setup`() {
        userRepository = mockk(relaxed = true)
        imageRepository = mockk(relaxed = true)

        val mockDir = mockk<File>(relaxed = true)
        context = mockk(relaxed = true) {
            every { cacheDir } returns mockDir
        }

        sut = ScanViewModel(
            context = context,
            userRepository = userRepository,
            imageRepository = imageRepository,
        )
    }

    @Test
    fun `test init`() {
        val correctUiState = ScanViewModel.ScanUiState(
            state = ScanViewModel.ScanState.DEFAULT,
            image = null,
            result = "Unknown",
            user = null
        )

        assertEquals(correctUiState, sut.uiState.value)
    }

    @Test
    fun `test take image success`() {
        val planesMockk = mockk<ImageProxy.PlaneProxy> {
            every { buffer } returns mockk(relaxed = true)
        }
        val image: ImageProxy = mockk(relaxed = true) {
            every { planes } returns Array(2) { planesMockk }
        }

        sut.imageCapturedCallback.onCaptureSuccess(image)

        val correctUiState = ScanViewModel.ScanUiState(
            state = ScanViewModel.ScanState.IMAGE_TAKEN,
            image = image,
            result = "Unknown",
            user = null
        )

        assertEquals(correctUiState, sut.uiState.value)
    }

    @Test
    fun `test take image error`() {
        val error: ImageCaptureException = mockk(relaxed = true)

        sut.imageCapturedCallback.onError(error)

        val correctUiState = ScanViewModel.ScanUiState(
            state = ScanViewModel.ScanState.ERROR,
            image = null,
            result = "Unknown",
            user = null
        )
        assertEquals(correctUiState, sut.uiState.value)
    }

    @Test
    fun `test send image success`() {
        mockkConstructor(File::class)
        val planesMockk = mockk<ImageProxy.PlaneProxy> {
            every { buffer } returns mockk(relaxed = true)
        }
        val image: ImageProxy = mockk(relaxed = true) {
            every { planes } returns Array(2) { planesMockk }
        }
        coJustRun { imageRepository.testResultParseImage(any()) }

        sut.imageCapturedCallback.onCaptureSuccess(image)
        sut.onSendClicked()

        val correctUiState = ScanViewModel.ScanUiState(
            state = ScanViewModel.ScanState.SUCCESS,
            image = image,
            result = "Unknown",
            user = null
        )
        assertEquals(correctUiState, sut.uiState.value)
    }

    @Test
    fun `test send image error`() {
        val planesMockk = mockk<ImageProxy.PlaneProxy> {
            every { buffer } returns mockk(relaxed = true)
        }
        val image: ImageProxy = mockk(relaxed = true) {
            every { planes } returns Array(2) { planesMockk }
        }
        coEvery { imageRepository.testResultParseImage(any()) } throws Exception()

        sut.imageCapturedCallback.onCaptureSuccess(image)
        sut.onSendClicked()

        val correctUiState = ScanViewModel.ScanUiState(
            state = ScanViewModel.ScanState.ERROR,
            image = null,
            result = "Unknown",
            user = null
        )
        assertEquals(correctUiState, sut.uiState.value)
    }
}
