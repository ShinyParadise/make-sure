package com.makesure.app

import android.content.Intent
import android.os.Bundle
import android.view.WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.makesure.app.theme.MakeSureTheme
import com.makesure.presentation.components.views.ErrorScreen
import com.makesure.presentation.screens.main.MainScreen

@Suppress("DEPRECATION")
class StartActivity : ComponentActivity() {
    private lateinit var navController: NavHostController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setSoftInputMode(SOFT_INPUT_ADJUST_RESIZE)

        setContent {
            navController = rememberNavController()

            MakeSureTheme {
                if (MakeSureApp.failedToSetup) {
                    ErrorScreen()
                } else {
                    MainScreen(navController)
                }
            }
        }
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        navController.handleDeepLink(intent)
    }
}
