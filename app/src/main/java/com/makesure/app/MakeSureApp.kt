package com.makesure.app

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import com.makesure.BuildConfig
import com.makesure.app.di.appModule
import com.makesure.app.di.mainDispatcher
import com.onesignal.OneSignal
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.qualifier.named

class MakeSureApp : Application() {

    override fun onCreate() {
        super.onCreate()

        try {
            startDI()
            setupOneSignal()
            createNotificationChannel()
        } catch (e: Exception) {
            failedToSetup = true
        }
    }

    private fun setupOneSignal() {
        OneSignal.initWithContext(this, BuildConfig.ONESIGNAL_APP_ID)

        val scope by inject<CoroutineScope>()
        val mainDispatcher by inject<CoroutineDispatcher>(named(mainDispatcher))

        scope.launch(mainDispatcher) {
            OneSignal.Notifications.requestPermission(true)
        }
    }

    private fun createNotificationChannel() {
        val channel = NotificationChannel(
            UserNotifications.CHANNEL_ID,
            UserNotifications.CHANNEL_NAME,
            NotificationManager.IMPORTANCE_DEFAULT
        )

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)
    }

    private fun startDI() {
        startKoin {
            androidLogger()
            androidContext(this@MakeSureApp)
            modules(appModule)
        }
    }

    companion object {
        var failedToSetup: Boolean = false
            private set
    }
}
