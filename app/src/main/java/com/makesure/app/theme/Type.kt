package com.makesure.app.theme

import androidx.compose.material.Typography
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import com.makesure.R

val mulishFonts = FontFamily(
    listOf(
        Font(
            R.font.mulish_medium,
            weight = FontWeight.Medium
        ),
        Font(
            R.font.mulish_bold,
            weight = FontWeight.Bold
        ),
        Font(
            R.font.mulish_regular,
            weight = FontWeight.Normal
        ),
        Font(
            resId = R.font.mulish_light,
            weight = FontWeight.Light
        ),
        Font(
            resId = R.font.mulish_black,
            weight = FontWeight.Black
        ),
        Font(
            resId = R.font.mulish_semibold,
            weight = FontWeight.SemiBold
        ),
        Font(
            resId = R.font.mulish_extrabold,
            weight = FontWeight.ExtraBold
        )
    )
)

val interFonts = FontFamily(
    listOf(
        Font(
            resId = R.font.inter_regular,
            weight = FontWeight.Normal
        ),
        Font(
            resId = R.font.inter_semibold,
            weight = FontWeight.SemiBold
        ),
    )
)

val wixFonts = FontFamily(
    listOf(
        Font(
            resId = R.font.wixmadefordisplay_regular,
            weight = FontWeight.Normal
        ),
        Font(
            resId = R.font.wixmadefordisplay_bold,
            weight = FontWeight.Bold
        ),
        Font(
            resId = R.font.wixmadefordisplay_semibold,
            weight = FontWeight.SemiBold
        ),
        Font(
            resId = R.font.wixmadefordisplay_medium,
            weight = FontWeight.Medium
        ),
        Font(
            resId = R.font.wixmadefordisplay_extrabold,
            weight = FontWeight.ExtraBold
        ),
    )
)


val Typography = Typography(
    defaultFontFamily = mulishFonts
)
