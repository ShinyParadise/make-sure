package com.makesure.app.theme

import androidx.compose.material.MaterialTheme
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import com.google.accompanist.systemuicontroller.rememberSystemUiController

private val LightColorPalette = lightColors(
    primary = Primary,
    primaryVariant = PrimaryVar,
    secondary = Secondary,
    secondaryVariant = SecondaryVar,
    surface = Surface,
    onSurface = onSurface,
    onSecondary = Color.White,
)

@Composable
fun MakeSureTheme(content: @Composable () -> Unit) {
    val colors = LightColorPalette

    val systemUiController = rememberSystemUiController()
    systemUiController.setSystemBarsColor(
        color = Color.White
    )

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}
