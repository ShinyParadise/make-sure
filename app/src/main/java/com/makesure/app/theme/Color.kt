package com.makesure.app.theme

import androidx.compose.ui.graphics.Color

val Primary = Color(0xFF001777)
val PrimaryVar = Color(0xFFAC77FF)
val PrimaryGradient = listOf(Primary, PrimaryVar)
val PrimaryDarkGradient = listOf(Color(0xFF7822FF), Primary)

val Surface = Color.White
val onSurface = Color.Black

val Secondary = Color(0xFF4924DD)
val SecondaryVar = Color(0xFFA672FD)
val SecondaryGradient = listOf(Secondary, SecondaryVar)

val homeScreenPrimaryColor = Color(0xFF675AC0)
val topAppBarButtonsColor = Primary

val greenDotColor = Color(0xFF5FE986)
val mainBlueColor = Color(0xFF4765FF)
