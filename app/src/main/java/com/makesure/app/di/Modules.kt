package com.makesure.app.di

import android.content.Context
import android.util.Log
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.makesure.BuildConfig
import com.makesure.data.datastore.dataStore
import com.makesure.data.gateways.ScanGateway
import com.makesure.data.gateways.SmsGateway
import com.makesure.data.gateways.SmsGatewayImpl
import com.makesure.data.middleware.BasicAuthenticator
import com.makesure.data.repositories.ImageRepository
import com.makesure.data.repositories.ImageRepositoryImpl
import com.makesure.data.repositories.LinksRepository
import com.makesure.data.repositories.LinksRepositoryImpl
import com.makesure.data.repositories.MeetingsRepository
import com.makesure.data.repositories.MeetingsRepositoryImpl
import com.makesure.data.repositories.NotificationsRepository
import com.makesure.data.repositories.NotificationsRepositoryImpl
import com.makesure.data.repositories.PostsRepository
import com.makesure.data.repositories.PostsRepositoryImpl
import com.makesure.data.repositories.ReportsRepository
import com.makesure.data.repositories.ReportsRepositoryImpl
import com.makesure.data.repositories.TestRepository
import com.makesure.data.repositories.TestRepositoryImpl
import com.makesure.data.repositories.UserRepository
import com.makesure.data.repositories.UserRepositoryImpl
import com.makesure.domain.entities.UserSession
import com.makesure.domain.interactors.AuthInteractor
import com.makesure.domain.interactors.LinksInteractor
import com.makesure.domain.interactors.MeetingsInteractor
import com.makesure.domain.interactors.NotificationsInteractor
import com.makesure.domain.interactors.ReportsInteractor
import com.makesure.domain.interactors.ScanInteractor
import com.makesure.domain.interactors.TestsInteractor
import com.makesure.domain.interactors.UserInteractor
import com.makesure.presentation.components.views.calendar.CalendarViewModel
import com.makesure.presentation.components.views.smallCalendar.SmallCalendarViewModel
import com.makesure.presentation.screens.contact.ContactViewModel
import com.makesure.presentation.screens.main.MainViewModel
import com.makesure.presentation.screens.main.bottomNavScreens.scan.ScanViewModel
import com.makesure.presentation.screens.notifications.NotificationsViewModel
import com.makesure.presentation.screens.register.RegisterViewModel
import com.makesure.presentation.screens.settings.SettingsViewModel
import com.makesure.presentation.screens.share.ShareViewModel
import com.makesure.presentation.screens.sharedProfile.SharedProfileViewModel
import com.makesure.presentation.screens.signIn.SignInViewModel
import com.makesure.presentation.screens.testResult.TestResultViewModel
import com.makesure.utils.DatesProvider
import io.github.jan.supabase.createSupabaseClient
import io.github.jan.supabase.gotrue.Auth
import io.github.jan.supabase.postgrest.Postgrest
import io.github.jan.supabase.serializer.KotlinXSerializer
import io.github.jan.supabase.storage.Storage
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.create
import java.time.Duration

// All context injected by Koin is an "application context",
// so the lint errors are ignored in viewmodels

private val repoModule = module {
    single<UserRepository> { UserRepositoryImpl(get()) }
    single<PostsRepository> { PostsRepositoryImpl(get()) }
    single<TestRepository> { TestRepositoryImpl(get()) }
    single<ImageRepository> { ImageRepositoryImpl(get()) }
    single<NotificationsRepository> { NotificationsRepositoryImpl(get()) }
    single<LinksRepository> { LinksRepositoryImpl(get()) }
    single<MeetingsRepository> { MeetingsRepositoryImpl(get()) }
    single<ReportsRepository> { ReportsRepositoryImpl(get()) }
}

private val interactorModule = module {
    single<UserInteractor> { UserInteractor(get(), get(), get(), get()) }
    single<LinksInteractor> { LinksInteractor(get(), get()) }
    single<AuthInteractor> { AuthInteractor(get(), get(), get()) }
    single<TestsInteractor> { TestsInteractor(get(), get()) }
    single<ReportsInteractor> { ReportsInteractor(get(), get()) }
    single<NotificationsInteractor> { NotificationsInteractor(get(), get()) }
    single<ScanInteractor> { ScanInteractor(get(), get(), get(), get(named(bgDispatcher))) }
    single<MeetingsInteractor> { MeetingsInteractor(get(), get(), get(), get()) }
}

private val networkModule = module {
    single(createdAtStart = true) {
        val json by inject<Json>()

        createSupabaseClient(
            supabaseUrl = BuildConfig.SUPABASE_URL,
            supabaseKey = BuildConfig.API_KEY
        ) {
            install(Postgrest)
            install(Auth)
            install(Storage) {
                jwtToken = BuildConfig.SERVICE_KEY
            }

            defaultSerializer = KotlinXSerializer(json)
        }
    }

    single(named(WORKHORSE_SERVICE)) {
        val client by inject<OkHttpClient>(qualifier = named(IMAGE_HTTP_CLIENT))
        val json by inject<Json>()

        Retrofit.Builder()
            .baseUrl(BuildConfig.WORKHORSE_URL)
            .client(client)
            .addConverterFactory(json.asConverterFactory("application/json".toMediaType()))
            .build()
    }

    single<ScanGateway> {
        val retrofit by inject<Retrofit>(qualifier = named(WORKHORSE_SERVICE))
        retrofit.create()
    }
    single<SmsGateway> { SmsGatewayImpl(get(qualifier = named(SMS_HTTP_CLIENT))) }

    single(named(SMS_HTTP_CLIENT)) {
        OkHttpClient.Builder()
            .authenticator(BasicAuthenticator(BuildConfig.SMS_EMAIL, BuildConfig.SMS_API_KEY))
            .build()
    }

    single(named(IMAGE_HTTP_CLIENT)) {
        OkHttpClient.Builder()
            .addInterceptor {
                val req = it.request()
                req.newBuilder()
                    .addHeader("Api-Key", BuildConfig.IMAGE_API_KEY)
                    .build()

                it.proceed(req)
            }
            .connectTimeout(Duration.ofSeconds(30))
            .readTimeout(Duration.ofSeconds(30))
            .writeTimeout(Duration.ofSeconds(30))
            .build()
    }

    single<Json> {
        Json { isLenient = true }
    }
}

val appModule = module {
    includes(networkModule, repoModule, interactorModule)

    single<CoroutineDispatcher>(named(bgDispatcher)) { Dispatchers.IO }
    single<CoroutineDispatcher>(named(mainDispatcher)) { Dispatchers.Main }
    single {
        val context = get<Context>()
        context.dataStore
    }
    single<CoroutineScope> {
        CoroutineScope(SupervisorJob() + Dispatchers.Default + CoroutineExceptionHandler {
                _, throwable -> Log.d("CustomCoroutineScope", throwable.stackTraceToString())
        })
    }

    single<UserSession>(createdAtStart = true) { UserSession(
        get(),
        get(named(bgDispatcher)),
        get(),
        get()
    ) }

    factory<DatesProvider> { DatesProvider() }

    viewModel {
        SignInViewModel(
            userInteractor = get(),
            authInteractor = get(),
            context = get(),
            mainDispatcher = get(qualifier = named(mainDispatcher)),
            bgDispatcher = get(qualifier = named(bgDispatcher)),
            savedStateHandle = get(),
        )
    }
    viewModel{
        RegisterViewModel(
            context = get(),
            userInteractor = get(),
            authInteractor = get(),
            mainDispatcher = get(qualifier = named(mainDispatcher)),
            bgDispatcher = get(qualifier = named(bgDispatcher)),
            savedStateHandle = get(),
        )
    }
    viewModel {
        MainViewModel(
            userInteractor = get(),
            meetingsInteractor = get(),
            authInteractor = get(),
            testsInteractor = get(),
            linksInteractor = get(),
            reportsInteractor = get(),
            bgDispatcher = get(qualifier = named(bgDispatcher)),
            mainDispatcher = get(qualifier = named(mainDispatcher)),
            context = get(),
            savedStateHandle = get(),
        )
    }
    viewModel {
        ShareViewModel(
            userInteractor = get(),
            bgDispatcher = get(qualifier = named(bgDispatcher)),
            mainDispatcher = get(qualifier = named(mainDispatcher)),
            savedStateHandle = get()
        )
    }
    viewModel {
        SettingsViewModel(
            userInteractor = get(),
            authInteractor = get(),
            savedStateHandle = get(),
            context = get(),
            bgDispatcher = get(qualifier = named(bgDispatcher)),
            mainDispatcher = get(qualifier = named(mainDispatcher)),
        )
    }
    viewModel {
        NotificationsViewModel(
            userInteractor = get(),
            notificationsInteractor = get(),
            bgDispatcher = get(qualifier = named(bgDispatcher)),
            mainDispatcher = get(qualifier = named(mainDispatcher)),
            savedStateHandle = get(),
        )
    }
    viewModel {
        SharedProfileViewModel(
            userInteractor = get(),
            linksInteractor = get(),
            mainDispatcher = get(qualifier = named(mainDispatcher)),
            bgDispatcher = get(qualifier = named(bgDispatcher)),
            savedStateHandle = get(),
        )
    }
    viewModel {
        ScanViewModel(
            userInteractor = get(),
            scanInteractor = get(),
            mainDispatcher = get(qualifier = named(mainDispatcher)),
            bgDispatcher = get(qualifier = named(bgDispatcher)),
        )
    }
    viewModel {
        TestResultViewModel(
            userInteractor = get(),
            notificationsInteractor = get(),
            savedStateHandle = get(),
            bgDispatcher = get(qualifier = named(bgDispatcher)),
            mainDispatcher = get(qualifier = named(mainDispatcher)),
        )
    }
    viewModel {
        ContactViewModel(
            userInteractor = get(),
            meetingsInteractor = get(),
            testsInteractor = get(),
            savedStateHandle = get(),
            bgDispatcher = get(qualifier = named(bgDispatcher)),
            mainDispatcher = get(qualifier = named(mainDispatcher)),
            context = get()
        )
    }
    viewModel {
        CalendarViewModel(
            userInteractor = get(),
            testsInteractor = get(),
            meetingsInteractor = get(),
            datesProvider = get(),
            savedStateHandle = get(),
            bgDispatcher = get(qualifier = named(bgDispatcher)),
            mainDispatcher = get(qualifier = named(mainDispatcher)),
        )
    }

    viewModel {
        SmallCalendarViewModel(
            userInteractor = get(),
            testsInteractor = get(),
            meetingsInteractor = get(),
            datesProvider = get(),
            savedStateHandle = get(),
            bgDispatcher = get(qualifier = named(bgDispatcher)),
            mainDispatcher = get(qualifier = named(mainDispatcher)),
        )
    }
}

private const val WORKHORSE_SERVICE = "workhorse"
private const val SMS_HTTP_CLIENT = "sms_client"
private const val IMAGE_HTTP_CLIENT = "image_client"
const val bgDispatcher = "bg_dispatcher"
const val mainDispatcher = "main_dispatcher"
