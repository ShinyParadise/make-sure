package com.makesure.app

object UserNotifications {
    const val CHANNEL_ID: String = "makesure_channel"
    const val CHANNEL_NAME: String = "User Notifications"
}
