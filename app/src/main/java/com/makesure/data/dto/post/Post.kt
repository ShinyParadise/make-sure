package com.makesure.data.dto.post

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@Parcelize
data class Post(
    val id: String,

    @SerialName("created_at")
    val createdAt: String,

    val title: String,

    @SerialName("title_ru")
    val ruTitle: String = "",

    val description: String?,

    @SerialName("description_ru")
    val ruDescription: String? = null,

    val link: String,

    @SerialName("link_ru")
    val ruLink: String = "",

    @SerialName("image_URL")
    val imageURL: String,

    val type: String,

    @SerialName("type_ru")
    val ruType: String = "",
) : Parcelable
