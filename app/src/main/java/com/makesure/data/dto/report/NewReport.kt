package com.makesure.data.dto.report

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class NewReport(
    @SerialName("from_user_id")
    val fromUser: String,

    val text: String,

    @SerialName("user_id")
    val userId: String,
)
