package com.makesure.data.dto.meeting

import com.makesure.data.serializers.ZonedDateTimeSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.time.ZonedDateTime

@Serializable
data class NewMeeting(
    @SerialName("user_id")
    val userId: String,

    @SerialName("meeting_date")
    val meetingDate: String,

    @SerialName("participant_b")
    val secondUserId: String,
)
