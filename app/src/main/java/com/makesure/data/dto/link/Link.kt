package com.makesure.data.dto.link

import com.makesure.data.serializers.ZonedDateTimeSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.time.ZonedDateTime

@Serializable
data class Link(
    val id: String,

    @SerialName("created_at")
    @Serializable(ZonedDateTimeSerializer::class)
    val createdAt: ZonedDateTime,

    @SerialName("user_id")
    val userId: String,
)
