package com.makesure.data.dto.user

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@Parcelize
data class NewUser(
    val name: String,

    val phone: String,

    val sex: String? = null,

    @SerialName("date_of_birth")
    val birthday: String,

    val email: String?,

    @SerialName("photo_URL")
    val photoURL: String?,

    @SerialName("blocked_users")
    val blockedUsers: List<String>?,

    val contacts: List<String>?
) : Parcelable
