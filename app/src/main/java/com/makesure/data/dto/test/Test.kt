package com.makesure.data.dto.test

import android.os.Parcelable
import com.makesure.data.serializers.ZonedDateTimeParceler
import com.makesure.data.serializers.ZonedDateTimeSerializer
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.WriteWith
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.time.ZonedDateTime

@Serializable
@Parcelize
data class Test(
    val id: String,

    @SerialName("package_id")
    val packageId: String,

    @SerialName("user_id")
    val userId: String,

    @SerialName("date_time")
    @Serializable(with = ZonedDateTimeSerializer::class)
    val dateTime: @WriteWith<ZonedDateTimeParceler> ZonedDateTime,

    @SerialName("infection_type")
    val infectionType: String,

    val result: String?,

    @SerialName("shared_with")
    val sharedWith: List<String>? = null,

    @SerialName("is_activated")
    val isActivated: Boolean,

    @SerialName("test_photoURL")
    val photoURL: String?,
) : Parcelable
