package com.makesure.data.dto.user

import android.os.Parcelable
import com.makesure.data.serializers.ZonedDateTimeParceler
import com.makesure.data.serializers.ZonedDateTimeSerializer
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.WriteWith
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.time.ZonedDateTime

@Serializable
@Parcelize
data class User(
    val id: String,

    val name: String,

    val phone: String,

    val sex: String,

    @SerialName("date_of_birth")
    @Serializable(ZonedDateTimeSerializer::class)
    val birthday: @WriteWith<ZonedDateTimeParceler> ZonedDateTime,

    val email: String?,

    @SerialName("photo_URL")
    val photoURL: String?,

    @SerialName("blocked_users")
    val blockedUsers: List<String>?,

    val contacts: List<String>?
) : Parcelable

enum class Sex {
    FEMALE, MALE
}
