package com.makesure.data.dto.link

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class NewLink(
    @SerialName("user_id")
    val userId: String,
)
