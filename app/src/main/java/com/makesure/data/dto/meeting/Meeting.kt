package com.makesure.data.dto.meeting

import com.makesure.data.serializers.ZonedDateTimeSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.time.ZonedDateTime

@Serializable
data class Meeting(
    val id: String,

    @SerialName("user_id")
    val userId: String,

    @SerialName("meeting_date")
    @Serializable(ZonedDateTimeSerializer::class)
    val meetingDate: ZonedDateTime,

    @SerialName("participant_b")
    val secondUserId: String,
)
