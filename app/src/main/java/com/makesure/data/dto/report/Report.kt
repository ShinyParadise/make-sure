package com.makesure.data.dto.report

import com.makesure.data.serializers.ZonedDateTimeSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.time.ZonedDateTime

@Serializable
data class Report(
    val id: String,

    @SerialName("created_at")
    @Serializable(ZonedDateTimeSerializer::class)
    val createdAt: ZonedDateTime,

    @SerialName("from_user_id")
    val fromUser: String,

    val text: String,

    @SerialName("user_id")
    val userId: String,
)
