package com.makesure.data.dto.notification

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class NewNotification(
    val title: String,

    val author: String? = null,

    val description: String? = null,

    @SerialName("created_at")
    val createdAt: String,

    @SerialName("user_id")
    val userId: String,

    @SerialName("is_notified")
    val isNotified: Boolean = false
)
