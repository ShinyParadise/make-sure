package com.makesure.data.dto.notification

import android.os.Parcelable
import com.makesure.data.serializers.ZonedDateTimeParceler
import com.makesure.data.serializers.ZonedDateTimeSerializer
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.WriteWith
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.time.ZonedDateTime

@Parcelize
@Serializable
data class Notification(
    val id: String,

    val title: String,

    @SerialName("title_ru")
    val titleRu: String? = null,

    val description: String? = null,

    @SerialName("description_ru")
    val descriptionRu: String? = null,

    val author: String? = null,

    @SerialName("created_at")
    @Serializable(ZonedDateTimeSerializer::class)
    val createdAt: @WriteWith<ZonedDateTimeParceler> ZonedDateTime,

    @SerialName("user_id")
    val userId: String,

    @SerialName("is_notified")
    val isNotified: Boolean = false
) : Parcelable

