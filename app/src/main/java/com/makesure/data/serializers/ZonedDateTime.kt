package com.makesure.data.serializers

import android.os.Parcel
import kotlinx.parcelize.Parceler
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import java.time.ZonedDateTime


object ZonedDateTimeParceler : Parceler<ZonedDateTime?> {
    override fun create(parcel: Parcel): ZonedDateTime? {
        return try {
            ZonedDateTime.parse(parcel.readString())
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    override fun ZonedDateTime?.write(parcel: Parcel, flags: Int) {
        parcel.writeString(this.toString())
    }
}

object ZonedDateTimeSerializer : KSerializer<ZonedDateTime> {
    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor(
        "ZonedDateTime",
        PrimitiveKind.STRING
    )

    override fun serialize(encoder: Encoder, value: ZonedDateTime) {
        encoder.encodeString(value.toString())
    }

    override fun deserialize(decoder: Decoder): ZonedDateTime {
        return ZonedDateTime.parse(decoder.decodeString())
    }
}
