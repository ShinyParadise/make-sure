package com.makesure.data.serializers

import android.os.Parcel
import androidx.compose.ui.graphics.Color
import kotlinx.parcelize.Parceler

object ColorParceler : Parceler<Color?> {
    override fun create(parcel: Parcel): Color? {
        return try {
            val red = parcel.readFloat()
            val green = parcel.readFloat()
            val blue = parcel.readFloat()
            Color(red, green, blue)
        } catch (_: Exception) {
            null
        }
    }

    override fun Color?.write(parcel: Parcel, flags: Int) {
        parcel.writeValue(this?.red)
        parcel.writeValue(this?.green)
        parcel.writeValue(this?.blue)
    }
}
