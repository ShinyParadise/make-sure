package com.makesure.data.middleware

import okhttp3.Authenticator
import okhttp3.Credentials
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route


class BasicAuthenticator(user: String, password: String) : Authenticator {
    private val credentials: String

    init {
        credentials = Credentials.basic(user, password)
    }

    override fun authenticate(route: Route?, response: Response): Request {
        return response.request
            .newBuilder()
            .addHeader("Authorization", credentials)
            .build()
    }
}
