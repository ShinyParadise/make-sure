package com.makesure.data.gateways

import android.os.Parcelable
import com.makesure.BuildConfig
import kotlinx.parcelize.Parcelize
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

private const val BASE_URL = BuildConfig.WORKHORSE_URL

interface ScanGateway {
    @GET("$BASE_URL/")
    suspend fun test(): ResponseBody

    @Multipart
    @POST("$BASE_URL/test_img_sending")
    suspend fun testImgSending(@Part image: MultipartBody.Part): ResponseBody

    @Multipart
    @POST("$BASE_URL/test_results_parse")
    suspend fun testResultsParse(@Part image: MultipartBody.Part): ResultsParseBody

    @Multipart
    @POST("$BASE_URL/test_results_detection")
    suspend fun testResultsDetection(@Part image: MultipartBody.Part): ResultsDetectionBody
}

@Parcelize
@Serializable
data class ResultsParseBody(
    val id: Int,
    val isActivated: Boolean,
    @SerialName("package_id") val packageId: Int,
    val result: Boolean
): Parcelable


/**
 * Error codes: 0 - QR recognition error
 * 1 - type recognition error
 * 2 - result recognition error
*/
@Parcelize
@Serializable
data class ResultsDetectionBody(
    @SerialName("error_code") val errorCode: Int?,
    val id: String?,
    val isActivated: Boolean?,
    @SerialName("package_id") val packageId: String?,
    val result: String?,
    @SerialName("test_type") val testType: String?
): Parcelable
