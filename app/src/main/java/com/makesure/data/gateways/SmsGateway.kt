package com.makesure.data.gateways

import okhttp3.OkHttpClient
import okhttp3.Request

interface SmsGateway {
    suspend fun sendSms(text: String, number: String)
}

class SmsGatewayImpl(
    private val client: OkHttpClient
) : SmsGateway {

    override suspend fun sendSms(text: String, number: String) {
        val url = "https://@gate.smsaero.ru/v2/sms/send?number=$number&text=$text&sign=SMS Aero"

        val req = Request.Builder().url(url).build()

        try {
            val res = client.newCall(req).execute()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
