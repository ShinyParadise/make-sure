package com.makesure.data.repositories

import com.makesure.data.dto.report.Report
import com.makesure.data.dto.report.NewReport
import io.github.jan.supabase.SupabaseClient
import io.github.jan.supabase.postgrest.postgrest
import io.github.jan.supabase.postgrest.query.Columns

interface ReportsRepository {
    val tableName: String

    suspend fun getAll(): Result<List<Report>>
    suspend fun getById(id: String): Result<Report>
    suspend fun getByUserId(userId: String): Result<List<Report>>
    suspend fun insert(report: NewReport): Result<Report>
    suspend fun update(report: Report): Result<Report>
    suspend fun delete(report: Report)
}

class ReportsRepositoryImpl(
    private val supabaseClient: SupabaseClient,
) : ReportsRepository {
    override val tableName: String = "support"

    override suspend fun getAll(): Result<List<Report>> = runCatching {
        supabaseClient.postgrest[tableName].select(Columns.ALL).decodeList()
    }

    override suspend fun getById(id: String): Result<Report> = runCatching {
        supabaseClient.postgrest[tableName].select {
            filter {
                Report::id eq id
            }
        }.decodeSingle()
    }

    override suspend fun getByUserId(userId: String): Result<List<Report>> = runCatching {
        supabaseClient.postgrest[tableName].select {
            filter {
                Report::userId eq userId
            }
        }.decodeList()
    }

    override suspend fun insert(report: NewReport): Result<Report> = runCatching {
        supabaseClient.postgrest[tableName].insert(report) {
            select()
        }.decodeSingle()
    }

    override suspend fun update(report: Report): Result<Report> = runCatching {
        supabaseClient.postgrest[tableName].update(report) {
            select()
        }.decodeSingle()
    }

    override suspend fun delete(report: Report) {
        runCatching {
            supabaseClient.postgrest[tableName].delete {
                filter {
                    Report::id eq report.id
                }
            }
        }
    }
}

