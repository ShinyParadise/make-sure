package com.makesure.data.repositories

import com.makesure.data.dto.notification.NewNotification
import com.makesure.data.dto.notification.Notification
import io.github.jan.supabase.SupabaseClient
import io.github.jan.supabase.postgrest.postgrest


interface NotificationsRepository {
    val tableName: String

    suspend fun getAll(): Result<List<Notification>>
    suspend fun getById(id: String): Result<Notification>
    suspend fun getByUserId(userId: String): Result<List<Notification>>
    suspend fun insert(notification: NewNotification): Result<Notification>
    suspend fun update(notification: Notification): Result<Notification>
    suspend fun delete(notification: Notification)
}

class NotificationsRepositoryImpl(
    private val client: SupabaseClient
) : NotificationsRepository {
    override val tableName: String = "notifications"

    override suspend fun getAll(): Result<List<Notification>> = runCatching {
        client.postgrest[tableName].select().decodeList()
    }

    override suspend fun getById(id: String): Result<Notification> = runCatching {
        client.postgrest[tableName].select {
            filter {
                Notification::id eq id
            }
        }.decodeSingle()
    }

    override suspend fun getByUserId(userId: String): Result<List<Notification>> = runCatching {
        client.postgrest[tableName].select {
            filter {
                Notification::userId eq userId
            }
        }.decodeList()
    }

    override suspend fun insert(
        notification: NewNotification
    ): Result<Notification> = runCatching {
        client.postgrest[tableName].insert(notification){
            select()
        }.decodeSingle()
    }

    override suspend fun update(notification: Notification): Result<Notification> = runCatching {
        client.postgrest[tableName]
            .update(notification) {
                filter {
                    Notification::id eq notification.id
                }
                select()
            }
            .decodeSingle()
    }

    override suspend fun delete(notification: Notification) {
        runCatching {
            client.postgrest[tableName].delete {
                filter {
                    Notification::id eq notification.id
                }
            }
        }
    }
}
