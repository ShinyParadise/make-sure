package com.makesure.data.repositories

import com.makesure.data.dto.post.Post
import io.github.jan.supabase.SupabaseClient
import io.github.jan.supabase.postgrest.postgrest
import io.github.jan.supabase.postgrest.query.Columns

interface PostsRepository {
    val tableName: String

    suspend fun getAll(): Result<List<Post>>
}

class PostsRepositoryImpl(private val supabaseClient: SupabaseClient) : PostsRepository {
    override val tableName: String = "tips"

    override suspend fun getAll(): Result<List<Post>> = runCatching {
        supabaseClient.postgrest[tableName].select(Columns.ALL).decodeList()
    }
}
