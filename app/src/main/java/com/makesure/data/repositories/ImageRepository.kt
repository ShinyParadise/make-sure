package com.makesure.data.repositories

import io.github.jan.supabase.SupabaseClient
import io.github.jan.supabase.storage.storage

interface ImageRepository {
    suspend fun uploadImage(
        name: String,
        byteArray: ByteArray
    ): Result<String>

    suspend fun getPublicUrl(path: String): Result<String>
}

class ImageRepositoryImpl(
    private val supabaseClient: SupabaseClient,
) : ImageRepository {
    private val bucketId = "user_images"

    override suspend fun uploadImage(
        name: String,
        byteArray: ByteArray
    ): Result<String> = runCatching {
        supabaseClient.storage[bucketId].upload(name, byteArray, true)
    }

    override suspend fun getPublicUrl(path: String): Result<String> = runCatching {
        supabaseClient.storage[bucketId].publicUrl(path)
    }
}
