package com.makesure.data.repositories

import com.makesure.data.dto.link.Link
import com.makesure.data.dto.link.NewLink
import io.github.jan.supabase.SupabaseClient
import io.github.jan.supabase.postgrest.postgrest


interface LinksRepository {
    val tableName: String

    suspend fun getAll(): Result<List<Link>>
    suspend fun getById(id: String): Result<Link>
    suspend fun getByUserId(userId: String): Result<Link?>
    suspend fun insert(friendLink: NewLink): Result<Link>
    suspend fun update(link: Link): Result<Link>
    suspend fun delete(link: Link)
}

class LinksRepositoryImpl(
    private val client: SupabaseClient
) : LinksRepository {
    override val tableName: String = "friend_links"

    override suspend fun getAll(): Result<List<Link>> = runCatching {
        client.postgrest[tableName].select().decodeList()
    }

    override suspend fun getById(id: String): Result<Link> = runCatching {
        client.postgrest[tableName].select {
            filter {
                Link::id eq id
            }
        }.decodeSingle()
    }

    override suspend fun getByUserId(userId: String): Result<Link?> = runCatching {
        client.postgrest[tableName].select {
            filter {
                Link::userId eq userId
            }
        }.decodeSingleOrNull()
    }

    override suspend fun insert(
        friendLink: NewLink
    ): Result<Link> = runCatching {
        client.postgrest[tableName].insert(friendLink) {
            select()
        }.decodeSingle()
    }

    override suspend fun update(link: Link): Result<Link> = runCatching {
        client.postgrest[tableName]
            .update(link) {
                filter {
                    Link::id eq link.id
                }
                select()
            }
            .decodeSingle()
    }

    override suspend fun delete(link: Link) {
        runCatching {
            client.postgrest[tableName].delete {
                filter {
                    Link::id eq link.id
                }
            }
        }
    }
}
