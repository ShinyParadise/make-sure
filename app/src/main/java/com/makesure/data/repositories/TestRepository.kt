package com.makesure.data.repositories

import com.makesure.data.dto.test.Test
import io.github.jan.supabase.SupabaseClient
import io.github.jan.supabase.postgrest.postgrest
import io.github.jan.supabase.postgrest.query.Columns

interface TestRepository {
    val tableName: String

    suspend fun getAll(): Result<List<Test>>
    suspend fun getById(id: String): Result<Test>
    suspend fun getByUserId(userId: String): Result<List<Test>>
    suspend fun insert(test: Test): Result<Test>
    suspend fun update(test: Test): Result<Test>
    suspend fun delete(test: Test)
}

class TestRepositoryImpl(private val supabaseClient: SupabaseClient) : TestRepository {
    override val tableName: String = "tests"

    override suspend fun getAll(): Result<List<Test>> = runCatching {
        supabaseClient.postgrest[tableName].select(Columns.ALL).decodeList()
    }

    override suspend fun getById(id: String): Result<Test> = runCatching {
        supabaseClient.postgrest[tableName].select {
            filter {
                Test::id eq id
            }
        }.decodeSingle()
    }

    override suspend fun getByUserId(userId: String): Result<List<Test>> = runCatching {
        supabaseClient.postgrest[tableName].select {
            filter {
                Test::userId eq userId
            }
        }.decodeList()
    }

    override suspend fun insert(test: Test): Result<Test> = runCatching {
        supabaseClient.postgrest[tableName].insert(test) {
            select()
        }.decodeSingle()
    }

    override suspend fun update(test: Test): Result<Test> = runCatching {
        supabaseClient.postgrest[tableName].update(test) {
            select()
        }.decodeSingle()
    }

    override suspend fun delete(test: Test) {
        runCatching {
            supabaseClient.postgrest[tableName].delete {
                filter {
                    Test::id eq test.id
                }
            }
        }
    }
}
