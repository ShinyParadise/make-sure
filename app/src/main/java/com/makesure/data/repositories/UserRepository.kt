package com.makesure.data.repositories

import com.makesure.data.dto.user.NewUser
import com.makesure.data.dto.user.User
import io.github.jan.supabase.SupabaseClient
import io.github.jan.supabase.postgrest.postgrest
import io.github.jan.supabase.postgrest.query.Columns

interface UserRepository {
    val tableName: String

    suspend fun getAll(): Result<List<User>>
    suspend fun getById(id: String): Result<User>
    suspend fun getByPhone(phone: String): Result<User>
    suspend fun insert(user: NewUser): Result<User>
    suspend fun update(user: User): Result<User>
    suspend fun delete(user: User)
}

class UserRepositoryImpl(private val supabaseClient: SupabaseClient): UserRepository {
    override val tableName: String = "users"

    override suspend fun getAll(): Result<List<User>> = runCatching {
        supabaseClient.postgrest[tableName].select(Columns.ALL).decodeList()
    }

    override suspend fun getById(id: String): Result<User> = runCatching {
        supabaseClient.postgrest[tableName].select {
            filter {
                User::id eq id
            }
        }.decodeSingle()
    }

    override suspend fun getByPhone(phone: String): Result<User> = runCatching {
        supabaseClient.postgrest[tableName].select {
            filter {
                User::phone eq phone
            }
        }.decodeSingle()
    }

    override suspend fun insert(user: NewUser): Result<User> = runCatching {
        supabaseClient.postgrest[tableName].insert(user) {
            select()
        }.decodeSingle()
    }

    override suspend fun update(user: User): Result<User> = runCatching {
        supabaseClient.postgrest[tableName]
            .update(user) {
                filter {
                    User::id eq user.id
                }
                select()
            }
            .decodeSingle()
    }

    override suspend fun delete(user: User) {
        runCatching {
            supabaseClient.postgrest[tableName].delete {
                filter {
                    User::id eq user.id
                }
            }
        }
    }
}
