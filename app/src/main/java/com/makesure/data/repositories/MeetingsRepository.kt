package com.makesure.data.repositories

import com.makesure.data.dto.meeting.Meeting
import com.makesure.data.dto.meeting.NewMeeting
import io.github.jan.supabase.SupabaseClient
import io.github.jan.supabase.postgrest.postgrest
import io.github.jan.supabase.postgrest.query.Columns
import io.github.jan.supabase.postgrest.query.Order

interface MeetingsRepository {
    val tableName: String

    suspend fun getAll(): Result<List<Meeting>>
    suspend fun getById(id: String): Result<Meeting>
    suspend fun getByUserId(
        userId: String,
        order: Order = Order.DESCENDING
    ): Result<List<Meeting>>
    suspend fun insert(meeting: NewMeeting): Result<Meeting>
    suspend fun update(meeting: Meeting): Result<Meeting>
    suspend fun delete(meeting: Meeting)
}

class MeetingsRepositoryImpl(
    private val supabaseClient: SupabaseClient,
) : MeetingsRepository {
    override val tableName: String = "meetings"

    override suspend fun getAll(): Result<List<Meeting>> = runCatching {
        supabaseClient.postgrest[tableName].select(Columns.ALL).decodeList()
    }

    override suspend fun getById(id: String): Result<Meeting> = runCatching {
        supabaseClient.postgrest[tableName].select {
            filter {
                Meeting::id eq id
            }
        }.decodeSingle()
    }

    override suspend fun getByUserId(
        userId: String,
        order: Order
    ): Result<List<Meeting>> = runCatching {
        supabaseClient.postgrest[tableName].select {
            filter {
                Meeting::userId eq userId
            }
            order(column = "meeting_date", order = order)
        }.decodeList()
    }

    override suspend fun insert(meeting: NewMeeting): Result<Meeting> = runCatching {
        supabaseClient.postgrest[tableName].insert(meeting) {
            select()
        }.decodeSingle()
    }

    override suspend fun update(meeting: Meeting): Result<Meeting> = runCatching {
        supabaseClient.postgrest[tableName].update(meeting) {
            select()
        }.decodeSingle()
    }

    override suspend fun delete(meeting: Meeting) {
        runCatching {
            supabaseClient.postgrest[tableName].delete {
                filter {
                    Meeting::id eq meeting.id
                }
            }
        }
    }
}
