package com.makesure.data.datastore

import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey

object DataStoreKeys {
    val userId = stringPreferencesKey("user")
    val notifications = booleanPreferencesKey("notifications")
    val language = stringPreferencesKey("lang")
}
