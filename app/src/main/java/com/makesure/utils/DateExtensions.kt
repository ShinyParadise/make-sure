package com.makesure.utils

import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

fun ZonedDateTime.getDayMonthAndYear(): String {
    val formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy")
    return this.format(formatter)
}

fun ZonedDateTime.getDayMonthAndYearLocal(): String {
    val formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)
    return this.format(formatter)
}

fun ZonedDateTime.formattedString(): String {
    val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssX")
    return this.format(formatter)
}
