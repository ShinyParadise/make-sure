package com.makesure.utils

import android.graphics.Bitmap
import android.graphics.Matrix
import java.nio.ByteBuffer
import java.util.Arrays

fun Bitmap.rotate(degrees: Float): Bitmap {
    val matrix = Matrix().apply { postRotate(degrees) }
    return Bitmap.createBitmap(this, 0, 0, width, height, matrix, true)
}

fun Bitmap.hashString(): String {
    val buffer: ByteBuffer = ByteBuffer.allocate(this.byteCount)
    this.copyPixelsToBuffer(buffer)
    return Arrays.hashCode(buffer.array()).toString()
}
