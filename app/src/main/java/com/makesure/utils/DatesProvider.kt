package com.makesure.utils

import java.time.DayOfWeek
import java.time.LocalDate
import java.time.temporal.ChronoUnit
import java.time.temporal.TemporalAdjusters
import java.util.stream.Collectors
import java.util.stream.LongStream

class DatesProvider {
    fun getDates(): List<LocalDate> {
        val startDate = LocalDate.ofYearDay(2023, 1)
        val endDate = LocalDate.now().plusMonths(1)

        return toDateList(startDate, endDate)
    }

    fun getCurrentWeek(): List<LocalDate> {
        val now = LocalDate.now()

        val start = now.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY))
        val end = now.with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY))

        return toDateList(start, end)
    }

    private fun toDateList(
        startDate: LocalDate,
        endDate: LocalDate
    ): List<LocalDate> {
        val days = startDate.until(endDate, ChronoUnit.DAYS)

        return LongStream.rangeClosed(0, days)
            .mapToObj { daysToAdd: Long ->
                startDate.plusDays(daysToAdd)
            }
            .collect(Collectors.toList())
    }
}
