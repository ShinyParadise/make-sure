package com.makesure.presentation.navigation.flows

import com.makesure.data.dto.user.User
import java.time.LocalDate

data class ContactUiFlow(
    val onCloseClick: () -> Unit = {},
    val onUserBlock: (User) -> Unit = {},
    val onShareLatestTest: () -> Unit = {},
    val onDateAdded: (LocalDate) -> Unit = {},
    val onDateAdd: () -> Unit = {},
)
