package com.makesure.presentation.navigation.flows

import com.makesure.domain.entities.CountryCode

data class PhoneUiFlow(
    val updatePhone: (String) -> Unit = {},
    val updateCountryCode: (CountryCode) -> Unit = {},
    val onPhoneEntered: () -> Unit = {},
)

data class CodeUiFlow(
    val updateCode: (String) -> Unit = {},
    val onCodeEntered: () -> Unit = {},
    val onResendCode: () -> Unit = {},
)

data class EmailUiFlow(
    val updateEmail: (String) -> Unit = {},
    val onEmailEntered: () -> Unit = {},
)
