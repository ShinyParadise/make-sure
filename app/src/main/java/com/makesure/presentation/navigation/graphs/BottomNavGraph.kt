package com.makesure.presentation.navigation.graphs

import androidx.annotation.StringRes
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.composable
import androidx.navigation.navigation
import com.makesure.R
import com.makesure.presentation.navigation.flows.MainUiFlow
import com.makesure.presentation.screens.main.MainViewModel
import com.makesure.presentation.screens.main.bottomNavScreens.contacts.ContactsScreen
import com.makesure.presentation.screens.main.bottomNavScreens.home.HomeScreen
import com.makesure.presentation.screens.main.bottomNavScreens.scan.ScanScreen
import com.makesure.presentation.screens.main.bottomNavScreens.tests.TestsScreen


fun NavGraphBuilder.bottomNavGraph(
    navController: NavHostController,
    uiState: MainViewModel.MainUiState,
    uiFlow: MainUiFlow
) {
    navigation(
        route = Graphs.BOTTOM_NAV_ROUTE,
        startDestination = MainBottomBar.Home.route
    ) {
        composable(route = MainBottomBar.Home.route) {
            HomeScreen(
                uiState = uiState,
                onSelectPhoto = uiFlow.onSelectPhoto,
            )
        }

        composable(route = MainBottomBar.Tests.route) {
            TestsScreen(
                uiState = uiState,
                onCalendarClick = uiFlow.onCalendarClick,
            )
        }

        composable(route = MainBottomBar.Scan.route) {
            ScanScreen(
                onCloseClick = navController::popBackStack,
                navController = navController,
            )
        }

        composable(route = MainBottomBar.Contacts.route) {
            ContactsScreen(
                uiState = uiState,
                uiFlow = uiFlow
            )
        }
    }
}

sealed class MainBottomBar(
    val route: String,
    @StringRes val title: Int,
    val iconId: Int
) {
    object Home : MainBottomBar(
        route = HOME,
        title = R.string.home_screen_title,
        iconId = R.drawable.home_icon
    )
    object Tests : MainBottomBar(
        route = TESTS,
        title = R.string.tests_screen_title,
        iconId = R.drawable.test_icon
    )
    object Scan : MainBottomBar(
        route = SCAN,
        title = R.string.scan_screen_title,
        iconId = R.drawable.scan_icon
    )
    object Contacts : MainBottomBar(
        route = CONTACTS,
        title = R.string.contacts_screen_title,
        iconId = R.drawable.contacts_icon
    )


    companion object {
        private const val HOME = "home"
        private const val TESTS = "tests"
        private const val SCAN = "scan"
        private const val CONTACTS = "contacts"
    }
}
