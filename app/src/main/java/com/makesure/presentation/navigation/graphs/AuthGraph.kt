package com.makesure.presentation.navigation.graphs

import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.composable
import androidx.navigation.navigation
import com.makesure.presentation.screens.register.RegisterScreen
import com.makesure.presentation.screens.signIn.SignInScreen
import com.makesure.presentation.screens.welcome.WelcomeScreen

fun NavGraphBuilder.authGraph(
    navController: NavHostController,
) {
    navigation(
        route = Graphs.AUTH_ROUTE,
        startDestination = AuthGraph.WELCOME
    ) {
        composable(AuthGraph.WELCOME) {
            WelcomeScreen(
                onSignWithNumberClicked = {
                    navController.navigate(AuthGraph.SIGN_IN)
                },
                onGetStarted = {
                    navController.navigate(AuthGraph.REGISTER)
                }
            )
        }

        composable(AuthGraph.SIGN_IN) {
            SignInScreen(navController = navController)
        }

        composable(AuthGraph.REGISTER) {
            RegisterScreen(navController = navController)
        }
    }
}

object AuthGraph {
    const val WELCOME = "welcome"
    const val REGISTER = "register"
    const val SIGN_IN = "sign_in"
}
