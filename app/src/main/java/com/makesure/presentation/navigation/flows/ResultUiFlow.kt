package com.makesure.presentation.navigation.flows

import com.makesure.data.dto.user.User

data class ResultUiFlow(
    val onResultErrorClick: () -> Unit = {},
    val onResultReceived: () -> Unit = {},
    val onPositiveContinue: () -> Unit = {},
    val onTipsContinue: () -> Unit = {},
    val onSendNotificationsClick: (List<User>) -> Unit = {},
    val onGoBackToNotifications: () -> Unit = {},
    val onDismissNotifications: () -> Unit = {},
    val onAlertReceived: () -> Unit = {},
)
