package com.makesure.presentation.navigation.flows

data class ProfileUiFlow(
    val onAddContactClick: () -> Unit = {},
    val onDismiss:  () -> Unit = {},
)
