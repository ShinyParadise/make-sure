package com.makesure.presentation.navigation.flows

import com.makesure.data.dto.notification.Notification

data class NotificationUiFlow(
    val onNotificationTap: (Notification) -> Unit = {},
    val onNotificationDelete: (Notification) -> Unit = {},
)
