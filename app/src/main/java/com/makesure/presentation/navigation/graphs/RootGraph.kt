package com.makesure.presentation.navigation.graphs

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import androidx.navigation.navDeepLink
import com.makesure.BuildConfig
import com.makesure.domain.entities.UserSession
import com.makesure.presentation.navigation.flows.MainUiFlow
import com.makesure.presentation.screens.contact.ContactProfileScreen
import com.makesure.presentation.screens.main.MainViewModel
import com.makesure.presentation.screens.notifications.NotificationsScreen
import com.makesure.presentation.screens.share.ShareScreen
import com.makesure.presentation.screens.sharedProfile.SharedProfileScreen
import com.makesure.presentation.screens.testResult.TestResultScreen
import com.makesure.utils.popUpToTop
import org.koin.androidx.compose.get

// There is a need for only 1 root graph to be here,
// because nested graphs doesn't provide proper support
// for deeplinking

@Composable
fun RootGraph(
    navController: NavHostController,
    uiState: MainViewModel.MainUiState,
    userSession: UserSession = get(),   // this is needed to improve UX
    uiFlow: MainUiFlow,
) {
    val startDestination = if (!userSession.userId.isNullOrEmpty()) {
        Graphs.BOTTOM_NAV_ROUTE
    } else Graphs.AUTH_ROUTE

    NavHost(
        navController = navController,
        route = Graphs.ROOT_ROUTE,
        startDestination = startDestination
    ) {
        authGraph(navController)

        settingsGraph(navController)

        bottomNavGraph(
            navController = navController,
            uiState = uiState,
            uiFlow = uiFlow
        )

        composable(
            route = "${MainRoutes.SHARE}/{userId}",
            arguments = listOf(navArgument("userId") { type = NavType.StringType })
        ) {
            ShareScreen(onCancelClick = navController::popBackStack)
        }

        composable(
            route = "${MainRoutes.NOTIFICATIONS}/{userId}",
            arguments = listOf(navArgument("userId") { type = NavType.StringType })
        ) {
            NotificationsScreen()
        }

        composable(
            route = "${MainRoutes.SHARED_PROFILE}/?contactId={contactId}/?inviteId={inviteId}",
            arguments = listOf(
                navArgument("contactId") {
                    type = NavType.StringType
                    nullable = true
                },
                navArgument("inviteId") {
                    type = NavType.StringType
                    nullable = true
                }
            ),
            deepLinks = listOf(
                navDeepLink { uriPattern = "${BuildConfig.DEEPLINK_BASE_URI}/{contactId}" },
                navDeepLink { uriPattern = "${BuildConfig.DEEPLINK_BASE_URI}/link/{inviteId}" }
            )
        ) {
            SharedProfileScreen(
                navController = navController
            ) {
                navController.navigate(Graphs.BOTTOM_NAV_ROUTE) {
                    popUpToTop(navController)
                }
            }
        }

        composable(
            route = "${MainRoutes.TEST_RESULTS}/{userId}/{result}",
            arguments = listOf(
                navArgument("userId") { type = NavType.StringType },
                navArgument("result") { type = NavType.StringType }
            ),
        ) {
            TestResultScreen(navController = navController)
        }

        composable(
            route = "${MainRoutes.CONTACT}/{contactId}",
            arguments = listOf(
                navArgument("contactId") { type = NavType.StringType },
            )
        ) {
            ContactProfileScreen(navController = navController)
        }
    }
}

object MainRoutes {
    const val NOTIFICATIONS = "notifications"
    const val SHARE = "share"
    const val SHARED_PROFILE = "shared_profile"
    const val TEST_RESULTS = "results"
    const val CONTACT = "contact"
}
