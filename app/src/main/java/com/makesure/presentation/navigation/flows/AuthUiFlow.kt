package com.makesure.presentation.navigation.flows

import com.makesure.data.dto.user.NewUser

data class AuthUiFlow(
    val onSignWithNumberClick: () -> Unit,
    val onGetStartedClick: () -> Unit,
    val onRegisterComplete: (newUser: NewUser) -> Unit,
    val onSignInComplete: (phone: String) -> Unit,
)
