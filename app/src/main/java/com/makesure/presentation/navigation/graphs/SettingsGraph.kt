package com.makesure.presentation.navigation.graphs

import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.composable
import androidx.navigation.compose.navigation
import androidx.navigation.navArgument
import com.makesure.utils.sharedViewModel
import com.makesure.presentation.navigation.flows.CodeUiFlow
import com.makesure.presentation.navigation.flows.EmailUiFlow
import com.makesure.presentation.navigation.flows.PhoneUiFlow
import com.makesure.presentation.navigation.flows.SettingsUiFlow
import com.makesure.presentation.screens.settings.components.BlacklistScreen
import com.makesure.presentation.screens.settings.components.ChangeEmailScreen
import com.makesure.presentation.screens.settings.components.ChangeNumberScreen
import com.makesure.presentation.screens.settings.SettingsScreen
import com.makesure.presentation.screens.settings.SettingsViewModel

fun NavGraphBuilder.settingsGraph(
    navController: NavHostController
) {
    navigation(
        route = "${Graphs.SETTINGS_ROUTE}/{userId}",
        startDestination = "${SettingsGraph.SETTINGS_LIST}/{userId}",
        arguments = listOf(navArgument("userId") { type = NavType.StringType })
    ) {
        composable(
            route = "${SettingsGraph.SETTINGS_LIST}/{userId}",
            arguments = listOf(navArgument("userId") { type = NavType.StringType })
        ) {
            val viewModel = it.sharedViewModel<SettingsViewModel>(navController)

            SettingsScreen(
                uiFlow = getUiFlowForViewModel(viewModel, navController),
                uiState = viewModel.uiState
            )
        }

        composable(
            route = "${SettingsGraph.CHANGE_PHONE}/{userId}",
            arguments = listOf(navArgument("userId") { type = NavType.StringType })
        ) {
            val viewModel = it.sharedViewModel<SettingsViewModel>(navController)

            ChangeNumberScreen(
                uiState = viewModel.uiState,
                uiFlow = getUiFlowForViewModel(viewModel, navController)
            )
        }

        composable(
            route = "${SettingsGraph.CHANGE_EMAIL}/{userId}",
            arguments = listOf(navArgument("userId") { type = NavType.StringType })
        ) {
            val viewModel = it.sharedViewModel<SettingsViewModel>(navController)

            ChangeEmailScreen(
                uiState = viewModel.uiState,
                uiFlow = getUiFlowForViewModel(viewModel, navController)
            )
        }

        composable(
            route = "${SettingsGraph.BLACKLIST}/{userId}",
            arguments = listOf(navArgument("userId") { type = NavType.StringType })
        ) {
            val viewModel = it.sharedViewModel<SettingsViewModel>(navController)

            BlacklistScreen(
                uiState = viewModel.uiState,
                uiFlow = getUiFlowForViewModel(viewModel, navController)
            )
        }
    }
}

private fun getUiFlowForViewModel(
    viewModel: SettingsViewModel,
    navController: NavHostController
): SettingsUiFlow {
    return SettingsUiFlow(
        onNotificationsClick = viewModel::notificationStateClicked,
        onAddEmailClick = { viewModel.changeEmailClicked(navController) },
        onChangeNumberClick = { viewModel.changeNumberClicked(navController) },
        onPhoneEditingFinished = { viewModel.phoneEditingFinished(navController) },
        onEmailEditingFinished = { viewModel.emailEditingFinished(navController) },
        onChangeLanguageClick = viewModel::changeLanguage,
        onBackClick = { viewModel.backClicked(navController) },
        onBlacklistClick = { viewModel.blacklistClicked(navController) },
        onSignOut = { viewModel.signOut(navController) },
        onUnblockUser = viewModel::unblock,
        onDeleteProfileClick = { viewModel.deleteProfile(navController) },

        phone = PhoneUiFlow(
            updateCountryCode = viewModel::updateCountryCode,
            updatePhone = viewModel::updateNumber,
            onPhoneEntered = viewModel::phoneEntered
        ),
        code = CodeUiFlow(
            updateCode = viewModel::updateCode,
            onResendCode = {},
            onCodeEntered = viewModel::codeEntered
        ),
        email = EmailUiFlow(
            updateEmail = viewModel::updateEmail,
            onEmailEntered = viewModel::emailEntered
        )
    )
}

object SettingsGraph {
    const val SETTINGS_LIST = "settings_list"
    const val CHANGE_EMAIL = "settings_email"
    const val CHANGE_PHONE = "settings_phone"
    const val BLACKLIST = "blacklist"
}
