package com.makesure.presentation.navigation.flows

import androidx.navigation.NavHostController

data class ScanUiFlow(
    val onRetry: () -> Unit = {},
    val onSend: () -> Unit = {},
    val selectQrScanner: () -> Unit = {},
    val selectTestScanner: () -> Unit = {},
    val onResultReceived: () -> Unit = {},
    val onQrCodeScanned: (String, NavHostController) -> Unit = { _, _ -> },
    val onBackClick: () -> Unit = {},
)
