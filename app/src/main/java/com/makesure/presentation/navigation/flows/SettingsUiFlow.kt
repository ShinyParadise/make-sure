package com.makesure.presentation.navigation.flows

import com.makesure.data.dto.user.User

data class SettingsUiFlow(
    val onNotificationsClick: () -> Unit = {},

    val onAddEmailClick: () -> Unit = {},
    val onChangeNumberClick: () -> Unit = {},
    val onChangeLanguageClick: () -> Unit = {},
    val onPhoneEditingFinished: () -> Unit = {},
    val onBlacklistClick: () -> Unit = {},
    val onDeleteProfileClick: () -> Unit = {},
    val onEmailEditingFinished: () -> Unit = {},
    val onUnblockUser: (User) -> Unit = {},
    val onSignOut: () -> Unit = {},
    val onBackClick: () -> Unit = {},

    val phone: PhoneUiFlow = PhoneUiFlow(),
    val code: CodeUiFlow = CodeUiFlow(),
    val email: EmailUiFlow = EmailUiFlow(),
)
