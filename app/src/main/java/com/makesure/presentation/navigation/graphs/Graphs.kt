package com.makesure.presentation.navigation.graphs

object Graphs {
    const val ROOT_ROUTE = "root_route"
    const val BOTTOM_NAV_ROUTE = "bottom_nav_route"
    const val AUTH_ROUTE = "auth_route"
    const val SETTINGS_ROUTE = "settings_route"
}
