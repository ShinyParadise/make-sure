package com.makesure.presentation.navigation.flows

data class TopBarUiFlow(
    val onQrClick: () -> Unit = {},
    val onSettingsClick: () -> Unit = {},
    val onBellClick: () -> Unit = {},
    val onEditClick: () -> Unit = {},
    val onCalendarClick: () -> Unit = {},
)
