package com.makesure.presentation.navigation.flows

import android.net.Uri
import com.makesure.data.dto.test.Test
import com.makesure.data.dto.user.User
import com.makesure.presentation.screens.main.ContactAction

data class MainUiFlow(
    val onTestClick: (Test) -> Unit = {},
    val onProfileClick: () -> Unit = {},
    val onTipClick: () -> Unit = {},
    val onChangeSort: () -> Unit = {},
    val onCalendarClick: () -> Unit = {},
    val onLinkClick: suspend () -> String? = { "" },
    val onContactClick: (User) -> Unit = {},
    val onContactActionSelected: (
        user: User,
        action: ContactAction,
    ) -> Unit = {_,_ ->},
    val onLogin: (String) -> Unit = {},
    val onReport: (report: String, contact: User) -> Unit = { _, _ ->},
    val onSelectPhoto: (Uri) -> Unit = {}
)
