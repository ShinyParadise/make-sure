package com.makesure.presentation.components.views.buttons

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Reply
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.makesure.app.theme.MakeSureTheme
import com.makesure.app.theme.Primary

@Composable
fun FilledButton(
    modifier: Modifier = Modifier,
    icon: ImageVector? = null,
    text: String,
    height: Dp = 50.dp,
    backgroundColor: Color = Color.White,
    contentColor: Color = Primary,
    onClick: () -> Unit
) {
    Button(
        onClick = onClick,
        shape = MaterialTheme.shapes.medium,
        colors = ButtonDefaults.buttonColors(
            contentColor = contentColor,
            backgroundColor = backgroundColor
        ),
        modifier = modifier
            .fillMaxWidth()
            .height(height)
    ) {
        icon?.let {
            Icon(
                imageVector = it,
                contentDescription = null,
                modifier = Modifier.size(24.dp)
            )
            Spacer(Modifier.width(10.dp))
        }

        Text(
            text = text,
            fontSize = 16.sp,
            fontWeight = FontWeight(800),
        )
    }
}

@Preview
@Composable
private fun FilledButton_Preview() {
    MakeSureTheme {
        FilledButton(icon = Icons.Default.Reply, text = "SHARE TEST") {}
    }
}
