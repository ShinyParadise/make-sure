package com.makesure.presentation.components.views.userInput

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.makesure.R
import com.makesure.presentation.components.views.Title
import com.makesure.presentation.components.views.buttons.BackButton
import com.makesure.presentation.components.views.buttons.MainButton
import com.makesure.app.theme.MakeSureTheme
import com.makesure.app.theme.mainBlueColor
import com.makesure.app.theme.mulishFonts

@Composable
fun CodeEnterContent(
    number: String,
    code: String,
    updateCode: (String) -> Unit,
    onResend: () -> Unit,
    isResendEnabled: Boolean,
    onContinue: () -> Unit,
    onBackClick: () -> Unit,
    modifier: Modifier = Modifier,
    shouldContinue: Boolean
) {
    BackHandler(onBack = onBackClick)
    Surface {
        Box(
            modifier
                .fillMaxSize()
                .padding(horizontal = 28.dp)
        ) {
            Column {
                BackButton(onClick = onBackClick)
                Spacer(Modifier.height(45.dp))

                Title(
                    text = stringResource(id = R.string.code_enter_title),
                    color = MaterialTheme.colors.primary
                )
                Spacer(Modifier.height(10.dp))
                Text(
                    text = number,
                    fontSize = 16.sp,
                    color = MaterialTheme.colors.onSurface.copy(alpha = 0.65f)
                )
                Spacer(Modifier.height(13.dp))

                BasicTextField(
                    value = code,
                    onValueChange = updateCode,
                    singleLine = true,
                    enabled = true,
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                    decorationBox = {
                        Row(
                            modifier = Modifier.fillMaxWidth(),
                            horizontalArrangement = Arrangement.SpaceAround
                        ) {
                            repeat(6) {
                                val text: Char =
                                    if (code.length - 1 >= it) code[it]
                                    else ' '
                                CharView(text = text)
                            }
                        }
                    }
                )
            }


            Column(
                modifier = Modifier.align(Alignment.BottomCenter),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                TextButton(
                    onClick = onResend,
                    enabled = isResendEnabled
                ) {
                    Text(
                        text = stringResource(R.string.btn_resend),
                        style = TextStyle(
                            fontSize = 14.sp,
                            fontWeight = FontWeight(600),
                            color = mainBlueColor,
                            textAlign = TextAlign.Center,
                        ),
                    )
                }
                MainButton(
                    onClick = onContinue,
                    text = stringResource(id = R.string.btn_continue),
                    isEnabled = shouldContinue,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 25.dp)
                )
            }
        }
    }
}

@Composable
private fun CharView(text: Char) {
    Column {
        Text(
            text = text.toString(),
            style = TextStyle(
                fontSize = 48.sp,
                fontWeight = FontWeight.Light,
                fontFamily = mulishFonts,
                color = MaterialTheme.colors.primary,
                letterSpacing = 20.sp
            ),
        )
        Box(modifier = Modifier
            .size(width = 48.dp, height = 2.dp)
            .background(Color(0x61000000))
        )
    }
}

@Preview
@Composable
fun CodeEnterContent_Preview() {
    MakeSureTheme {
        CodeEnterContent(
            number = "+79996915101",
            code = "123456",
            updateCode = {},
            onResend = {},
            onContinue = {},
            onBackClick = {},
            shouldContinue = true,
            isResendEnabled = true,
        )
    }
}

@Preview(locale = "RU")
@Composable
fun CodeEnterContent_PreviewRU() {
    MakeSureTheme {
        CodeEnterContent(
            number = "+79996915101",
            code = "123456",
            updateCode = {},
            onResend = {},
            onContinue = {},
            onBackClick = {},
            shouldContinue = true,
            isResendEnabled = false,
        )
    }
}

