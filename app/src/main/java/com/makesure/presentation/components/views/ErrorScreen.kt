package com.makesure.presentation.components.views

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.makesure.app.theme.MakeSureTheme
import com.makesure.app.theme.mulishFonts

@Composable
fun ErrorScreen() {
    Box(modifier = Modifier.fillMaxSize().padding(16.dp), contentAlignment = Alignment.Center) {
        Text(
            text = "Произошла ошибка, повторите позже",
            fontFamily = mulishFonts,
            fontSize = 32.sp
        )
    }
}


@Preview
@Composable
fun ErrorScreen_Preview() {
    MakeSureTheme {
        ErrorScreen()
    }
}
