package com.makesure.presentation.components.views.userInput

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawWithCache
import androidx.compose.ui.graphics.BlendMode
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.makesure.R
import com.makesure.presentation.components.views.SmallText
import com.makesure.presentation.components.views.buttons.BackButton
import com.makesure.presentation.components.views.buttons.MainButton
import com.makesure.app.theme.MakeSureTheme
import com.makesure.app.theme.Primary

@Composable
fun AgreementContent(
    modifier: Modifier = Modifier,
    onContinue: () -> Unit,
    onBackClick: () -> Unit,
) {
    BackHandler(onBack = onBackClick)
    Surface {
        Box(modifier = modifier
            .fillMaxSize()
            .padding(horizontal = 25.dp)
        ) {
            Column {
                BackButton(onClick = onBackClick)
                Spacer(Modifier.height(30.dp))

                HeaderSection()
                Spacer(Modifier.height(40.dp))

                RulesSection()
            }

            MainButton(
                onClick = onContinue,
                text = stringResource(id = R.string.btn_agree),
                modifier = Modifier
                    .fillMaxWidth()
                    .align(Alignment.BottomCenter)
                    .padding(vertical = 25.dp)
            )
        }
    }
}

@Composable
private fun RulesSection() {
    val paddingValue = 34.dp
    Rule(
        rule = stringResource(id = R.string.rule_1),
        explanation = stringResource(id = R.string.explanation_1)
    )
    Spacer(Modifier.height(paddingValue))
    Rule(
        rule = stringResource(id = R.string.rule_2),
        explanation = stringResource(id = R.string.explanation_2)
    )
    Spacer(Modifier.height(paddingValue))
    Rule(
        rule = stringResource(id = R.string.rule_3),
        explanation = stringResource(id = R.string.explanation_3)
    )
    Spacer(Modifier.height(paddingValue))
    Rule(
        rule = stringResource(id = R.string.rule_4),
        explanation = stringResource(id = R.string.explanation_4)
    )
}

@Composable
private fun Rule(
    rule: String,
    explanation: String,
) {
    Row(verticalAlignment = Alignment.CenterVertically) {
        Icon(
            painter = painterResource(id = R.drawable.logo),
            contentDescription = null,
            modifier = Modifier
                .size(24.dp)
                .graphicsLayer(alpha = 0.99f)
                .drawWithCache {
                    onDrawWithContent {
                        drawContent()
                        drawRect(
                            Primary,
                            blendMode = BlendMode.SrcAtop
                        )
                    }
                }
        )
        Spacer(Modifier.width(7.dp))
        Text(
            text = rule,
            fontSize = 18.sp,
            color = MaterialTheme.colors.primary,
            fontWeight = FontWeight(700),
        )
    }
    Text(
        text = explanation,
        fontSize = 13.sp,
        fontWeight = FontWeight(600),
        color = Color(0xA6000000),
    )
}

@Composable
private fun HeaderSection() {
    Text(
        text = stringResource(id = R.string.agreement_title),
        color = MaterialTheme.colors.primary,
        fontWeight = FontWeight.Bold,
        fontSize = 23.sp
    )
    Spacer(Modifier.height(5.dp))
    SmallText(
        text = stringResource(id = R.string.agreement_warning),
    )
}

@Preview
@Composable
fun AgreementContent_Preview() {
    MakeSureTheme {
        AgreementContent(
            onContinue = {},
            onBackClick = {}
        )
    }
}

@Preview(locale = "RU")
@Composable
fun AgreementContent_PreviewRU() {
    MakeSureTheme {
        AgreementContent(
            onContinue = {},
            onBackClick = {}
        )
    }
}
