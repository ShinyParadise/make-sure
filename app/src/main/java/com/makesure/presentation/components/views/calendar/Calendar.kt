package com.makesure.presentation.components.views.calendar

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.lazy.grid.rememberLazyGridState
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Card
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowLeft
import androidx.compose.material.icons.filled.KeyboardArrowRight
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.navigation.NavHostController
import com.makesure.app.theme.MakeSureTheme
import com.makesure.domain.entities.CalendarDayContent
import com.makesure.presentation.components.views.CircleClickableUserPhoto
import com.makesure.presentation.components.views.CircleUserPhoto
import com.makesure.presentation.navigation.graphs.MainRoutes
import com.makesure.utils.DatesProvider
import org.koin.androidx.compose.koinViewModel
import java.text.DateFormatSymbols
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.Collections

@Composable
fun CalendarPopup(
    navController: NavHostController,
    modifier: Modifier = Modifier,
    viewModel: CalendarViewModel = koinViewModel(),
    onDismiss: () -> Unit
) {
    val uiState = viewModel.uiState

    if (uiState.isLoading) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .background(color = Color.Black.copy(alpha = 0.5f)),
            contentAlignment = Alignment.Center
        ) {
            CircularProgressIndicator()
        }
    } else {
        Dialog(onDismissRequest = onDismiss) {
            Calendar(
                modifier = modifier,
                uiState = viewModel.uiState,
                uiFlow = CalendarUiFlow(
                    onLeftArrowClick = viewModel::leftArrowClicked,
                    onRightArrowClick = viewModel::rightArrowClicked,
                    onSelectDay = viewModel::selectDay,
                    onProfileClick = {
                        navController.navigate("${MainRoutes.CONTACT}/${it.id}")
                        onDismiss()
                    }
                )
            )
        }
    }
}

@Composable
fun Calendar(
    modifier: Modifier = Modifier,
    uiState: CalendarViewModel.CalendarUiState,
    uiFlow: CalendarUiFlow,
    elevation: Dp = 0.dp,
) {
    val formatter = DateTimeFormatter.ofPattern("MMMM',' yyyy")

    Card(
        modifier = modifier.fillMaxWidth(),
        elevation = elevation,
        backgroundColor = MaterialTheme.colors.background,
        contentColor = MaterialTheme.colors.onBackground,
        shape = MaterialTheme.shapes.small
    ) {
        Column(
            Modifier.padding(vertical = 11.dp, horizontal = 16.dp)
        ) {
            HeaderSection(
                monthWithYear = uiState.currentDate.format(formatter),
                onLeftArrowClick = uiFlow.onLeftArrowClick,
                onRightArrowClick = uiFlow.onRightArrowClick
            )

            CalendarSection(uiState, uiFlow)
        }
    }
}

@Composable
private fun HeaderSection(
    modifier: Modifier = Modifier,
    monthWithYear: String,
    onChangeClick: () -> Unit = {},
    onLeftArrowClick: () -> Unit = {},
    onRightArrowClick: () -> Unit = {},
) {
    val weekDays = DateFormatSymbols().shortWeekdays.toMutableList()
    Collections.rotate(weekDays, -2)

    Column(
        modifier = modifier
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = monthWithYear,
                fontSize = 22.sp,
                fontWeight = FontWeight.Medium
            )
            IconButton(onClick = onChangeClick) {
                Icon(
                    imageVector = Icons.Default.KeyboardArrowRight,
                    tint = MaterialTheme.colors.primary,
                    contentDescription = null,
                    modifier = Modifier.size(22.dp)
                )
            }
            Spacer(Modifier.weight(1f))

            ArrowButtons(onLeftArrowClick, onRightArrowClick)
        }

        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceAround
        ) {
            for (item in weekDays) {
                Text(
                    text = item.uppercase(),
                    color = Color(0x4D3C3C43),
                    fontSize = 16.sp,
                )
            }
        }
    }
}

@Composable
private fun ArrowButtons(onLeftArrowClick: () -> Unit, onRightArrowClick: () -> Unit) {
    IconButton(onClick = onLeftArrowClick) {
        Icon(
            imageVector = Icons.Default.KeyboardArrowLeft,
            tint = MaterialTheme.colors.primary,
            contentDescription = null,
            modifier = Modifier.size(30.dp)
        )
    }
    IconButton(onClick = onRightArrowClick) {
        Icon(
            imageVector = Icons.Default.KeyboardArrowRight,
            tint = MaterialTheme.colors.primary,
            contentDescription = null,
            modifier = Modifier.size(30.dp)
        )
    }
}

@Composable
private fun CalendarSection(
    uiState: CalendarViewModel.CalendarUiState,
    uiFlow: CalendarUiFlow,
) {
    val state = rememberLazyGridState()
    val now by remember { derivedStateOf { LocalDate.now() } }

    val firstDate = uiState.currentMonthDates.firstOrNull()?.date
    val shiftMultiplier = when (firstDate?.dayOfWeek) {
        DayOfWeek.TUESDAY -> 1
        DayOfWeek.WEDNESDAY -> 2
        DayOfWeek.THURSDAY -> 3
        DayOfWeek.FRIDAY -> 4
        DayOfWeek.SATURDAY -> 5
        DayOfWeek.SUNDAY -> 6
        else -> 0
    }

    Column {
        LazyVerticalGrid(
            columns = GridCells.Fixed(7),
            horizontalArrangement = Arrangement.SpaceBetween,
            userScrollEnabled = false,
            state = state,
        ) {
            for (i in 1..shiftMultiplier) { item {} }

            items(uiState.currentMonthDates) {
                val isEnabled = it.date <= now
                val isSelected = it == uiState.currentDateContent

                DayItem(
                    modifier = Modifier
                        .padding(vertical = 12.dp, horizontal = 2.dp)
                        .height(40.dp)
                        .clip(CircleShape)
                        .selectable(
                            selected = isSelected,
                            enabled = isEnabled,
                            onClick =  {
                                uiFlow.onSelectDay(it.date)
                            }
                        ),
                    dayContent = it,
                    isEnabled = isEnabled,
                )
            }
        }

        uiState.currentDateContent?.let { MeetingsItem(it, uiFlow) }
    }
}

@Composable
private fun MeetingsItem(
    dayContent: CalendarDayContent,
    uiFlow: CalendarUiFlow
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 20.dp),
        horizontalArrangement = Arrangement.SpaceAround
    ) {
        for (user in dayContent.meetings) {
            CircleClickableUserPhoto(
                photoURL = user.photoURL,
                sizeDp = 56.dp,
                onClick = { uiFlow.onProfileClick(user) }
            )
        }
    }
}

@Composable
private fun DayItem(
    modifier: Modifier = Modifier,
    dayContent: CalendarDayContent,
    isEnabled: Boolean,
) {
    var bgColor by remember { mutableStateOf(Color.Transparent) }
    var textColor by remember { mutableStateOf(Color.Black) }

    if (dayContent.testResult == false) {
        bgColor = Color(0xFF5FE986)
        textColor = Color.White
    } else if (dayContent.testResult == true) {
        bgColor = Color(0xFFFF9D56)
        textColor = Color.White
    } else if (dayContent.meetings.isNotEmpty()) {
        textColor = Color.White
    }

    Box(
        modifier = modifier.background(bgColor, CircleShape),
        contentAlignment = Alignment.Center
    ) {
        if (dayContent.meetings.isNotEmpty()) {
            CircleUserPhoto(
                photoURL = dayContent.meetings.last().photoURL,
                sizeDp = 40.dp,
            )
        }

        Text(
            text = dayContent.date.dayOfMonth.toString(),
            fontSize = 18.sp,
            color = if (isEnabled) textColor else Color.Black.copy(alpha = 0.3f)
        )
    }
}

@Preview
@Composable
private fun Calendar_Preview() {
    val dates = DatesProvider().getDates()
    val now = LocalDate.now()
    val currentMonthDates = dates
        .map { CalendarDayContent(it) }
        .filter { it.date.month == now.month && it.date.year == now.year }

    MakeSureTheme {
        Calendar(
            elevation = 10.dp,
            uiState = CalendarViewModel.CalendarUiState(
                currentMonthDates = currentMonthDates,
                currentDate = now,
            ),
            uiFlow = CalendarUiFlow()
        )
    }
}
