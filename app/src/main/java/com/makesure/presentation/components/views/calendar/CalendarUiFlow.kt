package com.makesure.presentation.components.views.calendar

import com.makesure.data.dto.user.User
import java.time.LocalDate

data class CalendarUiFlow(
    val onLeftArrowClick: () -> Unit = {},
    val onRightArrowClick: () -> Unit = {},
    val onSelectDay: (LocalDate) -> Unit = {},
    val onProfileClick: (User) -> Unit = {}
)
