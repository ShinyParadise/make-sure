package com.makesure.presentation.components.views

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.unit.dp
import com.makesure.app.theme.PrimaryGradient

@Composable
fun GradientCard(
    modifier: Modifier = Modifier,
    paddingValues: PaddingValues = PaddingValues(),
    brush: Brush = Brush.linearGradient(PrimaryGradient),
    content: @Composable BoxScope.() -> Unit,
) {
    Box(
        modifier = modifier
            .fillMaxSize()
            .clip(RoundedCornerShape(20.dp, 20.dp, 0.dp, 0.dp))
            .background(brush)
            .padding(paddingValues)
    ) { content() }
}

@Composable
fun GradientBox(
    modifier: Modifier = Modifier,
    paddingValues: PaddingValues = PaddingValues(),
    brush: Brush = Brush.linearGradient(PrimaryGradient),
    content: @Composable BoxScope.() -> Unit,
) {
    Box(
        modifier = modifier
            .fillMaxSize()
            .background(brush)
            .padding(paddingValues)
    ) { content() }
}
