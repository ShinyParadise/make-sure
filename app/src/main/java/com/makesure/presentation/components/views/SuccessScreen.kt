package com.makesure.presentation.components.views

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.makesure.R
import com.makesure.presentation.components.views.buttons.MainButton
import com.makesure.app.theme.MakeSureTheme


@Composable
fun SuccessScreen(
    title: String,
    subtitle: String,
    shouldContinue: Boolean,
    onContinue: () -> Unit,
) {
    Box(modifier = Modifier
        .fillMaxSize()
        .padding(horizontal = 28.dp)
    ) {
        Column(Modifier.padding(top = 100.dp)) {
            Title(text = title, fontSize = 34.sp)
            Text(
                text = subtitle,
                fontSize = 26.sp,
                fontWeight = FontWeight.Normal
            )
        }

        MainButton(
            onClick = onContinue,
            text = stringResource(id = R.string.btn_continue),
            isEnabled = shouldContinue,
            modifier = Modifier
                .align(Alignment.BottomCenter)
                .padding(bottom = 28.dp)
        )
    }
}

@Preview(showSystemUi = true)
@Composable
private fun SuccessScreen_Preview() {
    MakeSureTheme {
        SuccessScreen("123", "1234", true) {}
    }
}
