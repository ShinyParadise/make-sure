package com.makesure.presentation.components.views.calendar

import android.os.Parcelable
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.SavedStateHandleSaveableApi
import androidx.lifecycle.viewmodel.compose.saveable
import com.makesure.data.dto.user.User
import com.makesure.domain.entities.CalendarDayContent
import com.makesure.domain.interactors.MeetingsInteractor
import com.makesure.domain.interactors.TestsInteractor
import com.makesure.domain.interactors.UserInteractor
import com.makesure.utils.DatesProvider
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.parcelize.Parcelize
import java.time.LocalDate

@OptIn(SavedStateHandleSaveableApi::class)
class CalendarViewModel(
    private val userInteractor: UserInteractor,
    private val testsInteractor: TestsInteractor,
    private val meetingsInteractor: MeetingsInteractor,
    private val mainDispatcher: CoroutineDispatcher,
    private val bgDispatcher: CoroutineDispatcher,
    datesProvider: DatesProvider,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    private var calendarDays: List<CalendarDayContent> = datesProvider.getDates().map {
        CalendarDayContent(it)
    }

    var uiState by savedStateHandle.saveable { mutableStateOf(CalendarUiState()) }
        private set

    @Parcelize
    data class CalendarUiState(
        val currentMonthDates: List<CalendarDayContent> = emptyList(),
        val currentDate: LocalDate = LocalDate.now(),
        val isLoading: Boolean = true,
        val currentDateContent: CalendarDayContent? = null,
        val user: User? = null
    ) : Parcelable

    init {
        viewModelScope.launch(bgDispatcher) {
            val user = userInteractor.getUser()

            withContext(mainDispatcher) {
                uiState = uiState.copy(user = user)
            }

            val tests = testsInteractor.getUserTestSets()
            val meetingsByDate = meetingsInteractor.getUserMeetingsByDate()

            calendarDays = calendarDays.map { dayContent ->
                val testSet = tests.firstOrNull { dayContent.date == it.date.toLocalDate() }
                val meetings = meetingsByDate.getOrDefault(dayContent.date, emptyList())

                dayContent.copy(
                    testSet = testSet,
                    testResult = testSet?.isPositive(),
                    meetings = meetings
                )
            }

            val currentDates = calendarDays.filterByMonth(uiState.currentDate)

            withContext(mainDispatcher) {
                uiState = uiState.copy(
                    currentMonthDates = currentDates,
                    user = user,
                    isLoading = false
                )
                updateCurrentDayContent()
            }
        }
    }

    fun leftArrowClicked() {
        decreaseMonth()
    }

    fun rightArrowClicked() {
        increaseMonth()
    }

    fun selectDay(day: LocalDate) {
        uiState = uiState.copy(currentDate = day)
        updateCurrentDayContent()
    }

    private fun increaseMonth() {
        if (isCurrentMonthLast()) return

        val newDate = uiState.currentDate.plusMonths(1)
        val currentDates = calendarDays.filterByMonth(newDate)

        uiState = uiState.copy(
            currentDate = newDate,
            currentMonthDates = currentDates,
        )
        updateCurrentDayContent()
    }

    private fun decreaseMonth() {
        if (isCurrentMonthFirst()) return

        val newDate = uiState.currentDate.minusMonths(1)
        val currentDates = calendarDays.filterByMonth(newDate)

        uiState = uiState.copy(
            currentDate = newDate,
            currentMonthDates = currentDates,
        )
        updateCurrentDayContent()
    }

    private fun isCurrentMonthLast() = (uiState.currentDate.year == calendarDays.last().date.year
            && uiState.currentDate.month == calendarDays.last().date.month)

    private fun isCurrentMonthFirst() =
        uiState.currentDate.year == calendarDays.first().date.year
                && uiState.currentDate.month == calendarDays.first().date.month

    private fun updateCurrentDayContent() {
        val newContent = uiState.currentMonthDates.find {
            it.date == uiState.currentDate
        }

        uiState = uiState.copy(currentDateContent = newContent)
    }
}

private fun List<CalendarDayContent>.filterByMonth(date: LocalDate): List<CalendarDayContent> {
    return this.filter {
        it.date.month == date.month && it.date.year == date.year
    }
}
