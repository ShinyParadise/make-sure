package com.makesure.presentation.components.views.buttons

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBackIosNew
import androidx.compose.material.icons.filled.Block
import androidx.compose.material.icons.filled.Close
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.makesure.R
import com.makesure.app.theme.MakeSureTheme

@Composable
fun BackButton(
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    TextButton(
        onClick = onClick,
        colors = ButtonDefaults.textButtonColors(
            contentColor = MaterialTheme.colors.primary
        ),
        modifier = modifier.offset(x = (-12).dp)
    ) {
        Box(Modifier.padding(12.dp)) {
            Icon(
                imageVector = Icons.Default.ArrowBackIosNew,
                contentDescription = null,
                modifier = Modifier.size(20.dp)
            )
        }
        Text(
            text = stringResource(id = R.string.btn_back),
            fontSize = 22.sp,
            fontWeight = FontWeight(600)
        )
    }
}

@Composable
fun BackWithSkipButtons(onBackClick: () -> Unit, onSkip: () -> Unit) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier.fillMaxWidth()
    ) {
        BackButton(onClick = onBackClick)
        SkipButton(
            onClick = onSkip,
            modifier = Modifier
        )
    }
}

@Composable
fun SkipButton(
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    TextButton(
        onClick = onClick,
        colors = ButtonDefaults.textButtonColors(
            contentColor = Color(0xFF585858)
        ),
        modifier = modifier
    ) {
        Text(
            text = stringResource(id = R.string.btn_skip),
            fontSize = 16.sp
        )
    }
}

@Composable
fun CloseButton(
    modifier: Modifier = Modifier,
    sizeDp: Dp = 24.dp,
    color: Color = Color.White,
    onClick: () -> Unit,
) {
    IconButton(
        onClick = onClick,
    ) {
        Icon(
            modifier = modifier.size(sizeDp),
            imageVector = Icons.Default.Close,
            tint = color,
            contentDescription = null
        )
    }
}


@Composable
fun BlockButton(
    modifier: Modifier = Modifier,
    sizeDp: Dp = 24.dp,
    color: Color = Color.White,
    onClick: () -> Unit,
) {
    IconButton(onClick = onClick) {
        Icon(
            modifier = modifier.size(sizeDp),
            imageVector = Icons.Default.Block,
            contentDescription = null,
            tint = color,
        )
    }
}

@Preview
@Composable
private fun SkipButton_Preview() {
    MakeSureTheme {
        SkipButton({})
    }
}

@Preview
@Composable
private fun BackButtonPreview() {
    MakeSureTheme {
        BackButton(
            onClick = {}
        )
    }
}
