package com.makesure.presentation.components.views

import android.net.Uri
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.makesure.app.theme.MakeSureTheme
import com.makesure.app.theme.PrimaryGradient
import com.makesure.data.dto.user.User
import com.makesure.presentation.components.previewUser
import com.makesure.presentation.screens.main.bottomNavScreens.home.AgeSection
import com.makesure.presentation.screens.main.bottomNavScreens.home.ProfileSection
import com.makesure.presentation.screens.main.bottomNavScreens.home.TestsDoneSection

@Composable
fun ProfileCard(
    modifier: Modifier = Modifier,
    user: User,
    testCount: Int,
    onSelectPhoto: (Uri) -> Unit,
) {
    CardContainer(
        height = 140.dp,
        modifier = modifier
    ) {
        Row(
            modifier = Modifier.fillMaxHeight(),
            verticalAlignment = Alignment.Bottom
        ) {
            TestsDoneSection(modifier = Modifier.weight(0.3f), testCount = testCount)
            ProfileSection(modifier = Modifier.weight(1f), user = user, onSelectPhoto = onSelectPhoto)
            AgeSection(modifier = Modifier.weight(0.3f), user = user)
        }
    }
}

@Composable
fun CardContainer(
    modifier: Modifier = Modifier,
    height: Dp,
    content: @Composable () -> Unit
) {
    Card(
        shape = MaterialTheme.shapes.small,
        modifier = modifier
            .fillMaxWidth()
            .height(height)
            .clip(MaterialTheme.shapes.small)
    ) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .background(
                    brush = Brush.linearGradient(PrimaryGradient),
                    shape = MaterialTheme.shapes.small
                )
        ) {
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(horizontal = 16.dp)
                    .padding(top = 8.dp, bottom = 15.dp),
                content = { content() }
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun CardsPreview() {
    MakeSureTheme {
       ProfileCard(user = previewUser, onSelectPhoto = {}, testCount = 10)
    }
}
