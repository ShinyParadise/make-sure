package com.makesure.presentation.components.views.userInput

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.makesure.R
import com.makesure.presentation.components.views.buttons.BackButton
import com.makesure.presentation.components.views.buttons.BackWithSkipButtons
import com.makesure.presentation.components.views.buttons.MainButton
import com.makesure.presentation.components.views.SmallText
import com.makesure.presentation.components.views.Title
import com.makesure.app.theme.MakeSureTheme

@Composable
fun EmailEnterContent(
    modifier: Modifier = Modifier,
    onSkip: () -> Unit = {},
    shouldSkip: Boolean = true,
    email: String,
    onContinue: () -> Unit,
    onBackClick: () -> Unit,
    updateEmail: (String) -> Unit,
    shouldContinue: Boolean,
) {
    BackHandler(onBack = onBackClick)
    Surface {
        Box(
            modifier = modifier
                .fillMaxSize()
                .padding(horizontal = 25.dp)
        ) {
            Column {
                if (shouldSkip) {
                    BackWithSkipButtons(onBackClick, onSkip)
                } else {
                    BackButton(onClick = onBackClick)
                }
                Spacer(Modifier.height(45.dp))

                Title(
                    text = stringResource(id = R.string.email_enter_title),
                    color = MaterialTheme.colors.primary
                )
                SmallText(text = stringResource(id = R.string.email_warning))
                Spacer(Modifier.height(75.dp))

                InputField(
                    value = email,
                    onValueChange = updateEmail,
                    modifier = Modifier.fillMaxWidth(),
                    placeholder = stringResource(id = R.string.enter_email_hint),
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Email)
                )

                Spacer(Modifier.height(16.dp))
                SmallText(stringResource(R.string.email_verify))
            }

            MainButton(
                onClick = onContinue,
                text = stringResource(id = R.string.btn_continue),
                isEnabled = shouldContinue,
                modifier = Modifier
                    .align(Alignment.BottomCenter)
                    .padding(bottom = 28.dp)
            )
        }
    }
}

@Preview
@Composable
private fun EmailEnterContent_Preview() {
    MakeSureTheme {
        EmailEnterContent(
            onContinue = {},
            onBackClick = {},
            updateEmail = {},
            email = "12345@gmail.com",
            shouldContinue = true,
            shouldSkip = false
        )
    }
}

@Preview(locale = "RU")
@Composable
private fun EmailEnterContent_PreviewRU() {
    MakeSureTheme {
        EmailEnterContent(
            onContinue = {},
            onBackClick = {},
            updateEmail = {},
            email = "12345@gmail.com",
            shouldContinue = true,
            shouldSkip = true
        )
    }
}
