package com.makesure.presentation.components.appbars

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import com.makesure.R
import com.makesure.app.theme.MakeSureTheme
import com.makesure.app.theme.PrimaryGradient
import com.makesure.app.theme.mulishFonts
import com.makesure.presentation.components.views.buttons.CloseButton
import com.makesure.presentation.navigation.flows.TopBarUiFlow
import com.makesure.presentation.navigation.graphs.AuthGraph
import com.makesure.presentation.navigation.graphs.MainBottomBar
import com.makesure.presentation.navigation.graphs.MainRoutes
import com.makesure.presentation.navigation.graphs.SettingsGraph

@Composable
fun HomeTopBar(
    navController: NavHostController,
    uiFlow: TopBarUiFlow,
) {
    val routesToExclude = listOf(
        MainBottomBar.Scan.route,
        AuthGraph.REGISTER,
        AuthGraph.SIGN_IN,
        AuthGraph.WELCOME,
        "${MainRoutes.SHARE}/{userId}",
        "${SettingsGraph.CHANGE_EMAIL}/{userId}",
        "${SettingsGraph.CHANGE_PHONE}/{userId}",
        "${SettingsGraph.BLACKLIST}/{userId}",
        "${MainRoutes.TEST_RESULTS}/{userId}/{result}",
        "${MainRoutes.CONTACT}/{contactId}",
    )
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentRoute = navBackStackEntry?.destination?.route

    if (routesToExclude.contains(currentRoute)) { return }

    when (currentRoute) {
        "${SettingsGraph.SETTINGS_LIST}/{userId}" -> {
            SettingsTopBar(
                onBackClick = navController::popBackStack,
            )
        }

        MainBottomBar.Tests.route -> {
            TestsTopBar(
                onSettingsClick = uiFlow.onSettingsClick,
                onBellClick = uiFlow.onBellClick
            )
        }

        "${MainRoutes.SHARED_PROFILE}/{contactId}",
        "${MainRoutes.CONTACT}/{contactId}",
        MainBottomBar.Contacts.route -> {
            ProfileTopBar(
                onSettingsClick = uiFlow.onSettingsClick,
                onBellClick = uiFlow.onBellClick,
                onCalendarClick = uiFlow.onCalendarClick
            )
        }

        else -> {
            DefaultTopBar(
                onSettingsClick = uiFlow.onSettingsClick,
                onQrClick = uiFlow.onQrClick,
                onBellClick = uiFlow.onBellClick
            )
        }
    }
}

@Composable
private fun ProfileTopBar(
    onSettingsClick: () -> Unit,
    onBellClick: () -> Unit,
    onCalendarClick: () -> Unit
) {
    TopBarContainer {
        Box(modifier = Modifier.height(32.dp)) {
            UserSettingsButton(onSettingsClick = onSettingsClick)

            MakeSureTitleBlurred()

            Row(
                modifier = Modifier.fillMaxSize(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.End
            ) {
                CalendarButton(onClick = onCalendarClick)
                Spacer(modifier = Modifier.width(12.dp))
                BellButton(onClick = onBellClick)
            }
        }
    }
}

@Composable
private fun TestsTopBar(
    onSettingsClick: () -> Unit,
    onBellClick: () -> Unit
) {
    TopBarContainer(
        modifier = Modifier.background(Brush.linearGradient(PrimaryGradient))
    ) {
        Box(modifier = Modifier.height(32.dp)) {
            UserSettingsButton(
                onSettingsClick = onSettingsClick,
                useDarkIcon = false
            )
            MakeSureTitleBlurred()
            Row(
                modifier = Modifier.fillMaxSize(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.End
            ) {
                BellButton(onClick = onBellClick)
            }
        }
    }
}

@Composable
private fun DefaultTopBar(
    onSettingsClick: () -> Unit,
    onQrClick: () -> Unit,
    onBellClick: () -> Unit
) {
    TopBarContainer {
        Box(modifier = Modifier.height(32.dp)) {
            UserSettingsButton(onSettingsClick = onSettingsClick)
            MakeSureTitleBlurred()
            QrCodeAndBellButtons(
                onQrClick = onQrClick,
                onBellClick = onBellClick
            )
        }
    }
}

@Composable
private fun SettingsTopBar(
    onBackClick: () -> Unit,
) {
    TopBarContainer {
        Box(modifier = Modifier.height(32.dp)) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Start,
                modifier = Modifier.fillMaxSize()
            ) {
                CloseButton(
                    onClick = onBackClick,
                    sizeDp = 32.dp,
                    color = MaterialTheme.colors.primary
                )
                Spacer(Modifier.width(10.dp))
                Text(
                    text = stringResource(id = R.string.settings_title),
                    fontFamily = mulishFonts,
                    fontWeight = FontWeight(600),
                    fontSize = 22.sp,
                    color = MaterialTheme.colors.primary
                )
            }
        }
    }
}

@Composable
private fun TopBarContainer(
    modifier: Modifier = Modifier,
    paddingValues: PaddingValues = PaddingValues(horizontal = 16.dp),
    backgroundColor: Color = MaterialTheme.colors.background,
    content: @Composable () -> Unit
) {
    TopAppBar(
        backgroundColor = backgroundColor,
        contentPadding = paddingValues,
        elevation = 0.dp,
        modifier = modifier.fillMaxWidth()
    ) {
        content()
    }
}

@Composable
private fun MakeSureTitleBlurred() {
    Row(
        modifier = Modifier.fillMaxSize(),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center
    ) {
        Icon(
            painter = painterResource(id = R.drawable.logo_blurred),
            contentDescription = null,
            tint = MaterialTheme.colors.primary,
            modifier = Modifier
                .fillMaxHeight()
        )
    }
}


@Preview(showSystemUi = true)
@Composable
private fun TestsTopBar_Preview() {
    MakeSureTheme {
        TestsTopBar(onSettingsClick = {}, onBellClick = {})
    }
}

@Preview(showSystemUi = true)
@Composable
private fun SettingsTopBar_Preview() {
    MakeSureTheme {
        SettingsTopBar(onBackClick = {})
    }
}

@Preview(showSystemUi = true)
@Composable
private fun DefaultTopBar_Preview() {
    MakeSureTheme {
        DefaultTopBar(onSettingsClick = {}, onBellClick = {}, onQrClick = {})
    }
}

@Preview(showSystemUi = true)
@Composable
private fun ProfileTopBar_Preview() {
    MakeSureTheme {
        ProfileTopBar(onSettingsClick = {}, onBellClick = {}, onCalendarClick = {})
    }
}
