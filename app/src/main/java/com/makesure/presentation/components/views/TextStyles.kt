package com.makesure.presentation.components.views

import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.sp
import com.makesure.app.theme.MakeSureTheme
import com.makesure.app.theme.interFonts
import com.makesure.app.theme.mulishFonts


@Composable
fun Title(
    text: String,
    fontSize: TextUnit = 32.sp,
    color: Color = Color.Black,
) {
    Text(
        text = text,
        fontSize = fontSize,
        fontWeight = FontWeight(800),
        color = color
    )
}

@Composable
fun SmallText(
    text: String,
    color: Color = Color(0xA6000000),
    textAlign: TextAlign = TextAlign.Start
) {
    Text(
        text = text,
        textAlign = textAlign,
        fontSize = 14.sp,
        fontFamily = interFonts,
        color = color
    )
}

@Preview
@Composable
private fun Title_Preview() {
    MakeSureTheme {
        Title(text = "Test title")
    }
}

@Preview
@Composable
private fun SmallText_Preview() {
    MakeSureTheme {
        SmallText(text = "Test title")
    }
}
