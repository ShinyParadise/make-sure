package com.makesure.presentation.components.views.userInput

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.selection.selectable
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.makesure.R
import com.makesure.data.dto.user.Sex
import com.makesure.presentation.components.views.buttons.BackButton
import com.makesure.presentation.components.views.buttons.MainButton
import com.makesure.presentation.components.views.Title
import com.makesure.app.theme.MakeSureTheme


@Composable
fun SexChooseContent(
    modifier: Modifier = Modifier,
    onBackClick: () -> Unit,
    onSexChosen: (Sex) -> Unit,
) {
    val radioOptions = listOf(Sex.FEMALE, Sex.MALE)
    var selectedOption by remember { mutableStateOf(radioOptions[0]) }

    BackHandler(onBack = onBackClick)

    Surface {
        Box(
            modifier = modifier
                .fillMaxSize()
                .padding(28.dp)
        ) {
            Column {
                BackButton(onClick = onBackClick)
                Spacer(modifier = Modifier.height(45.dp))
                Title(text = stringResource(id = R.string.sex_choose_title))
                Spacer(modifier = Modifier.height(35.dp))

                Column {
                    radioOptions.forEach { sex ->
                        Row(
                            Modifier
                                .fillMaxWidth()
                                .selectable(
                                    selected = (sex == selectedOption),
                                    onClick = {
                                        selectedOption = sex
                                    }
                                )
                                .padding(vertical = 15.dp)
                        ) {
                            SexButton(
                                text = sex.name,
                                selected = (sex == selectedOption),
                                onClick = {
                                    selectedOption = sex
                                },
                                modifier = Modifier.fillMaxWidth()
                            )
                        }
                    }
                }
            }

            MainButton(
                onClick = { onSexChosen(selectedOption) },
                text = stringResource(id = R.string.btn_continue),
                modifier = Modifier
                    .align(Alignment.BottomCenter)
                    .padding(bottom = 28.dp)
            )
        }
    }
}

@Composable
private fun SexButton(
    modifier: Modifier = Modifier,
    text: String,
    selected: Boolean,
    onClick: () -> Unit,
) {
    val buttonColor = if (selected) Color(0xFF996DF0)
                      else Color(0xFF969696)

    OutlinedButton(
        onClick = onClick,
        shape = MaterialTheme.shapes.medium,
        border = BorderStroke(2.dp, buttonColor),
        colors = ButtonDefaults.outlinedButtonColors(
            contentColor = buttonColor
        ),
        modifier = modifier
            .height(62.dp)
    ) {
        Text(
            text = text,
            fontSize = 21.sp,
            color = buttonColor,
            fontWeight = FontWeight.Bold
        )
    }
}

@Preview
@Composable
private fun SexChooseContent_Preview() {
    MakeSureTheme {
        SexChooseContent(
            onBackClick = {},
            onSexChosen = {}
        )
    }
}
