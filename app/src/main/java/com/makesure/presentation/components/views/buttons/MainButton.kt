package com.makesure.presentation.components.views.buttons

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.makesure.app.theme.MakeSureTheme
import com.makesure.app.theme.PrimaryGradient
import com.makesure.app.theme.mulishFonts


@Composable
fun MainButton(
    modifier: Modifier = Modifier,
    gradient: Brush = Brush.linearGradient(PrimaryGradient),
    isEnabled: Boolean = true,
    text: String,
    fontFamily: FontFamily = mulishFonts,
    fontWeight: FontWeight = FontWeight.Bold,
    onClick: () -> Unit,
) {
    GradientButton(
        onClick = onClick,
        modifier = modifier
            .clip(MaterialTheme.shapes.medium)
            .height(61.dp),
        text = text,
        fontFamily = fontFamily,
        fontWeight = fontWeight,
        gradient = gradient,
        isEnabled = isEnabled
    )
}

@Composable
fun GradientButton(
    text: String,
    gradient: Brush,
    isEnabled: Boolean,
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    fontFamily: FontFamily = mulishFonts,
    fontWeight: FontWeight = FontWeight(800),
) {
    val actualGradient = if (isEnabled) gradient
        else Brush.linearGradient(listOf(Color(0xFFCECECE), Color(0xFF878787)))

    Button(
        colors = ButtonDefaults.buttonColors(
            backgroundColor = Color.Transparent,
            contentColor = Color.White,
            disabledContentColor = Color.White
        ),
        enabled = isEnabled,
        contentPadding = PaddingValues(),
        onClick = { onClick() },
        modifier = modifier
    ) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .background(actualGradient),
            contentAlignment = Alignment.Center
        ) {
            Text(
                text = text,
                fontFamily = fontFamily,
                fontSize = 21.sp,
                fontWeight = fontWeight
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun MainButton_Preview() {
    MakeSureTheme {
        MainButton(
            onClick = {},
            text = "Text",
            modifier = Modifier.fillMaxWidth(),
            isEnabled = false
        )
    }
}
