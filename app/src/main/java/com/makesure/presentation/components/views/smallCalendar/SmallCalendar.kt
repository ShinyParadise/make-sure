package com.makesure.presentation.components.views.smallCalendar

import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.size
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.makesure.domain.entities.CalendarDayContent
import com.makesure.presentation.components.views.CircleUserPhoto
import org.koin.androidx.compose.koinViewModel
import java.time.format.TextStyle

@Composable
fun SmallCalendar(
    modifier: Modifier = Modifier,
    viewModel: SmallCalendarViewModel = koinViewModel(),
    onClick: () -> Unit,
) {
    val uiState = viewModel.uiState

    SmallCalendarContent(modifier, uiState) { onClick() }
}

@Composable
private fun SmallCalendarContent(
    modifier: Modifier = Modifier,
    uiState: SmallCalendarViewModel.SmallCalendarUiState,
    onClick: () -> Unit
) {
    val interactionSource = remember { MutableInteractionSource() }

    Row(
        modifier = modifier.clickable(interactionSource, indication = null) { onClick() },
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        for (day in uiState.days) {
            DayItem(day)
        }
    }
}

@Composable
private fun DayItem(
    day: CalendarDayContent,
) {
    val context = LocalContext.current

    Column(horizontalAlignment = Alignment.CenterHorizontally) {
        Text(
            text = day.date
                .dayOfWeek.getDisplayName(
                    TextStyle.SHORT,
                    context.resources.configuration.locales[0]
                ),
            fontSize = 14.sp,
            color = MaterialTheme.colors.primary
        )

        Box(
            modifier = Modifier
                .size(22.dp),
            contentAlignment = Alignment.Center
        ) {
            if (day.meetings.isNotEmpty()) {
                CircleUserPhoto(
                    photoURL = day.meetings.last().photoURL,
                    sizeDp = 22.dp,
                )
            }

            Text(
                text = day.date.dayOfMonth.toString(),
                fontSize = 14.sp,
                color = if (day.meetings.isNotEmpty()) Color.White
                        else MaterialTheme.colors.primary
            )
        }
    }
}
