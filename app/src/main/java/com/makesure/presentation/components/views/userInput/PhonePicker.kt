package com.makesure.presentation.components.views.userInput

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import com.makesure.presentation.components.countryCodes
import com.makesure.domain.entities.CountryCode
import com.makesure.app.theme.MakeSureTheme
import com.makesure.app.theme.mulishFonts

@Composable
fun PhonePicker(
    countryCodes: List<CountryCode>,
    onCountryChoose: (CountryCode) -> Unit,
    onTextChange: (String) -> Unit,
    modifier: Modifier = Modifier
) {
    var phoneWithoutCode by remember { mutableStateOf("") }

    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
    ) {
        CountryCodePicker(
            countryCodes = countryCodes,
            onChoose = onCountryChoose,
        )

        PhoneInputField(
            text = phoneWithoutCode,
            onTextChange = {
                phoneWithoutCode = it
                onTextChange(it)
            }
        )
    }
}

@Composable
private fun CountryCodePicker(
    countryCodes: List<CountryCode>,
    onChoose: (CountryCode) -> Unit,
){
    var expanded by remember { mutableStateOf(false) }
    var selectedCode by remember { mutableStateOf(countryCodes[0]) }

    Box {
        TextButton(
            onClick = { expanded = true },
        ) {
            Text(
                text = "${selectedCode.country} ${selectedCode.code}",
                fontSize = 24.sp,
                color = MaterialTheme.colors.primary,
            )
            Icon(
                imageVector = Icons.Default.ArrowDropDown,
                tint = MaterialTheme.colors.primary,
                contentDescription = null
            )
        }
// // Now this functionality is disabled because it isn't needed
//        DropdownMenu(
//            expanded = expanded,
//            onDismissRequest = { expanded = false },
//        ) {
//            for (item in countryCodes) {
//                DropdownMenuItem(
//                    onClick = {
//                        selectedCode = item
//                        expanded = false
//                        onChoose(selectedCode)
//                    }
//                ) {
//                    Text(text = "${item.country} ${item.code}")
//                }
//            }
//        }
    }
}

@Composable
private fun PhoneInputField(
    text: String,
    onTextChange: (String) -> Unit
) {
    TextField(
        value = text,
        textStyle = TextStyle(
            fontSize = 24.sp,
            fontFamily = mulishFonts,
            fontWeight = FontWeight(600)
        ),
        onValueChange = {
            onTextChange(it)
        },
        colors = TextFieldDefaults.textFieldColors(
            backgroundColor = Color.Transparent,
            focusedIndicatorColor = Color(0x61000000),
            unfocusedIndicatorColor = Color(0x61000000)
        ),
        singleLine = true,
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Phone)
    )
}

@Preview
@Composable
private fun PhonePicker_Preview() {
    MakeSureTheme {
        PhonePicker(countryCodes, {}, {})
    }
}
