package com.makesure.presentation.components.views

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.makesure.R
import com.makesure.app.theme.mulishFonts
import com.makesure.data.dto.test.Test
import com.makesure.domain.entities.TestSet
import com.makesure.utils.getDayMonthAndYearLocal

@Composable
fun TestSetDetails(
    modifier: Modifier = Modifier,
    testSet: TestSet
) {
    Column(
        modifier = modifier
            .fillMaxWidth(),
        horizontalAlignment = Alignment.End
    ) {
        Text(
            text = testSet.date.getDayMonthAndYearLocal(),
            fontFamily = mulishFonts,
            fontWeight = FontWeight(600),
            color = MaterialTheme.colors.onPrimary,
            fontSize = 20.sp,
            modifier = Modifier
                .align(Alignment.Start)
                .padding(top = 12.dp)
        )

        for (item in testSet.tests) {
            SingleTestItem(
                test = item,
                modifier = Modifier.padding(vertical = 10.dp)
            )
        }
        Spacer(Modifier.height(10.dp))

        Text(
            text = stringResource(R.string.title_learn_more),
            textDecoration = TextDecoration.Underline,
            fontFamily = mulishFonts,
            fontWeight = FontWeight.Light,
            fontSize = 14.sp,
            color = Color(0xFFB3AEFF)
        )
    }
}

@Composable
private fun SingleTestItem(
    modifier: Modifier = Modifier,
    test: Test
) {
    Row(
        modifier = modifier.fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically
    ) {
        if (test.result?.lowercase() == "positive") {
            RedDot(size = 18.dp)
        } else {
            GreenDot(size = 18.dp)
        }
        Spacer(modifier = Modifier.width(12.dp))

        TestItemText(test.infectionType)
        Spacer(Modifier.weight(1f))

        test.result?.let { TestItemText(it, color = Color.White.copy(alpha = 0.52f)) }
    }
}

@Composable
private fun TestItemText(
    text: String,
    color: Color = Color.White
) {
    Text(
        text = text,
        fontFamily = mulishFonts,
        fontWeight = FontWeight.Medium,
        fontSize = 16.sp,
        color = color
    )
}
