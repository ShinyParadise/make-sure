package com.makesure.presentation.components.views

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.makesure.R
import com.makesure.app.theme.mulishFonts
import com.makesure.data.dto.user.User
import com.makesure.presentation.screens.main.MainViewModel

@Composable
fun ContactTile(
    contact: User,
    interactionSource: MutableInteractionSource,
    modifier: Modifier = Modifier,
    uiState: MainViewModel.MainUiState? = null,
    onClick: (User) -> Unit = {},
    onDetailsClick: () -> Unit = {},
) {
    Row(
        modifier = modifier
            .clickable(
                interactionSource = interactionSource,
                indication = null
            ) { onClick(contact) },
        verticalAlignment = Alignment.CenterVertically
    ) {
        CircleClickableUserPhoto(photoURL = contact.photoURL, sizeDp = 56.dp)
        Spacer(Modifier.width(15.dp))

        ContactInfo(
            contact = contact,
            uiState = uiState
        )
        Spacer(Modifier.weight(1f))

        IconButton(onClick = onDetailsClick) {
            Icon(
                painter = painterResource(id = R.drawable.icon_dots),
                contentDescription = null,
                modifier = Modifier.size(48.dp)
            )
        }
    }
}

@Composable
private fun ContactInfo(
    uiState: MainViewModel.MainUiState? = null,
    contact: User
) {
    val color = uiState?.contactsLabelColors?.get(contact)
    val text = uiState?.contactsLabels?.get(contact)

    Column {
        Text(
            text = contact.name,
            fontFamily = mulishFonts,
            fontWeight = FontWeight.Bold,
            fontSize = 16.sp,
            color = MaterialTheme.colors.primary
        )
        Spacer(Modifier.height(5.dp))

        if (color != null && text != null) {
            Box(
                modifier = Modifier
                    .background(
                        shape = MaterialTheme.shapes.small,
                        color = color
                    )
                    .padding(horizontal = 15.dp, vertical = 4.dp),
                contentAlignment = Alignment.CenterStart
            ) {
                Text(
                    text = text,
                    fontFamily = mulishFonts,
                    fontSize = 14.sp,
                )
            }
        }
    }
}
