package com.makesure.presentation.components.views.userInput

import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import com.makesure.app.theme.MakeSureTheme
import com.makesure.app.theme.Primary
import com.makesure.app.theme.mulishFonts

@Composable
fun InputField(
    modifier: Modifier = Modifier,
    value: String,
    onValueChange: (String) -> Unit,
    placeholder: String,
    keyboardOptions: KeyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Text),
    textStyle: TextStyle = defaultStyle,
) {
    TextField(
        value = value,
        onValueChange = onValueChange,
        modifier = modifier,
        textStyle = textStyle,
        placeholder = {
            Text(
                text = placeholder,
                fontFamily = textStyle.fontFamily,
                fontWeight = textStyle.fontWeight,
                fontSize = textStyle.fontSize,
                color = Color(0xFF343434).copy(alpha = 0.26f)
            )
        },
        singleLine = true,
        keyboardOptions = keyboardOptions,
        colors = TextFieldDefaults.textFieldColors(
            backgroundColor = Color.Transparent,
            focusedIndicatorColor = MaterialTheme.colors.primary,
            unfocusedIndicatorColor = MaterialTheme.colors.primary
        )
    )
}

private val defaultStyle = TextStyle(
    fontFamily = mulishFonts,
    fontSize = 23.sp,
    color = Primary
)

@Preview(showBackground = true)
@Composable
private fun InputField_Entered_Preview() {
    MakeSureTheme {
        InputField(value = "123456789", onValueChange = {}, placeholder = "")
    }
}

@Preview(showBackground = true)
@Composable
private fun InputField_Placeholder_Preview() {
    MakeSureTheme {
        InputField(value = "", onValueChange = {}, placeholder = "1234567890")
    }
}
