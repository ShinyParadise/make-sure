package com.makesure.presentation.components.views

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp
import com.makesure.app.theme.greenDotColor

@Composable
fun GreenDot(
    modifier: Modifier = Modifier,
    size: Dp,
) {
    Box(modifier = modifier
        .size(size)
        .background(shape = CircleShape, color = greenDotColor)
    )
}

@Composable
fun RedDot(
    modifier: Modifier = Modifier,
    size: Dp,
) {
    Box(modifier = modifier
        .size(size)
        .background(shape = CircleShape, color = Color.Red)
    )
}

