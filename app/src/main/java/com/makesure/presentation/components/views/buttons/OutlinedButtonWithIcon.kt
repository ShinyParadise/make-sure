package com.makesure.presentation.components.views.buttons

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.makesure.app.theme.MakeSureTheme
import com.makesure.R

@Composable
fun OutlinedButtonWithIcon(
    icon: Painter,
    text: String,
    modifier: Modifier = Modifier,
    onClick: () -> Unit
) {
    OutlinedButton(
        onClick = onClick,
        shape = MaterialTheme.shapes.medium,
        colors = ButtonDefaults.buttonColors(
            backgroundColor = Color.Transparent
        ),
        border = BorderStroke(
            width = 1.dp,
            color = MaterialTheme.colors.onPrimary
        ),
        modifier = modifier
            .fillMaxWidth()
            .size(60.dp)
    ) {
        Icon(
            painter = icon,
            tint = MaterialTheme.colors.onPrimary,
            contentDescription = null,
        )
        Text(
            text = text,
            fontSize = 16.sp,
            fontWeight = FontWeight.SemiBold,
            color = MaterialTheme.colors.onPrimary,
            modifier = Modifier
                .padding(vertical = 10.dp, horizontal = 20.dp)
        )
    }
}

@Preview
@Composable
fun OutlinedButtonWithIcon_Preview() {
    MakeSureTheme {
        OutlinedButtonWithIcon(
            icon = painterResource(id = R.drawable.google_icon),
            text = "test",
            onClick = {}
        )
    }
}
