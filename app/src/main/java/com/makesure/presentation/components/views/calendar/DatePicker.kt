package com.makesure.presentation.components.views.calendar

import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import com.vanpra.composematerialdialogs.MaterialDialog
import com.vanpra.composematerialdialogs.MaterialDialogState
import com.vanpra.composematerialdialogs.datetime.date.DatePickerDefaults
import com.vanpra.composematerialdialogs.datetime.date.datepicker
import java.time.LocalDate

@Composable
fun DatePicker(
    dateDialogState: MaterialDialogState,
    positiveBtnText: String = "Save",
    negativeBtnText: String = "Cancel",
    title: String = "Pick a date",
    onPositiveClick: () -> Unit = {},
    onDateChange: (LocalDate) -> Unit,
) {
    MaterialDialog(
        dialogState = dateDialogState,
        buttons = {
            positiveButton(positiveBtnText) {
                onPositiveClick()
            }
            negativeButton(negativeBtnText)
        }
    ) {
        datepicker(
            initialDate = LocalDate.now(),
            title = title,
            onDateChange = onDateChange,
            colors = DatePickerDefaults.colors(
                headerBackgroundColor = MaterialTheme.colors.secondary,
                headerTextColor = MaterialTheme.colors.onSecondary,
                dateActiveBackgroundColor = MaterialTheme.colors.secondary,
                dateActiveTextColor = MaterialTheme.colors.onSecondary
            )
        )
    }
}
