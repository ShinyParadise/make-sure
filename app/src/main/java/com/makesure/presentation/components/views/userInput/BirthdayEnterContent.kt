package com.makesure.presentation.components.views.userInput

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.makesure.R
import com.makesure.presentation.components.views.SmallText
import com.makesure.presentation.components.views.buttons.BackButton
import com.makesure.presentation.components.views.buttons.MainButton
import com.makesure.presentation.components.views.Title
import com.makesure.app.theme.MakeSureTheme
import com.makesure.app.theme.mulishFonts


@Composable
fun BirthdayEnterContent(
    birthday: String,
    shouldContinue: Boolean,
    onBirthdayChange: (String) -> Unit,
    onBackClick: () -> Unit,
    onContinue: () -> Unit,
    modifier: Modifier = Modifier
) {
    BackHandler(onBack = onBackClick)
    Surface {
        Box(
            modifier = modifier
                .fillMaxSize()
                .padding(horizontal = 25.dp)
        ) {
            Column {
                BackButton(onClick = onBackClick)
                Spacer(modifier = Modifier.height(45.dp))

                Title(
                    text = stringResource(id = R.string.birthday_title),
                    color = MaterialTheme.colors.primary,
                )
                SmallText(text = stringResource(id = R.string.birthday_description))
                Spacer(modifier = Modifier.height(60.dp))

                BasicTextField(
                    value = birthday,
                    onValueChange = onBirthdayChange,
                    singleLine = true,
                    enabled = true,
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                    decorationBox = {
                        Row(
                            modifier = Modifier.fillMaxWidth(),
                            horizontalArrangement = Arrangement.SpaceAround
                        ) {
                            repeat(10) {
                                if (it == 2 || it == 5) {
                                    SlashSymbol()
                                } else {
                                    var offset = if (it >= 5) 1 else 0
                                    offset += if (it >= 2) 1 else 0

                                    val text: Char =
                                        if (birthday.length - 1 >= it - offset) birthday[it - offset]
                                        else ' '
                                    CharView(text = text, index = it)
                                }
                            }
                        }
                    }
                )
            }

            MainButton(
                onClick = onContinue,
                text = stringResource(id = R.string.btn_continue),
                isEnabled = shouldContinue,
                modifier = Modifier
                    .align(Alignment.BottomCenter)
                    .padding(bottom = 28.dp)
            )
        }
    }
}

@Composable
private fun CharView(text: Char, index: Int) {
    var char = text
    var color = MaterialTheme.colors.primary

    if (char == ' ' && index < 2) {
        char = 'D'
        color = Color(0x61000000)
    } else if (char == ' ' && index < 5) {
        char = 'M'
        color = Color(0x61000000)
    } else if (char == ' ' && index < 10) {
        char = 'Y'
        color = Color(0x61000000)
    }

    Column {
        Text(
            text = char.toString(),
            style = TextStyle(
                fontSize = 24.sp,
                fontWeight = FontWeight.Light,
                fontFamily = mulishFonts,
                color = color,
                letterSpacing = 10.sp
            ),
        )
        Box(
            modifier = Modifier
                .size(width = 24.dp, height = 2.dp)
                .background(Color(0x61000000))
        )
    }
}

@Composable
private fun SlashSymbol() {
    Text(
        text = "/",
        style = TextStyle(
            fontSize = 24.sp,
            fontWeight = FontWeight.Light,
            fontFamily = mulishFonts,
            color = MaterialTheme.colors.primary,
            letterSpacing = 10.sp
        ),
    )
}

@Preview
@Composable
fun BirthdayEnterContent_Preview() {
    MakeSureTheme {
        BirthdayEnterContent(
            birthday = "",
            onContinue = {},
            onBackClick = {},
            onBirthdayChange = {},
            shouldContinue = true
        )
    }
}

@Preview(locale = "RU")
@Composable
fun BirthdayEnterContent_PreviewRU() {
    MakeSureTheme {
        BirthdayEnterContent(
            birthday = "12122024",
            onContinue = {},
            onBackClick = {},
            onBirthdayChange = {},
            shouldContinue = true
        )
    }
}
