package com.makesure.presentation.components

import com.makesure.data.dto.notification.Notification
import com.makesure.data.dto.post.Post
import com.makesure.data.dto.test.Test
import com.makesure.data.dto.user.User
import com.makesure.domain.entities.CountryCode
import com.makesure.domain.entities.TestSet
import java.time.ZoneId
import java.time.ZonedDateTime

val countryCodes = listOf(
    CountryCode(country = "RU", code = "+7"),
    CountryCode(country = "UK", code = "+44"),
    CountryCode(country = "KZ", code = "+7"),
    CountryCode(country = "US", code = "+1"),
    CountryCode(country = "BY", code = "+375"),
    CountryCode(country = "CZ", code = "+420"),
    CountryCode(country = "FI", code = "+358"),
    CountryCode(country = "DE", code = "+49"),
    CountryCode(country = "TM", code = "+993"),
)

val previewPostCard = Post(
    id = "123",
    createdAt = "123213",
    title = "Test Tip",
    description = "Test description",
    link = "youtube.com",
    imageURL = "https://yxhzpmbfylvoizdcwkpa.supabase.co/storage/v1/object/public/tips_photos/image%2016.png",
    type = "dates"
)

val previewTest = Test(
    id = "",
    packageId = "",
    userId = "",
    dateTime = ZonedDateTime.parse("2023-05-02T15:06:06+00:00"),
    infectionType = "syphilis",
    result = "negative",
    isActivated = true,
    photoURL = null
)

val previewTestSet = TestSet(
    tests = listOf(previewTest),
    date = ZonedDateTime.of(2023, 12, 12, 12, 12, 12, 12, ZoneId.systemDefault())
)

val previewTips = listOf(previewPostCard)

val previewUser = User(
    id = "19c479a8-e8f0-11ed-a05b-0242ac120003",
    name = "Ivan",
    phone = "+77777777777",
    sex = "male",
    birthday = ZonedDateTime.now(),
    blockedUsers = null,
    photoURL = null,
    email = "ivan@mail.ru",
    contacts = emptyList()
)

val previewNotifications = listOf(
    Notification(
        "1",
        "123",
        "123",
        "descriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescription",
        "description",
        "1",
        ZonedDateTime.now(),
        "",
        false
    ),
    Notification("2", "234", "234","description", "description", "1", ZonedDateTime.now(), "", false),
    Notification("3", "234", "234","description", "description","1", ZonedDateTime.now(), "", true),
)
