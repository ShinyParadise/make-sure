package com.makesure.presentation.components.appbars

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.icons.filled.Notifications
import androidx.compose.material.icons.filled.QrCodeScanner
import androidx.compose.material.icons.outlined.CalendarToday
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.makesure.R
import com.makesure.app.theme.topAppBarButtonsColor


@Composable
fun UserSettingsButton(
    modifier: Modifier = Modifier,
    useDarkIcon: Boolean = true,
    onSettingsClick: () -> Unit,
) {
    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Start
    ) {
        IconButton(
            onClick = onSettingsClick,
            modifier = Modifier.size(32.dp).padding(4.dp)
        ) {
            Icon(
                painter = if (useDarkIcon) { painterResource(id = R.drawable.drawer_icon) }
                          else { painterResource(id = R.drawable.drawer_icon_light) },
                contentDescription = null,
                tint = topAppBarButtonsColor,
            )
        }
    }
}

@Composable
fun QrCodeAndBellButtons(
    onQrClick: () -> Unit,
    onBellClick: () -> Unit,
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.End,
        modifier = Modifier.fillMaxSize()
    ) {
        QrCodeButton(onClick = onQrClick)
        Spacer(modifier = Modifier.width(12.dp))
        BellButton(onClick =  onBellClick)
    }
}

@Composable
fun QrCodeButton(
    color: Color = topAppBarButtonsColor,
    onClick: () -> Unit
) {
    IconButton(
        onClick = onClick,
        modifier = Modifier.size(32.dp)
    ) {
        Icon(
            imageVector = Icons.Default.QrCodeScanner,
            contentDescription = null,
            tint = color
        )
    }
}

@Composable
fun BellButton(
    modifier: Modifier = Modifier,
    color: Color = topAppBarButtonsColor,
    onClick: () -> Unit
) {
    IconButton(
        onClick = onClick,
        modifier = modifier.size(32.dp)
    ) {
        Icon(
            imageVector = Icons.Default.Notifications,
            contentDescription = null,
            tint = color
        )
    }
}

@Composable
fun CalendarButton(
    color: Color = topAppBarButtonsColor,
    onClick: () -> Unit,
) {
    IconButton(onClick = onClick,
        modifier = Modifier.size(32.dp)) {
        Icon(
            imageVector = Icons.Outlined.CalendarToday,
            tint = color,
            contentDescription = null
        )
    }
}

@Composable
fun EditButton(onEditClick: () -> Unit) {
    IconButton(
        onClick = onEditClick,
        modifier = Modifier.size(32.dp)
    ) {
        Icon(
            imageVector = Icons.Default.Edit,
            contentDescription = null,
            tint = topAppBarButtonsColor
        )
    }
}
