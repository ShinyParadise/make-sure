package com.makesure.presentation.components.views.smallCalendar

import android.os.Parcelable
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.SavedStateHandleSaveableApi
import androidx.lifecycle.viewmodel.compose.saveable
import com.makesure.domain.entities.CalendarDayContent
import com.makesure.domain.interactors.MeetingsInteractor
import com.makesure.domain.interactors.TestsInteractor
import com.makesure.domain.interactors.UserInteractor
import com.makesure.utils.DatesProvider
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.parcelize.Parcelize
import java.time.LocalDate

@OptIn(SavedStateHandleSaveableApi::class)
class SmallCalendarViewModel(
    private val userInteractor: UserInteractor,
    private val testsInteractor: TestsInteractor,
    private val meetingsInteractor: MeetingsInteractor,
    private val mainDispatcher: CoroutineDispatcher,
    private val bgDispatcher: CoroutineDispatcher,
    datesProvider: DatesProvider,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val currentWeek = datesProvider.getCurrentWeek()

    var uiState by savedStateHandle.saveable { mutableStateOf(SmallCalendarUiState()) }
        private set

    @Parcelize
    data class SmallCalendarUiState(
        val days: List<CalendarDayContent> = emptyList(),
        val currentDate: LocalDate = LocalDate.now(),
    ): Parcelable {
        val currentDayContent: CalendarDayContent?
            get() = days.find { it.date == currentDate }
    }

    init {
        uiState = uiState.copy(days = currentWeek.map { CalendarDayContent(date = it) })

        viewModelScope.launch(bgDispatcher) {
            val tests = testsInteractor.getUserTestSets()
            val meetingsByDate = meetingsInteractor.getUserMeetingsByDate()

            val content = currentWeek.map { date ->
                val testSet = tests.firstOrNull { date == it.date.toLocalDate() }
                val meetings = meetingsByDate.getOrDefault(date, emptyList())

                CalendarDayContent(
                    testSet = testSet,
                    testResult = testSet?.isPositive(),
                    meetings = meetings,
                    date = date
                )
            }

            withContext(mainDispatcher) {
                uiState = uiState.copy(days = content)
            }
        }
    }
}
