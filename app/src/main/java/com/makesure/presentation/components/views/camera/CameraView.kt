package com.makesure.presentation.components.views.camera

import android.graphics.Color
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.activity.ComponentActivity
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST
import androidx.camera.core.ImageCapture
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.LifecycleCameraController
import androidx.camera.view.PreviewView
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import com.makesure.domain.entities.BarcodeAnalyzer
import java.util.concurrent.Executors


@Composable
fun CameraView(
    modifier: Modifier = Modifier,
    bgColor: Int = Color.WHITE,
    controller: LifecycleCameraController,
    lifecycleOwner: LifecycleOwner
) {
    AndroidView(
        modifier = modifier.fillMaxSize(),
        factory = { context ->
            PreviewView(context).apply {
                layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                setBackgroundColor(bgColor)
                scaleType = PreviewView.ScaleType.FILL_START
            }.also { previewView ->
                previewView.controller = controller
                controller.bindToLifecycle(lifecycleOwner)
            }
        }
    )
}

@Composable
@androidx.annotation.OptIn(androidx.camera.core.ExperimentalGetImage::class)
fun QrScannerView(
    modifier: Modifier = Modifier,
    onScanResult: (String) -> Unit
) {
    AndroidView({ context ->
        val cameraExecutor = Executors.newSingleThreadExecutor()

        val previewView = PreviewView(context).also {
            it.scaleType = PreviewView.ScaleType.FILL_CENTER
        }
        val cameraProviderFuture = ProcessCameraProvider.getInstance(context)

        cameraProviderFuture.addListener({
            val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()

            val preview = Preview.Builder()
                .build()
                .also { it.setSurfaceProvider(previewView.surfaceProvider) }

            val imageCapture = ImageCapture.Builder().build()

            val imageAnalysis = ImageAnalysis.Builder()
                .setBackpressureStrategy(STRATEGY_KEEP_ONLY_LATEST)
                .build()
            imageAnalysis.setAnalyzer(cameraExecutor, BarcodeAnalyzer(onScanSuccess = onScanResult))

            val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA
            cameraProvider.unbindAll()
            cameraProvider.bindToLifecycle(
                context as ComponentActivity, cameraSelector, preview, imageCapture, imageAnalysis)

        }, ContextCompat.getMainExecutor(context))
        previewView
    }, modifier = modifier)
}
