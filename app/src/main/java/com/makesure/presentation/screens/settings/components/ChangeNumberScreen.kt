package com.makesure.presentation.screens.settings.components

import androidx.compose.foundation.layout.Box
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.makesure.R
import com.makesure.presentation.components.views.SuccessScreen
import com.makesure.presentation.components.views.userInput.CodeEnterContent
import com.makesure.presentation.components.views.userInput.PhoneEnterContent
import com.makesure.presentation.navigation.flows.SettingsUiFlow
import com.makesure.presentation.screens.settings.SettingsViewModel
import com.makesure.presentation.screens.settings.SettingsViewModel.SettingsState.*
import com.makesure.app.theme.MakeSureTheme

@Composable
fun ChangeNumberScreen(
    modifier: Modifier = Modifier,
    uiState: SettingsViewModel.SettingsUiState,
    uiFlow: SettingsUiFlow
) {
    Box(modifier = modifier) {
        when (uiState.state) {
            CHANGE_PHONE -> {
                PhoneEnterContent(
                    shouldContinue = uiState.shouldContinue,
                    onBackClick = uiFlow.onBackClick,
                    updatePhone = uiFlow.phone.updatePhone,
                    updateCountryCode = uiFlow.phone.updateCountryCode,
                    onContinue = uiFlow.phone.onPhoneEntered
                )
            }

            CODE -> {
                CodeEnterContent(
                    number = uiState.countryCode.code + uiState.enteredNumber,
                    code = uiState.code,
                    updateCode = uiFlow.code.updateCode,
                    onResend = uiFlow.code.onResendCode,
                    onContinue = uiFlow.code.onCodeEntered,
                    onBackClick = uiFlow.onBackClick,
                    shouldContinue = uiState.shouldContinue,
                    isResendEnabled = uiState.isResendEnabled
                )
            }

            NUMBER_CHANGED -> {
                SuccessScreen(
                    title = stringResource(R.string.settings_number_success),
                    subtitle = uiState.countryCode.code + uiState.enteredNumber,
                    onContinue = uiFlow.onPhoneEditingFinished,
                    shouldContinue = uiState.shouldContinue
                )
            }
            else -> {}
        }
    }
}

@Preview(showSystemUi = true)
@Composable
private fun ChangeNumberScreen_Preview() {
    MakeSureTheme {
        ChangeNumberScreen(
            uiState = SettingsViewModel.SettingsUiState(),
            uiFlow = SettingsUiFlow()
        )
    }
}
