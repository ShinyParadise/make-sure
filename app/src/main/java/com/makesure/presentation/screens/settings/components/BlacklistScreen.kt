package com.makesure.presentation.screens.settings.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.makesure.R
import com.makesure.data.dto.user.User
import com.makesure.presentation.components.previewUser
import com.makesure.presentation.components.views.CircleClickableUserPhoto
import com.makesure.presentation.components.views.GradientBox
import com.makesure.presentation.components.views.buttons.CloseButton
import com.makesure.presentation.navigation.flows.SettingsUiFlow
import com.makesure.presentation.screens.settings.SettingsViewModel
import com.makesure.app.theme.MakeSureTheme
import com.makesure.app.theme.mulishFonts

@Composable
fun BlacklistScreen(
    uiState: SettingsViewModel.SettingsUiState,
    uiFlow: SettingsUiFlow
) {
    Column {
        BlacklistHeader(uiFlow.onBackClick)
        GradientBox {
            Column {
                LazyColumn {
                    items(
                        items = uiState.blockedUsers
                    ) {blockedUser ->
                        BlockedUserItem(blockedUser) {
                            uiFlow.onUnblockUser(blockedUser)
                        }
                    }
                }
            }
        }
    }
}

@Composable
private fun BlockedUserItem(
    user: User,
    onClick: () -> Unit
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier.padding(horizontal = 16.dp, vertical = 10.dp)
    ) {
        CircleClickableUserPhoto(photoURL = user.photoURL, sizeDp = 62.dp)
        Spacer(Modifier.width(10.dp))

        Text(
            text = user.name,
            fontFamily = mulishFonts,
            fontSize = 15.sp,
            color = Color.White,
            fontWeight = FontWeight.Bold,
            modifier = Modifier.weight(1f)
        )
        Spacer(Modifier.width(10.dp))

        Button(
            onClick = onClick,
            colors = ButtonDefaults.buttonColors(
                backgroundColor = Color(0xFFF7D5FF),
                contentColor = Color.Black
            )
        ) {
            Text(
                text = stringResource(R.string.blacklist_unlock),
                fontFamily = mulishFonts,
                fontSize = 14.sp
            )
        }
    }
}

@Composable
private fun BlacklistHeader(onBackClick: () -> Unit) {
    Row(
        modifier = Modifier.padding(horizontal = 12.dp, vertical = 16.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        CloseButton(
            onClick = onBackClick,
            color = MaterialTheme.colors.primary,
            sizeDp = 24.dp
        )
        Text(
            text = stringResource(R.string.blacklist_title),
            color = MaterialTheme.colors.primary,
            fontFamily = mulishFonts,
            fontWeight = FontWeight.Bold,
            fontSize = 22.sp,
            modifier = Modifier.padding(start = 10.dp)
        )
    }
}

@Preview(showSystemUi = true)
@Composable
private fun BlacklistScreen_Preview() {
    MakeSureTheme {
        BlacklistScreen(
            uiState = SettingsViewModel.SettingsUiState(
                blockedUsers = listOf(previewUser)
            ),
            uiFlow = SettingsUiFlow()
        )
    }
}
