package com.makesure.presentation.screens.main.bottomNavScreens.home

import android.net.Uri
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.makesure.R
import com.makesure.app.theme.mulishFonts
import com.makesure.data.dto.user.User
import com.makesure.presentation.components.views.CircleClickableUserPhoto
import com.makesure.utils.getDayMonthAndYearLocal

@Composable
fun ProfileSection(
    modifier: Modifier = Modifier,
    onSelectPhoto: (Uri) -> Unit,
    user: User
) {
    val pickMedia = rememberLauncherForActivityResult(
        ActivityResultContracts.PickVisualMedia()
    ) { uri ->
        if (uri != null) onSelectPhoto(uri)
    }

    Column(
        modifier = modifier.fillMaxHeight(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        if (user.photoURL != null)
            CircleClickableUserPhoto(photoURL = user.photoURL, sizeDp = 81.dp) {
                pickMedia.launch(
                    PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly)
                )
            }
        else
            AddPhoto {
                pickMedia.launch(
                    PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly)
                )
            }
        Spacer(Modifier.height(7.dp))
        Text(
            text = user.name,
            fontFamily = mulishFonts,
            fontSize = 15.sp,
            fontWeight = FontWeight.SemiBold,
            color = Color.White,
            textAlign = TextAlign.Center
        )
    }
}

@Composable
private fun AddPhoto(
    sizeDp: Dp = 81.dp,
    onClick: () -> Unit,
) {
    Box(
        Modifier
            .size(sizeDp)
            .background(Color.Black.copy(alpha = 0.3f), shape = CircleShape)
            .clip(CircleShape)
            .clickable { onClick() }
    ) {
        Icon(
            imageVector = Icons.Default.Add,
            tint = Color.White,
            modifier = Modifier
                .size(sizeDp / 2)
                .align(Alignment.Center),
            contentDescription = null
        )
    }
}

@Composable
fun TestsDoneSection(modifier: Modifier = Modifier, testCount: Int) {
    Column(
        modifier = modifier.fillMaxHeight(),
        verticalArrangement = Arrangement.Bottom,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = if (testCount < 99)
                "$testCount " + stringResource(R.string.tests_done)
            else
                "99+ " + stringResource(R.string.tests_done),
            fontFamily = mulishFonts,
            fontSize = 12.sp,
            color = Color.White.copy(alpha = 0.8f),
            textAlign = TextAlign.Center
        )
    }
}

@Composable
fun AgeSection(modifier: Modifier = Modifier, user: User) {
    Column(
        modifier = modifier.fillMaxHeight(),
        verticalArrangement = Arrangement.Bottom,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = user.birthday.getDayMonthAndYearLocal(),
            fontFamily = mulishFonts,
            fontSize = 12.sp,
            color = Color.White.copy(alpha = 0.8f),
            textAlign = TextAlign.Center
        )
    }
}
