package com.makesure.presentation.screens.settings.components

import androidx.compose.foundation.layout.Box
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.makesure.R
import com.makesure.presentation.components.views.SuccessScreen
import com.makesure.presentation.components.views.userInput.EmailEnterContent
import com.makesure.presentation.navigation.flows.SettingsUiFlow
import com.makesure.presentation.screens.settings.SettingsViewModel
import com.makesure.presentation.screens.settings.SettingsViewModel.SettingsState.*

@Composable
fun ChangeEmailScreen(
    modifier: Modifier = Modifier,
    uiState: SettingsViewModel.SettingsUiState,
    uiFlow: SettingsUiFlow
) {
    Box(modifier = modifier) {
        when (uiState.state) {
            CHANGE_EMAIL -> {
                EmailEnterContent(
                    email = uiState.enteredEmail,
                    onContinue = uiFlow.email.onEmailEntered,
                    onBackClick = uiFlow.onBackClick,
                    updateEmail = uiFlow.email.updateEmail,
                    shouldContinue = uiState.shouldContinue,
                    shouldSkip = false,
                )
            }
            
            EMAIL_CHANGED -> {
                SuccessScreen(
                    title = stringResource(R.string.settings_success_email_change),
                    subtitle = uiState.enteredEmail,
                    shouldContinue = uiState.shouldContinue,
                    onContinue = uiFlow.onEmailEditingFinished
                )
            }

            else -> {}
        }
    }
}
