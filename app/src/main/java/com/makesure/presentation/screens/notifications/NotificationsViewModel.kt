package com.makesure.presentation.screens.notifications

import android.os.Parcelable
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.SavedStateHandleSaveableApi
import androidx.lifecycle.viewmodel.compose.saveable
import com.makesure.data.dto.notification.Notification
import com.makesure.data.dto.user.User
import com.makesure.domain.interactors.NotificationsInteractor
import com.makesure.domain.interactors.UserInteractor
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.parcelize.Parcelize

@OptIn(SavedStateHandleSaveableApi::class)
class NotificationsViewModel(
    private val userInteractor: UserInteractor,
    private val notificationsInteractor: NotificationsInteractor,
    private val mainDispatcher: CoroutineDispatcher,
    private val bgDispatcher: CoroutineDispatcher,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    var uiState by savedStateHandle.saveable {
        mutableStateOf(NotificationsUiState())
    }
        private set

    @Parcelize
    data class NotificationsUiState(
        val user: User? = null,
        val notifications: List<Notification> = emptyList()
    ) : Parcelable

    init {
        viewModelScope.launch(bgDispatcher) {
            val user = userInteractor.getUser()
            val notifications = notificationsInteractor.getUserNotifications()

            withContext(mainDispatcher) {
                uiState = uiState.copy(user = user, notifications = notifications)
            }
        }
    }

    fun deleteNotification(notification: Notification) {
        viewModelScope.launch(bgDispatcher) {
            notificationsInteractor.delete(notification)

            val notifications = uiState.notifications.filter { it.id != notification.id }
            withContext(mainDispatcher) {
                uiState = uiState.copy(notifications = notifications)
            }
        }
    }
}
