package com.makesure.presentation.screens.settings

import android.annotation.SuppressLint
import android.content.Context
import android.os.Parcelable
import android.util.Log
import android.widget.Toast
import androidx.compose.runtime.mutableStateOf
import androidx.datastore.preferences.core.edit
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.SavedStateHandleSaveableApi
import androidx.lifecycle.viewmodel.compose.saveable
import androidx.navigation.NavHostController
import com.makesure.data.datastore.DataStoreKeys
import com.makesure.data.datastore.dataStore
import com.makesure.data.dto.user.User
import com.makesure.domain.entities.CountryCode
import com.makesure.domain.interactors.AuthInteractor
import com.makesure.domain.interactors.UserInteractor
import com.makesure.presentation.components.countryCodes
import com.makesure.presentation.navigation.graphs.Graphs
import com.makesure.presentation.navigation.graphs.SettingsGraph
import com.makesure.utils.popUpToTop
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.parcelize.Parcelize

@SuppressLint("StaticFieldLeak")
@OptIn(SavedStateHandleSaveableApi::class)
class SettingsViewModel(
    private val userInteractor: UserInteractor,
    private val authInteractor: AuthInteractor,
    savedStateHandle: SavedStateHandle,
    private val mainDispatcher: CoroutineDispatcher,
    private val bgDispatcher: CoroutineDispatcher,
    private val context: Context
) : ViewModel() {

    private val userId: String = checkNotNull(savedStateHandle["userId"])

    var uiState by savedStateHandle.saveable { mutableStateOf(SettingsUiState()) }

    @Parcelize
    data class SettingsUiState(
        val state: SettingsState = SettingsState.DEFAULT,
        val user: User? = null,
        val blockedUsers: List<User> = emptyList(),
        val areNotificationsEnabled: Boolean = false,
        val chosenLanguage: String = "EN",
        val shouldContinue: Boolean = false,
        val enteredNumber: String = "",
        val countryCode: CountryCode = countryCodes.first(),
        val code: String = "",
        val enteredEmail: String = "",
        val isResendEnabled: Boolean = true,
    ) : Parcelable

    enum class SettingsState {
        DEFAULT, CHANGE_PHONE, CHANGE_EMAIL, BLACKLIST,
        EMAIL_CHANGED, NUMBER_CHANGED, CODE
    }

    init {
        viewModelScope.launch(bgDispatcher) {
            try {
                val user = userInteractor.getUser()

                val areNotificationsEnabled = context.dataStore.data
                    .map { it[DataStoreKeys.notifications] ?: false }.first()

                val blockedUsers = userInteractor.getBlockedContacts()

                withContext(mainDispatcher) {
                    uiState = uiState.copy(
                        user = user,
                        areNotificationsEnabled = areNotificationsEnabled,
                        blockedUsers = blockedUsers
                    )
                }
            } catch (e: Exception) {
                Log.d(TAG, "init: ${e.localizedMessage}")
            }
        }
    }

    fun notificationStateClicked() {
        uiState = uiState.copy(areNotificationsEnabled = !uiState.areNotificationsEnabled)

        viewModelScope.launch(bgDispatcher) {
            context.dataStore.edit {
                it[DataStoreKeys.notifications] = uiState.areNotificationsEnabled
            }
        }
    }

    fun changeNumberClicked(navController: NavHostController) {
        uiState = uiState.copy(state = SettingsState.CHANGE_PHONE)
        navController.navigate("${SettingsGraph.CHANGE_PHONE}/$userId")
    }

    fun updateNumber(newNumber: String) {
        uiState = uiState.copy(
            enteredNumber = newNumber,
            shouldContinue = newNumber.length > 6
        )
    }

    fun updateCountryCode(newCode: CountryCode) {
        uiState = uiState.copy(countryCode = newCode)
    }

    fun phoneEntered() {
        uiState = uiState.copy(state = SettingsState.CODE, shouldContinue = false)
    }

    fun updateCode(code: String) {
        if (code.length > 6) return
        uiState = uiState.copy(
            code = code,
            shouldContinue = code.length == 6
        )
    }

    fun codeEntered() {
        uiState = uiState.copy(state = SettingsState.NUMBER_CHANGED)

        viewModelScope.launch(bgDispatcher) {
            try {
                val user = userInteractor.updatePhone(uiState.enteredNumber) ?: return@launch

                withContext(mainDispatcher) {
                    uiState = uiState.copy(user = user)
                }
            } catch (e: Exception) {
                Toast.makeText(context, "Не получилось обновить номер", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun phoneEditingFinished(navController: NavHostController) {
        uiState = uiState.copy(state = SettingsState.DEFAULT, shouldContinue = false)
        navController.popBackStack()
    }

    fun changeEmailClicked(navController: NavHostController) {
        uiState = uiState.copy(state = SettingsState.CHANGE_EMAIL)
        navController.navigate("${SettingsGraph.CHANGE_EMAIL}/$userId")
    }

    fun updateEmail(email: String) {
        uiState = uiState.copy(enteredEmail = email, shouldContinue = checkEmail(email))
    }

    fun emailEntered() {
        uiState = uiState.copy(state = SettingsState.EMAIL_CHANGED, shouldContinue = true)

        viewModelScope.launch(bgDispatcher) {
            try {
                val user = userInteractor.updateEmail(uiState.enteredEmail) ?: return@launch

                withContext(mainDispatcher) {
                    uiState = uiState.copy(user = user)
                }
            } catch (e: Exception) {
                Toast.makeText(context, "Не получилось обновить почту", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun emailEditingFinished(navController: NavHostController) {
        uiState = uiState.copy(state = SettingsState.DEFAULT, shouldContinue = false)
        navController.popBackStack()
    }


    fun backClicked(navController: NavHostController) {
        when (uiState.state) {
            SettingsState.DEFAULT -> navController.popBackStack()
            SettingsState.CODE -> {
                uiState = uiState.copy(state = SettingsState.CHANGE_PHONE, shouldContinue = true)
            }
            else -> {
                uiState = uiState.copy(state = SettingsState.DEFAULT, shouldContinue = false)
                navController.popBackStack()
            }
        }
    }

    fun blacklistClicked(navController: NavHostController) {
        uiState = uiState.copy(state = SettingsState.BLACKLIST)
        navController.navigate("${SettingsGraph.BLACKLIST}/$userId")
    }

    fun changeLanguage() {
        uiState = if (uiState.chosenLanguage == "EN") {
            uiState.copy(chosenLanguage = "RU")
        } else {
            uiState.copy(chosenLanguage = "EN")
        }
    }

    fun unblock(contact: User) {
        viewModelScope.launch(bgDispatcher) {
            try {
                val user = userInteractor.unblockContact(contact.id)
                val blockedUsers = userInteractor.getBlockedContacts()

                withContext(mainDispatcher) {
                    uiState = uiState.copy(
                        user = user,
                        blockedUsers = blockedUsers
                    )
                }
            } catch (e: Exception) {
                Log.d(TAG, "unblock: ${e.localizedMessage}")
            }
        }
    }

    fun signOut(navController: NavHostController) {
        viewModelScope.launch(bgDispatcher) {
            authInteractor.logout()

            withContext(mainDispatcher) {
                navController.navigate(Graphs.AUTH_ROUTE) {
                    popUpToTop(navController)
                }
            }
        }
    }

    fun deleteProfile(navController: NavHostController) {
        viewModelScope.launch(bgDispatcher) {
            authInteractor.logout()
            userInteractor.deleteProfile()

            withContext(mainDispatcher) {
                navController.navigate(Graphs.AUTH_ROUTE) {
                    popUpToTop(navController)
                }
            }
        }
    }

    private fun checkEmail(email: String): Boolean {
        if (!email.contains('@')) return false

        val beforeAt = email.substringBefore('@')
        val afterAt = email.substringAfter('@')
        return beforeAt.isNotBlank() && afterAt.isNotBlank()
    }

    companion object {
        private const val TAG: String = "SettingsViewModel"
    }
}
