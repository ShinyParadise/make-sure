package com.makesure.presentation.screens.sharedProfile

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.makesure.R
import com.makesure.app.theme.MakeSureTheme
import com.makesure.app.theme.PrimaryDarkGradient
import com.makesure.app.theme.mulishFonts
import com.makesure.presentation.components.previewUser
import com.makesure.presentation.components.views.CircleClickableUserPhoto
import com.makesure.presentation.components.views.GradientBox
import com.makesure.presentation.components.views.buttons.CloseButton
import com.makesure.presentation.components.views.buttons.FilledButton
import com.makesure.presentation.navigation.flows.ProfileUiFlow
import org.koin.androidx.compose.koinViewModel

@Composable
fun SharedProfileScreen(
    viewModel: SharedProfileViewModel = koinViewModel(),
    navController: NavHostController,
    onDismiss: () -> Unit,
) {
    val uiState = viewModel.uiState
    val uiFlow = ProfileUiFlow(
        onDismiss = onDismiss,
        onAddContactClick = {
            viewModel.addContact(navController)
        }
    )

    BackHandler(onBack = onDismiss)

    SharedProfileScreenContent(uiState, uiFlow)
}

@Composable
fun SharedProfileScreenContent(
    uiState: SharedProfileViewModel.ProfileUiState,
    uiFlow: ProfileUiFlow
) {
    GradientBox(
        paddingValues = PaddingValues(horizontal = 20.dp, vertical = 20.dp),
        brush = Brush.linearGradient(colors = PrimaryDarkGradient)
    ) {
        CloseButton(
            onClick = uiFlow.onDismiss,
            sizeDp = 30.dp
        )

        Column(
            modifier = Modifier
                .fillMaxWidth()
                .align(Alignment.Center),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Spacer(Modifier.height(40.dp))

            CircleClickableUserPhoto(
                photoURL = uiState.sharedUser?.photoURL,
                sizeDp = 300.dp
            )
            Spacer(Modifier.height(30.dp))

            Text(
                text = uiState.sharedUser?.name ?: "",
                fontFamily = mulishFonts,
                fontSize = 25.sp,
                fontWeight = FontWeight.Bold,
                color = Color.White
            )
        }

        Column(
            modifier = Modifier
                .align(Alignment.BottomCenter)
                .padding(horizontal = 40.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            FilledButton(
                text = stringResource(R.string.profile_add_to_contacts),
                onClick = uiFlow.onAddContactClick,
                modifier = Modifier.fillMaxWidth()
            )
            TextButton(
                onClick = { uiFlow.onDismiss }
            ) {
                Text(
                    text = stringResource(R.string.cancel),
                    fontSize = 16.sp,
                    fontWeight = FontWeight(800),
                    color = Color.White,
                )
            }
        }
    }
}

@Preview(showSystemUi = true)
@Composable
private fun SharedProfileScreen_Preview() {
    MakeSureTheme {
        SharedProfileScreenContent(
            uiState = SharedProfileViewModel.ProfileUiState(previewUser),
            uiFlow = ProfileUiFlow()
        )
    }
}
