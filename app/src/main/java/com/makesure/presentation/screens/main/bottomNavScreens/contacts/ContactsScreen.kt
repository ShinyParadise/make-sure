package com.makesure.presentation.screens.main.bottomNavScreens.contacts

import android.widget.Toast
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Card
import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.TextField
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Link
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.ClipboardManager
import androidx.compose.ui.platform.LocalClipboardManager
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import com.makesure.R
import com.makesure.app.theme.MakeSureTheme
import com.makesure.app.theme.mulishFonts
import com.makesure.data.dto.user.User
import com.makesure.presentation.components.previewUser
import com.makesure.presentation.components.views.ContactTile
import com.makesure.presentation.components.views.Title
import com.makesure.presentation.components.views.smallCalendar.SmallCalendar
import com.makesure.presentation.navigation.flows.MainUiFlow
import com.makesure.presentation.screens.main.ContactAction
import com.makesure.presentation.screens.main.ContactAction.ADD_DATE
import com.makesure.presentation.screens.main.ContactAction.BLOCK
import com.makesure.presentation.screens.main.ContactAction.DELETE
import com.makesure.presentation.screens.main.ContactAction.REPORT
import com.makesure.presentation.screens.main.ContactAction.SHARE_TEST
import com.makesure.presentation.screens.main.MainViewModel
import kotlinx.coroutines.launch

@Composable
fun ContactsScreen(
    modifier: Modifier = Modifier,
    uiState: MainViewModel.MainUiState,
    uiFlow: MainUiFlow
) {
    val contacts = uiState.contacts
    var selectedContact: User? by remember { mutableStateOf(null) }

    var showReportDialog by remember { mutableStateOf(false) }
    val context = LocalContext.current

    val dropDownItems = listOf(
        SHARE_TEST,
        ADD_DATE,
        DELETE,
        BLOCK,
        REPORT
    )

    LazyColumn(
        modifier = modifier.padding(horizontal = 16.dp)
    ) {
        item {
            Column(Modifier.padding(vertical = 16.dp)) {
                SmallCalendar(modifier = Modifier.fillMaxWidth()) {
                    uiFlow.onCalendarClick()
                }
                Spacer(modifier = Modifier.height(30.dp))
                TitleWithLink(uiFlow)
                Sorting(uiFlow.onChangeSort)
            }
        }

        if (contacts.isNotEmpty()) {
            items(items = contacts) { contact ->
                ContactItem(
                    modifier = Modifier.padding(bottom = 15.dp),
                    contact = contact,
                    uiState = uiState,
                    dropDownItems = dropDownItems,
                    onActionSelected = { user, action ->
                        uiFlow.onContactActionSelected(user, action)

                        if (action == REPORT) {
                            showReportDialog = true
                            selectedContact = contact
                        }
                    },
                    onContactClick = uiFlow.onContactClick,
                )
            }
        } else {
            item {
                Text(
                    text = stringResource(R.string.empty_contacts),
                    fontFamily = mulishFonts,
                    fontWeight = FontWeight(600),
                    fontSize = 16.sp,
                    color = Color.Black.copy(alpha = 0.25f)
                )
                Spacer(modifier = Modifier.height(10.dp))
                Text(
                    text = stringResource(R.string.empty_contacts_link),
                    fontFamily = mulishFonts,
                    fontWeight = FontWeight(600),
                    fontSize = 16.sp,
                    color = Color.Black.copy(alpha = 0.25f)
                )
            }
        }
    }

    if (showReportDialog) {
        var report by remember { mutableStateOf("") }

        ReportDialog(
            onDismiss = {
                showReportDialog = false
            },
            onReport = {
                showReportDialog = false
                selectedContact?.let { uiFlow.onReport(report, it) }
                Toast.makeText(context, context.getString(R.string.report_toast), Toast.LENGTH_SHORT).show()
            },
            value = report,
            onValueChange = { if (it.length < 250)  report = it }
        )
    }
}

@Composable
private fun Sorting(
    onChangeSort: () -> Unit,
) {
    Row(
        modifier = Modifier.fillMaxWidth().offset(y = (-10).dp),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            text = stringResource(R.string.sort_by_date),
            fontSize = 14.sp,
            color = MaterialTheme.colors.primary
        )
        IconButton(onClick = onChangeSort) {
            Icon(
                painter = painterResource(R.drawable.arrows_sort),
                contentDescription = null,
                tint = MaterialTheme.colors.primary
            )
        }
    }
}

@Composable
private fun ReportDialog(
    onDismiss: () -> Unit,
    onReport: () -> Unit,
    value: String,
    onValueChange: (String) -> Unit,
) {
    Dialog(onDismissRequest = onDismiss) {
        Card(
            shape = MaterialTheme.shapes.small,
        ) {
            Column(
                modifier = Modifier.padding(10.dp),
            ) {
                Text(
                    text = "Причина жалобы",
                    fontWeight = FontWeight.Bold,
                    fontSize = 20.sp,
                )

                TextField(
                    value = value,
                    onValueChange = onValueChange,
                    maxLines = 12,
                    modifier = Modifier.fillMaxWidth()
                )

                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.End,
                ) {
                    TextButton(onClick = onDismiss) {
                        Text(
                            stringResource(R.string.cancel),
                            fontSize = 16.sp
                        )
                    }
                    TextButton(onClick = onReport) {
                        Text(
                            stringResource(R.string.report),
                            color = Color.Red,
                            fontSize = 16.sp
                        )
                    }
                }
            }
        }
    }
}

@Composable
private fun ContactItem(
    modifier: Modifier = Modifier,
    contact: User,
    uiState: MainViewModel.MainUiState,
    dropDownItems: List<ContactAction>,
    onActionSelected: (contact: User, ContactAction) -> Unit,
    onContactClick: (User) -> Unit,
) {
    val density = LocalDensity.current
    val interactionSource = remember { MutableInteractionSource() }
    var isMenuVisible by remember { mutableStateOf(false) }
    var itemWidth by remember { mutableStateOf(0.dp) }

    Box(
        Modifier.onSizeChanged {
            itemWidth = with(density) { it.width.toDp() }
        }
    ) {
        ContactTile(
            contact = contact,
            onDetailsClick = {
                isMenuVisible = true
            },
            onClick = { onContactClick(contact) },
            modifier = modifier.fillMaxWidth(),
            uiState = uiState,
            interactionSource = interactionSource,
        )

        DropdownMenu(
            expanded = isMenuVisible,
            onDismissRequest = { isMenuVisible = false },
            offset = DpOffset.Zero.copy(x = itemWidth)
        ) {
            dropDownItems.forEach {
                DropdownMenuItem(onClick = {
                    onActionSelected(contact, it)
                    isMenuVisible = false
                }) {
                    ContactDropDownItem(it)
                }
            }
        }
    }
}

@Composable
private fun ContactDropDownItem(item: ContactAction) {
    Column {
        Text(
            text = item.action,
            fontFamily = mulishFonts,
            fontSize = 14.sp,
            color = when (item) {
                DELETE, BLOCK, REPORT -> Color.Red
                else -> MaterialTheme.colors.primary
            }
        )
    }
}

@Composable
private fun TitleWithLink(uiFlow: MainUiFlow) {
    val clipboardManager: ClipboardManager = LocalClipboardManager.current
    val scope = rememberCoroutineScope()

    Row(verticalAlignment = Alignment.CenterVertically) {
        Title(
            text = stringResource(R.string.contacts_title),
            fontSize = 24.sp,
            color = MaterialTheme.colors.primary
        )
        IconButton(onClick = {
            scope.launch {
                val text = uiFlow.onLinkClick() ?: return@launch
                clipboardManager.setText(AnnotatedString(text))
            }
        }) {
            Icon(
                imageVector = Icons.Default.Link,
                contentDescription = null,
                tint = MaterialTheme.colors.primary
            )
        }
    }
}

@Preview(showSystemUi = true)
@Composable
private fun ContactsScreen_Preview() {
    MakeSureTheme {
        ContactsScreen(
            uiState = MainViewModel.MainUiState(
                user = previewUser,
                contacts = listOf(
                    previewUser,
                    previewUser,
                    previewUser,
                    previewUser,
                    previewUser,
                    previewUser
                ),
                contactsLabelColors = mapOf(previewUser to Color(0xFF03A9F4)),
                contactsLabels = mapOf(previewUser to "123")
            ),
            uiFlow = MainUiFlow()
        )
    }
}
