package com.makesure.presentation.screens.testResult

import android.os.Parcelable
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.SavedStateHandleSaveableApi
import androidx.lifecycle.viewmodel.compose.saveable
import androidx.navigation.NavHostController
import com.makesure.data.dto.user.User
import com.makesure.domain.interactors.NotificationsInteractor
import com.makesure.domain.interactors.UserInteractor
import com.makesure.presentation.navigation.graphs.Graphs
import com.makesure.presentation.screens.testResult.TestResultViewModel.TestResultState.ALERT
import com.makesure.presentation.screens.testResult.TestResultViewModel.TestResultState.GOT_NEGATIVE
import com.makesure.presentation.screens.testResult.TestResultViewModel.TestResultState.GOT_POSITIVE
import com.makesure.presentation.screens.testResult.TestResultViewModel.TestResultState.GO_BACK
import com.makesure.presentation.screens.testResult.TestResultViewModel.TestResultState.NOTIFY
import com.makesure.presentation.screens.testResult.TestResultViewModel.TestResultState.TIPS
import com.makesure.utils.popUpToTop
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.parcelize.Parcelize

@OptIn(SavedStateHandleSaveableApi::class)
class TestResultViewModel(
    private val userInteractor: UserInteractor,
    private val notificationsInteractor: NotificationsInteractor,
    private val mainDispatcher: CoroutineDispatcher,
    private val bgDispatcher: CoroutineDispatcher,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val _result: String = checkNotNull(savedStateHandle["result"])

    init {
        viewModelScope.launch(bgDispatcher) {
            val fetchedUser = userInteractor.getUser()

            val allContacts = fetchedUser.contacts?.mapNotNull {
                userInteractor.getById(it)
            } ?: return@launch

            val highRiskUsers = allContacts.take(1)

            val possibleRiskUsers = if (allContacts.size > 1) {
                 allContacts.slice(1..allContacts.lastIndex)
            } else emptyList()

            withContext(mainDispatcher) {
                uiState = uiState.copy(
                    user = fetchedUser,
                    highRiskUsers = highRiskUsers,
                    possibleRiskUsers = possibleRiskUsers
                )
            }
        }
    }

    var uiState by savedStateHandle.saveable {
        mutableStateOf(TestResultUiState(result = _result))
    }
        private set

    @Parcelize
    data class TestResultUiState(
        val result: String = "",
        val user: User? = null,
        val highRiskUsers: List<User> = emptyList(),
        val possibleRiskUsers: List<User> = emptyList(),
        val state: TestResultState = getStateForResult(result)
    ) : Parcelable {
        companion object {
            fun getStateForResult(result: String): TestResultState {
                return if (result.lowercase() == POSITIVE) GOT_POSITIVE
                else GOT_NEGATIVE
            }

            const val POSITIVE = "positive"
            const val NEGATIVE = "negative"
        }
    }

    enum class TestResultState {
        GOT_POSITIVE, GOT_NEGATIVE, TIPS, NOTIFY, GO_BACK, ALERT
    }

    fun onResultReceived(navController: NavHostController) {
        if (uiState.state == GOT_NEGATIVE) {
            navigateToHome(navController)
        } else if (uiState.state == GOT_POSITIVE) {
            uiState = uiState.copy(state = TIPS)
        }
    }

    fun onTipsContinue() {
        uiState = uiState.copy(state = NOTIFY)
    }

    fun onBackClick(navController: NavHostController) {
        when (uiState.state) {
            GOT_POSITIVE, GOT_NEGATIVE -> {}

            TIPS -> {
                uiState = if (uiState.result == TestResultUiState.POSITIVE) {
                    uiState.copy(state = GOT_POSITIVE)
                } else {
                    uiState.copy(state = GOT_NEGATIVE)
                }
            }

            NOTIFY -> uiState = uiState.copy(state = TIPS)
            GO_BACK -> uiState = uiState.copy(state = NOTIFY)
            ALERT -> navigateToHome(navController)
        }
    }

    private fun navigateToHome(navController: NavHostController) {
        resetUiState()
        navController.navigate(Graphs.BOTTOM_NAV_ROUTE) {
            popUpToTop(navController)
            launchSingleTop = true
        }
    }

    private fun resetUiState() {
        uiState = TestResultUiState()
    }

    fun onSendNotificationsClick(users: List<User>) {
        uiState = if (users.isEmpty() && uiState.possibleRiskUsers.isNotEmpty()) {
            uiState.copy(state = GO_BACK)
        }
        else {
            uiState.copy(state = ALERT)
        }

        notifyUsers(users)
    }

    fun onDismissNotifications() {
        uiState = uiState.copy(state = ALERT)
    }

    private fun notifyUsers(possibleRiskUsers: List<User>) {
        val allSelectedUsers = possibleRiskUsers + uiState.highRiskUsers

        viewModelScope.launch(bgDispatcher) {
            for (user in allSelectedUsers) {
                notificationsInteractor.sendNotification()
            }
        }
    }
}
