package com.makesure.presentation.screens.main.bottomNavScreens.scan

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.net.Uri
import android.util.Log
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.ImageProxy
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavHostController
import com.makesure.data.dto.user.User
import com.makesure.domain.interactors.ScanInteractor
import com.makesure.domain.interactors.UserInteractor
import com.makesure.presentation.navigation.graphs.MainRoutes
import com.makesure.utils.popUpToTop
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


@SuppressLint("StaticFieldLeak")
class ScanViewModel(
    private val userInteractor: UserInteractor,
    private val scanInteractor: ScanInteractor,
    private val mainDispatcher: CoroutineDispatcher,
    private val bgDispatcher: CoroutineDispatcher,
) : ViewModel() {

    private val _uiState = MutableStateFlow(ScanUiState())
    val uiState: StateFlow<ScanUiState> = _uiState.asStateFlow()

    data class ScanUiState(
        val state: ScanState = ScanState.DEFAULT,
        val image: Bitmap? = null,
        val result: String = "Unknown",
        val user: User? = null
    )

    init {
        viewModelScope.launch(bgDispatcher) {
            val user = userInteractor.getUser()

            _uiState.value = _uiState.value.copy(user = user)
        }
    }

    enum class ScanState { DEFAULT, QR_SCANNER, IMAGE_TAKEN, ERROR, SUCCESS, LOADING }

    val imageCapturedCallback = object : ImageCapture.OnImageCapturedCallback() {
        override fun onCaptureSuccess(image: ImageProxy) = onImageTaken(image)

        override fun onError(e: ImageCaptureException) {
            Log.d(TAG, "onSendClicked: ${e.printStackTrace()}")
            errorHappened()
        }
    }

    fun selectQrScanner() {
        _uiState.value = _uiState.value.copy(state = ScanState.QR_SCANNER)
    }

    fun selectTestScanner() {
        _uiState.value = _uiState.value.copy(state = ScanState.DEFAULT)
    }

    fun onQrCodeScanned(result: String, navController: NavHostController) {
        if (isQrLinkCorrect(result)) {
            _uiState.value = _uiState.value.copy(state = ScanState.DEFAULT)

            val uri = Uri.parse(result)
            navController.navigate(uri)
        }
    }

    fun onSendClicked() {
        val bitmap = uiState.value.image ?: return

        _uiState.value = _uiState.value.copy(state = ScanState.LOADING)

        viewModelScope.launch(bgDispatcher) {
            try {
                val response = scanInteractor.getScanResult(bitmap)

                if (response.result == null)
                    throw Exception("Error code: ${response.errorCode}")

                resetImage()

                withContext(mainDispatcher) {
                    _uiState.value = _uiState.value.copy(
                        state = ScanState.SUCCESS,
                        result = response.result
                    )
                }
            } catch (e: Exception) {
                Log.d(TAG, "onSendClicked: ${e.printStackTrace()}")

                withContext(mainDispatcher) {
                    _uiState.value = _uiState.value.copy(state = ScanState.ERROR)
                }

                resetImage()
            }
        }
    }

    fun onRetryClicked() {
        _uiState.value = _uiState.value.copy(state = ScanState.DEFAULT)
        resetImage()
    }

    fun resultReceived(navController: NavHostController) {
        navController
            .navigate("${MainRoutes.TEST_RESULTS}/${uiState.value.user?.id}/${uiState.value.result}"
            ) {
                popUpToTop(navController)
            }

        resetUiState()
    }

    private fun onImageTaken(image: ImageProxy) {
        _uiState.value = _uiState.value.copy(state = ScanState.IMAGE_TAKEN, image = image.toBitmap())
    }

    private fun errorHappened() {
        _uiState.value = _uiState.value.copy(state = ScanState.ERROR)
        resetImage()
    }

    private fun resetUiState() {
        _uiState.value = ScanUiState()
    }

    private fun isQrLinkCorrect(payload: String): Boolean {
        return payload matches Regex("^makesure://open/[0-9A-Fa-f]{8}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{12}\$")
    }

    private fun resetImage() {
        viewModelScope.launch(bgDispatcher) {
            scanInteractor.cleanup()
            withContext(mainDispatcher) {
                _uiState.value = _uiState.value.copy(image = null)
            }
        }
    }

    companion object {
        private const val TAG = "ScanViewModel"
    }
}
