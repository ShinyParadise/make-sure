package com.makesure.presentation.screens.settings

import android.app.LocaleManager
import android.os.Build
import android.os.LocaleList
import androidx.appcompat.app.AppCompatDelegate
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.AlertDialog
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ExitToApp
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.os.LocaleListCompat
import com.makesure.R
import com.makesure.app.theme.MakeSureTheme
import com.makesure.app.theme.PrimaryDarkGradient
import com.makesure.presentation.components.previewUser
import com.makesure.presentation.navigation.flows.SettingsUiFlow

@Composable
fun SettingsScreen(
    modifier: Modifier = Modifier,
    uiState: SettingsViewModel.SettingsUiState,
    uiFlow: SettingsUiFlow
) {
    val scrollState = rememberScrollState()

    Box(modifier = modifier
        .fillMaxSize()
        .background(brush = Brush.linearGradient(PrimaryDarkGradient))
    ) {
        Column(modifier = Modifier
            .padding(vertical = 20.dp, horizontal = 24.dp)
            .verticalScroll(scrollState)
        ) {
            SettingsButtons(
                uiState = uiState,
                uiFlow = uiFlow
            )
        }
    }
}

@Composable
private fun SettingsButtons(
    uiState: SettingsViewModel.SettingsUiState,
    uiFlow: SettingsUiFlow
) {
    var openDeleteProfileDialog by remember { mutableStateOf(false) }
    var openLogoutDialog by remember { mutableStateOf(false) }

    val buttonsPadding = 20.dp
    val uriHandler = LocalUriHandler.current
    val context = LocalContext.current

    SettingsButtonWithDropdownMenu(
        painter = painterResource(R.drawable.icon_lang),
        settingsText = stringResource(id = R.string.settings_lang),
        sizeDp = 21.dp,
        chosenValue = uiState.chosenLanguage,
        onValueChoose = {
            uiFlow.onChangeLanguageClick()
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                context.getSystemService(LocaleManager::class.java)
                    .applicationLocales = LocaleList.forLanguageTags(uiState.chosenLanguage)
            } else {
                AppCompatDelegate.setApplicationLocales(
                    LocaleListCompat.forLanguageTags(uiState.chosenLanguage)
                )
            }
        },
        modifier = Modifier.padding(bottom = buttonsPadding)
    )
    SettingsButton(
        painter = painterResource(R.drawable.icon_email),
        settingsText = stringResource(
            if (uiState.user?.email.isNullOrEmpty()) R.string.settings_add_email
            else R.string.settings_email
        ),
        subtitleText = uiState.user?.email,
        onClick = uiFlow.onAddEmailClick,
        modifier = Modifier.padding(bottom = buttonsPadding)
    )
    SettingsButton(
        painter = painterResource(R.drawable.icon_phone),
        settingsText = stringResource(id = R.string.settings_phone),
        subtitleText = uiState.user?.phone ?: stringResource(id = R.string.settings_phone_error),
        onClick = uiFlow.onChangeNumberClick,
        modifier = Modifier.padding(bottom = buttonsPadding)
    )
    SettingsButton(
        painter = painterResource(R.drawable.icon_help),
        settingsText = stringResource(id = R.string.settings_help),
        onClick = {
            uriHandler.openUri("https://makesure.app/faq")
        },
        modifier = Modifier.padding(bottom = buttonsPadding)
    )
    SettingsButton(
        painter = painterResource(R.drawable.icon_legal),
        settingsText = stringResource(id = R.string.settings_legal),
        onClick = {
            uriHandler.openUri("https://makesure.app/license_agreement")
        },
        modifier = Modifier.padding(bottom = buttonsPadding)
    )
    SettingsButton(
        painter = painterResource(R.drawable.icon_privacy),
        settingsText = stringResource(id = R.string.settings_privacy),
        onClick = {
            uriHandler.openUri("https://makesure.app/confidentiality")
        },
        modifier = Modifier.padding(bottom = buttonsPadding)
    )
    SettingsButton(
        painter = painterResource(R.drawable.icon_blacklist),
        settingsText = stringResource(id = R.string.settings_blacklist),
        onClick = uiFlow.onBlacklistClick,
        modifier = Modifier.padding(bottom = buttonsPadding)
    )
    SettingsButton(
        painter = painterResource(R.drawable.icon_delete),
        settingsText = stringResource(id = R.string.settings_delete),
        onClick = {
            openDeleteProfileDialog = true
        },
        modifier = Modifier.padding(bottom = buttonsPadding)
    )
    SettingsButton(
        imageVector = Icons.Default.ExitToApp,
        settingsText = stringResource(id = R.string.profile_sign_out),
        onClick = {
            openLogoutDialog = true
        },
        modifier = Modifier.padding(bottom = buttonsPadding)
    )

    if (openLogoutDialog) {
        ConfirmationAlertDialog(
            title = stringResource(R.string.settings_logout_title),
            confirmText = stringResource(R.string.settings_logout),
            onConfirm = {
                openLogoutDialog = false
                uiFlow.onSignOut()
            },
            onDismiss = {
                openLogoutDialog = false
            }
        )
    }

    if (openDeleteProfileDialog) {
        ConfirmationAlertDialog(
            title = stringResource(R.string.settings_delete_confirmation),
            confirmText = stringResource(R.string.settings_delete_button),
            text = stringResource(R.string.delete_profile_description),
            confirmTextColor = Color.Red,
            onConfirm = {
                openDeleteProfileDialog = false
                uiFlow.onDeleteProfileClick()
            },
            onDismiss = {
                openDeleteProfileDialog = false
            }
        )
    }
}

@Composable
private fun ConfirmationAlertDialog(
    title: String,
    text: String = "",
    confirmText: String,
    confirmTextColor: Color = MaterialTheme.colors.primary,
    onConfirm: () -> Unit = {},
    onDismiss: () -> Unit,
) {
    AlertDialog(
        title = { Text(title) },
        text = { if (text.isNotEmpty()) { Text(text) } },
        onDismissRequest = onDismiss,
        dismissButton = {
            TextButton(onClick = onDismiss) {
                Text(text = stringResource(R.string.cancel))
            }
        },
        confirmButton = {
            TextButton(onClick = onConfirm) {
                Text(
                    text = confirmText,
                    color = confirmTextColor,
                )
            }
        }
    )

}

@Preview(showSystemUi = true)
@Composable
private fun SettingsScreen_Preview() {
    MakeSureTheme {
        SettingsScreen(
            uiState = SettingsViewModel.SettingsUiState(
                user = previewUser
            ),
            uiFlow = SettingsUiFlow()
        )
    }
}

@Preview(showSystemUi = true, locale = "RU")
@Composable
private fun SettingsScreen_PreviewRU() {
    MakeSureTheme {
        SettingsScreen(
            uiState = SettingsViewModel.SettingsUiState(
                user = previewUser
            ),
            uiFlow = SettingsUiFlow()
        )
    }
}
