package com.makesure.presentation.screens.sharedProfile

import android.os.Parcelable
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.SavedStateHandleSaveableApi
import androidx.lifecycle.viewmodel.compose.saveable
import androidx.navigation.NavController
import com.makesure.data.dto.user.User
import com.makesure.domain.interactors.LinksInteractor
import com.makesure.domain.interactors.UserInteractor
import com.makesure.presentation.navigation.graphs.Graphs
import com.makesure.utils.popUpToTop
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.parcelize.Parcelize

@OptIn(SavedStateHandleSaveableApi::class)
class SharedProfileViewModel(
    private val userInteractor: UserInteractor,
    private val linksInteractor: LinksInteractor,
    private val mainDispatcher: CoroutineDispatcher,
    private val bgDispatcher: CoroutineDispatcher,
    savedStateHandle: SavedStateHandle,
) : ViewModel() {

    private val userId: String? = savedStateHandle["contactId"]
    private val inviteId: String? = savedStateHandle["inviteId"]

    var uiState  by savedStateHandle.saveable { mutableStateOf(ProfileUiState()) }

    @Parcelize
    data class ProfileUiState(
        val sharedUser: User? = null,
        val user: User? = null,
    ) : Parcelable

    init {
        viewModelScope.launch(bgDispatcher) {
            var user: User? = null

            if (userId != null) {
                user = userInteractor.getUser()
            } else if (inviteId != null) {
                val link = linksInteractor.getById(inviteId)
                user = userInteractor.getById(link.userId)
            }

            withContext(mainDispatcher) {
                uiState = uiState.copy(user = user)
            }
        }
    }

    fun addContact(navController: NavController) {
        viewModelScope.launch(bgDispatcher) {
            val userToUpdate = uiState.user ?: return@launch
            val newContact = uiState.sharedUser ?: return@launch

            if (userToUpdate.contacts?.contains(newContact.id) == true) {
                navigateToHome(navController)
                return@launch
            }

            userInteractor.addContacts(newContact.id).contacts

            withContext(mainDispatcher) { uiState = ProfileUiState() }

            navigateToHome(navController)
        }
    }

    private fun navigateToHome(navController: NavController) {
        navController.navigate(Graphs.BOTTOM_NAV_ROUTE) {
            popUpToTop(navController)
        }
    }
}
