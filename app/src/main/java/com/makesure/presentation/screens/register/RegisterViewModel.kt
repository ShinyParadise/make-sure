package com.makesure.presentation.screens.register

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.os.Parcelable
import android.util.Log
import android.widget.Toast
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.SavedStateHandleSaveableApi
import androidx.lifecycle.viewmodel.compose.saveable
import androidx.navigation.NavHostController
import com.makesure.R
import com.makesure.data.dto.user.NewUser
import com.makesure.data.dto.user.Sex
import com.makesure.data.serializers.ZonedDateTimeParceler
import com.makesure.domain.entities.CountryCode
import com.makesure.domain.interactors.AuthInteractor
import com.makesure.domain.interactors.UserInteractor
import com.makesure.presentation.components.countryCodes
import com.makesure.presentation.navigation.graphs.Graphs
import com.makesure.utils.formattedString
import com.makesure.utils.popUpToTop
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.WriteWith
import java.time.LocalDate
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

@OptIn(SavedStateHandleSaveableApi::class)
@SuppressLint("StaticFieldLeak")
class RegisterViewModel(
    private val context: Context,
    private val userInteractor: UserInteractor,
    private val authInteractor: AuthInteractor,
    private val mainDispatcher: CoroutineDispatcher,
    private val bgDispatcher: CoroutineDispatcher,
    savedStateHandle: SavedStateHandle
) : ViewModel() {
    enum class RegisterState {
        NUMBER, CODE, EMAIL, FIRST_NAME, BIRTHDAY, AGREEMENT
    }

    @Parcelize
    data class RegisterUiState(
        val state: RegisterState = RegisterState.NUMBER,
        val progress: Float = 0.125f,
        val phoneNumber: String = "",
        val countryCode: CountryCode = countryCodes.first(),
        val code: String = "",
        val email: String = "",
        val firstName: String = "",
        val birthdayString: String = "",
        val birthdayDate: @WriteWith<ZonedDateTimeParceler> ZonedDateTime? = null,
        val shouldContinue: Boolean = false,
        val photoUri: Uri? = null,
        val newUser: NewUser? = null,
        val isCodeCorrect: Boolean = false,
        val isResendEnabled: Boolean = true,
    ) : Parcelable {
        val fullNumber: String
            get() = countryCode.code + phoneNumber
    }

    var uiState by savedStateHandle.saveable { mutableStateOf(RegisterUiState()) }
        private set

    fun updatePhoneNumber(phoneNumber: String) {
        uiState = uiState.copy(
            phoneNumber = phoneNumber,
            shouldContinue = phoneNumber.length in 8..14,
        )
    }

    fun phoneEntered() {
        viewModelScope.launch(bgDispatcher) {
            val user = userInteractor.getByPhone(uiState.fullNumber)

            if (user != null) {
                withContext(mainDispatcher) {
                    Toast.makeText(
                        context,
                        context.getString(R.string.register_already_exists),
                        Toast.LENGTH_SHORT
                    ).show()
                    resetToPhoneEnter()
                }
            } else {
                uiState = uiState.copy(
                    state = RegisterState.CODE,
                    progress = 0.25f,
                    shouldContinue = false,
                )

                sendSmsWithCode()
            }
        }
    }

    fun updateCountryCode(countryCode: CountryCode) {
        uiState = uiState.copy(countryCode = countryCode)
    }

    fun updateCode(code: String) {
        if (code.length > 6) return
        uiState = uiState.copy(
            code = code,
            shouldContinue = code.length == 6,
            isCodeCorrect = checkCode(code)
        )
    }

    fun sendSmsWithCode() {
        viewModelScope.launch(bgDispatcher) {
            authInteractor.sendVerificationCode(uiState.fullNumber)
        }
    }

    fun codeEntered() {
        if (uiState.isCodeCorrect) {
            uiState = uiState.copy(
                state = RegisterState.EMAIL,
                progress = 0.5f,
                shouldContinue = false
            )
        }
    }

    fun updateEmail(email: String) {
        uiState = uiState.copy(
            email = email,
            shouldContinue = checkEmail(email)
        )
    }

    private fun checkEmail(email: String): Boolean {
        if (!email.contains('@')) return false

        val beforeAt = email.substringBefore('@')
        val afterAt = email.substringAfter('@')
        return beforeAt.isNotBlank() && afterAt.isNotBlank()
    }

    fun emailEntered() {
        uiState = uiState.copy(
            state = RegisterState.FIRST_NAME,
            progress = 0.675f,
        )
    }

    fun updateName(firstName: String) {
        uiState = uiState.copy(
            firstName = firstName,
            shouldContinue = firstName.length > 2
        )
    }

    fun nameEntered() {
        uiState = uiState.copy(
            state = RegisterState.BIRTHDAY,
            progress = 0.75f,
            shouldContinue = false,
        )
    }

    fun updateBirthday(birthday: String) {
        uiState = uiState.copy(
            birthdayString = birthday,
            shouldContinue = checkBirthday(birthday)
        )
    }

    fun birthdayEntered() {
        val isBirthdayCorrect = checkBirthday(uiState.birthdayString)

        uiState = uiState.copy(
            state = RegisterState.AGREEMENT,
            progress = 0.875f,
            birthdayDate = if (isBirthdayCorrect) parseBirthday(uiState.birthdayString)
            else null
        )
    }

    fun registerNewUser(navController: NavHostController) {
        val birthday = uiState.birthdayDate ?: return

        val user = NewUser(
            name = uiState.firstName,
            phone = uiState.fullNumber,
            email = uiState.email,
            birthday = birthday.formattedString(),
            photoURL = uiState.photoUri?.toString(),
            blockedUsers = null,
            contacts = null
        )

        uiState = uiState.copy(
            newUser = user,
            progress = 0f
        )

        registerUser(navController)
    }

    private fun registerUser(navController: NavHostController) {
        viewModelScope.launch(bgDispatcher) {
            try {
                val newUser = uiState.newUser ?: throw Exception("Couldn't create such user")
                authInteractor.register(newUser)

                withContext(mainDispatcher) {
                    navigateToHome(navController)
                }
            } catch (e: Exception) {
                Log.d(TAG, "registerUser: ${e.printStackTrace()}")

                withContext(mainDispatcher) {
                    Toast.makeText(context,
                        context.getString(R.string.register_try_again), Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun navigateToHome(navController: NavHostController) {
        navController.navigate(Graphs.BOTTOM_NAV_ROUTE) {
            popUpToTop(navController)
            launchSingleTop = true
        }
    }

    fun backPressed(navController: NavHostController) {
        when (uiState.state) {
            RegisterState.NUMBER -> {
                navController.navigate(Graphs.AUTH_ROUTE) {
                    popUpToTop(navController)
                }
            }
            RegisterState.CODE -> { uiState = uiState.copy(state = RegisterState.NUMBER) }
            RegisterState.EMAIL -> { uiState = uiState.copy(state = RegisterState.CODE) }
            RegisterState.FIRST_NAME -> { uiState = uiState.copy(state = RegisterState.EMAIL) }
            RegisterState.BIRTHDAY -> { uiState = uiState.copy(state = RegisterState.FIRST_NAME) }
            RegisterState.AGREEMENT -> { uiState = uiState.copy(state = RegisterState.BIRTHDAY) }
        }
    }

    fun skipPressed() {
        when (uiState.state) {
            RegisterState.EMAIL -> {
                uiState = uiState.copy(state = RegisterState.FIRST_NAME, progress = 0.675f)
            }
            else -> {}
        }
    }

    private fun parseBirthday(birthday: String): ZonedDateTime {
        val formatter = DateTimeFormatter.ofPattern("ddMMyyyy")
        val localDate = LocalDate.parse(birthday, formatter)

        return localDate.atStartOfDay(ZoneId.systemDefault())
    }

    private fun checkBirthday(birthday: String) =
        birthday matches Regex("^([0-2][0-9]|3[0-1])(0[0-9]|1[0-2])([0-9][0-9])?[1-2][0-9][0-9][0-9]$")

    private fun checkCode(code: String): Boolean {
        return if (code.isNotBlank()) code.toInt() == authInteractor.randomCode
        else false
    }

    private fun resetToPhoneEnter() {
        uiState = uiState.copy(state = RegisterState.NUMBER, phoneNumber = "")
    }

    companion object {
        private const val TAG = "RegisterViewModel"
    }
}
