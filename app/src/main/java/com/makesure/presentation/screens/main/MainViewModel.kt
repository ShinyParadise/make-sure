package com.makesure.presentation.screens.main

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.os.Parcelable
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.graphics.Color
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.SavedStateHandleSaveableApi
import androidx.lifecycle.viewmodel.compose.saveable
import androidx.navigation.NavHostController
import com.makesure.R
import com.makesure.data.dto.post.Post
import com.makesure.data.dto.test.Test
import com.makesure.data.dto.user.User
import com.makesure.data.serializers.ColorParceler
import com.makesure.domain.entities.OfflinePost
import com.makesure.domain.entities.TestSet
import com.makesure.domain.interactors.AuthInteractor
import com.makesure.domain.interactors.LinksInteractor
import com.makesure.domain.interactors.MeetingsInteractor
import com.makesure.domain.interactors.ReportsInteractor
import com.makesure.domain.interactors.TestsInteractor
import com.makesure.domain.interactors.UserInteractor
import com.makesure.presentation.navigation.graphs.Graphs
import com.makesure.presentation.navigation.graphs.MainRoutes
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.WriteWith
import java.util.concurrent.TimeUnit
import kotlin.concurrent.fixedRateTimer

@OptIn(SavedStateHandleSaveableApi::class)
@SuppressLint("StaticFieldLeak")
class MainViewModel(
    savedStateHandle: SavedStateHandle,
    private val userInteractor: UserInteractor,
    private val meetingsInteractor: MeetingsInteractor,
    private val authInteractor: AuthInteractor,
    private val testsInteractor: TestsInteractor,
    private val linksInteractor: LinksInteractor,
    private val reportsInteractor: ReportsInteractor,
    private val mainDispatcher: CoroutineDispatcher,
    private val bgDispatcher: CoroutineDispatcher,
    private val context: Context,
) : ViewModel() {
    var uiState by savedStateHandle.saveable { mutableStateOf(MainUiState()) }
        private set

    private var isSortedAscending = true

    init {
        try {
            fixedRateTimer(period = TimeUnit.SECONDS.toMillis(3)) {
                getUserInfo()
            }
        } catch (e: Exception) {
            uiState = uiState.copy(isLoading = false, hasError = true)
        }
    }

    @Parcelize
    data class MainUiState(
        val posts: List<Post> = emptyList(),
        val offlinePosts: List<OfflinePost> = previewPosts,
        val user: User? = null,
        val contacts: List<User> = emptyList(),
        val allTests: List<Test> = emptyList(),
        val testSets: List<TestSet> = emptyList(),
        val isLoading: Boolean = true,
        val hasError: Boolean = false,
        val contactsLabels: Map<User, String?> = emptyMap(),
        val contactsLabelColors: Map<User, @WriteWith<ColorParceler> Color?> = emptyMap(),
    ) : Parcelable

    suspend fun getProfileLink(): String? {
        return try { linksInteractor.getCurrentUserLink() }
        catch (_: Exception) { null }
    }

    fun handleContactAction(contact: User, action: ContactAction) {
        when (action) {
            ContactAction.DELETE -> {
                handleDelete(contact)
                Toast
                    .makeText(context, "${contact.name} " + context.getString(R.string.deleted), Toast.LENGTH_SHORT)
                    .show()

            }
            ContactAction.BLOCK -> {
                handleBlock(contact)
                Toast
                    .makeText(context, "${contact.name} " + context.getString(R.string.blocked), Toast.LENGTH_SHORT)
                    .show()
            }
            else -> {}
        }
    }

    fun navigateToQrScreen(navController: NavHostController) {
        navController.navigate("${MainRoutes.SHARE}/${uiState.user?.id}")
    }

    fun navigateToNotifications(navController: NavHostController) {
        navController.navigate("${MainRoutes.NOTIFICATIONS}/${uiState.user?.id}")
    }

    fun navigateToSettings(navController: NavHostController) {
        navController.navigate("${Graphs.SETTINGS_ROUTE}/${uiState.user?.id}")
    }

    fun loginUser(userId: String) {
        viewModelScope.launch(bgDispatcher) {
            val user = authInteractor.login(userId)
            updateUser(user)
        }
    }

    fun sendReport(report: String, contact: User) {
        viewModelScope.launch(bgDispatcher) {
            reportsInteractor.reportUser(contact.id, report)
        }
    }

    @Suppress("DEPRECATION")
    fun uploadPhoto(uri: Uri) {
        viewModelScope.launch(bgDispatcher) {
            try {
                val bitmap = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    val src = ImageDecoder.createSource(context.contentResolver, uri)
                    ImageDecoder.decodeBitmap(src) { decoder, _, _ ->
                        decoder.allocator = ImageDecoder.ALLOCATOR_SOFTWARE
                        decoder.isMutableRequired = true
                    }
                } else {
                    MediaStore.Images.Media.getBitmap(context.contentResolver, uri)
                }

                val user = userInteractor.uploadPhoto(bitmap)

                withContext(mainDispatcher) {
                    uiState = uiState.copy(user = user)
                }
            } catch (e: Exception) {
                Log.d(TAG, "uploadPhoto: ${e.printStackTrace()}")
            }
        }
    }

    private suspend fun updateUserContacts() {
        val fetchedContacts = userInteractor.getContactsSortedByLastDateAscending()
        val props = meetingsInteractor.getPropertiesForUserMeetings()

        withContext(mainDispatcher) {
            uiState = uiState.copy(
                contacts = fetchedContacts,
                contactsLabels = props.map { it.key to it.value.first }.toMap(),
                contactsLabelColors = props.map { it.key to it.value.second }.toMap(),
            )
        }
    }

    private fun updateUser(user: User) {
        uiState = uiState.copy(user = user)
    }

    private fun handleDelete(contact: User) {
        viewModelScope.launch(bgDispatcher) {
            val user = userInteractor.deleteContact(contact.id)

            withContext(mainDispatcher) {
                uiState = uiState.copy(
                    user = user,
                    contacts = uiState.contacts.minus(contact)
                )
            }
        }
    }

    private fun handleBlock(contact: User) {
        viewModelScope.launch(bgDispatcher) {
            val user = userInteractor.blockContact(contact.id)
            val contacts = userInteractor.getUserContacts()

            withContext(mainDispatcher) {
                uiState = uiState.copy(
                    user = user,
                    contacts = contacts,
                )
            }
        }
    }


    private suspend fun updateTests() {
        val testSets: List<TestSet> = testsInteractor.getUserTestSets()
            .sortedByDescending { it.date }
        val allTests = testsInteractor.getUserTests()

        withContext(mainDispatcher) {
            uiState = uiState.copy(allTests = allTests, testSets = testSets)
        }
    }

    private fun getUserInfo() {
        viewModelScope.launch(bgDispatcher) {
            try {
                val user = userInteractor.getUser()
                updateUser(user)

                updateTests()
                updateUserContacts()

                withContext(mainDispatcher) {
                    uiState = uiState.copy(isLoading = false)
                }
            } catch (e: Exception) {
                Log.d(TAG, "getUserInfo: ${e.localizedMessage}")
            }
        }
    }

    fun changeSort() {
        viewModelScope.launch(bgDispatcher) {
            val fetchedContacts = if (isSortedAscending)
                    userInteractor.getContactsSortedByLastDateDescending()
                else
                    userInteractor.getContactsSortedByLastDateAscending()

            isSortedAscending = !isSortedAscending

            val props = meetingsInteractor.getPropertiesForUserMeetings()

            withContext(mainDispatcher) {
                uiState = uiState.copy(
                    contacts = fetchedContacts,
                    contactsLabels = props.map { it.key to it.value.first }.toMap(),
                    contactsLabelColors = props.map { it.key to it.value.second }.toMap(),
                )
            }
        }

    }

    companion object {
        private const val TAG: String = "MainViewModel"
    }
}

enum class ContactAction(val action: String) {
    SHARE_TEST("Share a test"),
    ADD_DATE("Add a date"),
    DELETE("Delete"),
    BLOCK("Block"),
    REPORT("Report")
}

private val previewPosts = listOf(
    OfflinePost(
        title = "Безопасные советы для быстрых свиданий",
        ruTitle = "Безопасные советы для быстрых свиданий",
        imageURI = R.drawable.img_6188,
    ),
    OfflinePost(
        title = "Безопасные советы для быстрых свиданий",
        ruTitle = "Безопасные советы для быстрых свиданий",
        imageURI = R.drawable.img_0852
    ),
    OfflinePost(
        title = "Безопасные советы для быстрых свиданий",
        ruTitle = "Безопасные советы для быстрых свиданий",
        imageURI = R.drawable.img_6119,
        width = 200,
        height = 200
    ),
    OfflinePost(
        title = "Безопасные советы для быстрых свиданий",
        ruTitle = "Безопасные советы для быстрых свиданий",
        imageURI = R.drawable.img_4297,
        height = 133,
        width = 328,
    ),
    OfflinePost(
        title = "Безопасные советы для быстрых свиданий",
        ruTitle = "Безопасные советы для быстрых свиданий",
        imageURI = R.drawable.img_9205,
    ),
    OfflinePost(
        title = "Безопасные советы для быстрых свиданий",
        ruTitle = "Безопасные советы для быстрых свиданий",
        imageURI = R.drawable.img_6181,
    ),
    OfflinePost(
        title = "Безопасные советы для быстрых свиданий",
        ruTitle = "Безопасные советы для быстрых свиданий",
        imageURI = R.drawable.img_7475,
    ),
)
