package com.makesure.presentation.screens.share

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Parcelable
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.SavedStateHandleSaveableApi
import androidx.lifecycle.viewmodel.compose.saveable
import com.makesure.BuildConfig
import com.makesure.data.dto.user.User
import com.makesure.domain.interactors.UserInteractor
import io.github.g0dkar.qrcode.QRCode
import io.github.g0dkar.qrcode.render.Colors
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.parcelize.Parcelize
import java.io.ByteArrayOutputStream

@OptIn(SavedStateHandleSaveableApi::class)
class ShareViewModel(
    private val userInteractor: UserInteractor,
    private val mainDispatcher: CoroutineDispatcher,
    bgDispatcher: CoroutineDispatcher,
    savedStateHandle: SavedStateHandle,
) : ViewModel() {
    var uiState by savedStateHandle.saveable { mutableStateOf(ShareProfileUiState()) }
        private set

    @Parcelize
    data class ShareProfileUiState(
        val user: User? = null,
        val qrBitmap: Bitmap? = null
    ) : Parcelable

    init {
        viewModelScope.launch(bgDispatcher) {
            val user = userInteractor.getUser()

            val qrContent = "${BuildConfig.DEEPLINK_BASE_URI}/${user.id}"
            withContext(mainDispatcher) {
                uiState = uiState.copy(user = user, qrBitmap = generateQrCode(qrContent))
            }
        }
    }

    fun onShareClicked() {
        // TODO
    }

    fun onQrClicked() {
        // TODO
    }
}

fun generateQrCode(qrContent: String): Bitmap {
    val imageOut = ByteArrayOutputStream()
    QRCode(qrContent)
        .render(
            cellSize = 50,
            brightColor = Colors.withAlpha(Colors.WHITE, 0)
        )
        .writeImage(imageOut)

    val imageBytes = imageOut.toByteArray()

    return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
}
