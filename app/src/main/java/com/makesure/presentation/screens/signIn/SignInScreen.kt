package com.makesure.presentation.screens.signIn

import android.widget.Toast
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.Column
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHostController
import com.makesure.R
import com.makesure.app.theme.MakeSureTheme
import com.makesure.presentation.components.views.userInput.CodeEnterContent
import com.makesure.presentation.components.views.userInput.PhoneEnterContent
import com.makesure.presentation.navigation.flows.CodeUiFlow
import com.makesure.presentation.navigation.flows.PhoneUiFlow
import org.koin.androidx.compose.koinViewModel

@Composable
fun SignInScreen(
    viewModel: SignInViewModel = koinViewModel(),
    navController: NavHostController
) {
    val uiState = viewModel.uiState
    val context = LocalContext.current

    BackHandler(onBack = {
        viewModel.onBackClick(navController)
    })

    val uiFlow = SignInUiFlow(
        phone = PhoneUiFlow(
            updatePhone = viewModel::updatePhoneNumber,
            updateCountryCode = viewModel::updateCountryCode,
            onPhoneEntered = viewModel::phoneEntered
        ),
        code = CodeUiFlow(
            updateCode = viewModel::updateCode,
            onCodeEntered = {
                if (uiState.isCodeCorrect) viewModel.loginUser(navController)
                else Toast
                    .makeText(context, context.getString(R.string.incorrect_code), Toast.LENGTH_SHORT)
                    .show()
            },
            onResendCode = viewModel::sendSmsWithCode
        ),
        onBack = { viewModel.onBackClick(navController) }
    )

    SignInScreenContent(uiState, uiFlow)
}

private data class SignInUiFlow(
    val onBack: () -> Unit = {},
    val code: CodeUiFlow = CodeUiFlow(),
    val phone: PhoneUiFlow = PhoneUiFlow(),
)

@Composable
private fun SignInScreenContent(
    uiState: SignInViewModel.SignInUiState,
    uiFlow: SignInUiFlow = SignInUiFlow()
) {
    Surface {
        Column {
            when (uiState.state) {
                SignInViewModel.SignInState.NUMBER -> {
                    PhoneEnterContent(
                        shouldContinue = uiState.shouldContinue,
                        onBackClick = uiFlow.onBack,
                        updatePhone = uiFlow.phone.updatePhone,
                        updateCountryCode = uiFlow.phone.updateCountryCode,
                        onContinue = uiFlow.phone.onPhoneEntered
                    )
                }
                SignInViewModel.SignInState.CODE -> {
                    CodeEnterContent(
                        number = uiState.fullNumber,
                        code = uiState.code,
                        onContinue = uiFlow.code.onCodeEntered,
                        onBackClick = uiFlow.onBack,
                        updateCode = uiFlow.code.updateCode,
                        onResend = uiFlow.code.onResendCode,
                        shouldContinue = uiState.shouldContinue,
                        isResendEnabled = uiState.isResendEnabled,
                    )
                }
            }
        }
    }
}

@Preview
@Composable
fun SignInScreen_Number_Preview() {
    MakeSureTheme {
        SignInScreenContent(
            uiState = SignInViewModel.SignInUiState(
                state = SignInViewModel.SignInState.NUMBER
            )
        )
    }
}
@Preview
@Composable
fun SignInScreen_Code_Preview() {
    MakeSureTheme {
        SignInScreenContent(
            uiState = SignInViewModel.SignInUiState(
                state = SignInViewModel.SignInState.CODE
            )
        )
    }
}

