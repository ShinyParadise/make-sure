package com.makesure.presentation.screens.welcome.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.makesure.R
import com.makesure.presentation.components.views.buttons.OutlinedButtonWithIcon
import com.makesure.app.theme.MakeSureTheme

@Composable
fun GetStartedButton(
    modifier: Modifier = Modifier,
    onClick: () -> Unit
) {
    Button(
        onClick = onClick,
        shape = MaterialTheme.shapes.medium,
        colors = ButtonDefaults.buttonColors(
            backgroundColor = MaterialTheme.colors.onPrimary
        ),
        modifier = modifier
            .fillMaxWidth()
    ) {
        Text(
            text = stringResource(id = R.string.welcome_get_started),
            color = MaterialTheme.colors.primary,
            fontWeight = FontWeight(1000),
            fontSize = 21.sp,
            modifier = Modifier.padding(vertical = 10.dp)
        )
    }
}

@Composable
fun SignInButton(
    modifier: Modifier = Modifier,
    onClick: () -> Unit
) {
    TextButton(
        onClick = onClick,
        modifier = modifier
            .fillMaxWidth()
    ) {
        Text(
            text = stringResource(id = R.string.welcome_sign_in),
            color = MaterialTheme.colors.onPrimary,
            fontSize = 21.sp,
            fontWeight = FontWeight(900),
            modifier = Modifier.padding(vertical = 10.dp)
        )
    }
}

@Composable
fun SignInWithGoogleButton(
    modifier: Modifier = Modifier,
    onClick: () -> Unit
) {
    OutlinedButtonWithIcon(
        icon = painterResource(id = R.drawable.google_icon),
        text = stringResource(id = R.string.welcome_sign_in_with_google),
        onClick = onClick,
        modifier = modifier
    )
}

@Composable
fun SignInWithNumberButton(
    modifier: Modifier = Modifier,
    onClick: () -> Unit
) {
    OutlinedButtonWithIcon(
        icon = painterResource(id = R.drawable.number_icon),
        text = stringResource(id = R.string.welcome_sign_in_with_number),
        onClick = onClick,
        modifier = modifier
    )
}

@Preview
@Composable
private fun Buttons_Preview() {
    MakeSureTheme {
        Column {
            GetStartedButton {}
            SignInButton {}
            SignInWithGoogleButton {}
            SignInWithNumberButton {}
        }
    }
}
