package com.makesure.presentation.screens.contact

import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Reply
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.makesure.R
import com.makesure.app.theme.MakeSureTheme
import com.makesure.app.theme.PrimaryDarkGradient
import com.makesure.app.theme.mulishFonts
import com.makesure.presentation.components.previewTestSet
import com.makesure.presentation.components.previewUser
import com.makesure.presentation.components.views.CircleClickableUserPhoto
import com.makesure.presentation.components.views.GradientBox
import com.makesure.presentation.components.views.TestSetDetails
import com.makesure.presentation.components.views.buttons.BlockButton
import com.makesure.presentation.components.views.buttons.CloseButton
import com.makesure.presentation.components.views.buttons.FilledButton
import com.makesure.presentation.components.views.calendar.DatePicker
import com.makesure.presentation.navigation.flows.ContactUiFlow
import com.vanpra.composematerialdialogs.rememberMaterialDialogState
import org.koin.androidx.compose.koinViewModel

@Composable
fun ContactProfileScreen(
    viewModel: ContactViewModel = koinViewModel(),
    navController: NavHostController,
) {
    val datePickerState = rememberMaterialDialogState()

    val uiFlow = ContactUiFlow(
        onCloseClick = navController::popBackStack,
        onDateAdd = {
            datePickerState.show()
        },
        onShareLatestTest = viewModel::shareLatestTest,
        onUserBlock = viewModel::blockUser
    )

    ContactProfileScreenContent(uiState = viewModel.uiState, uiFlow = uiFlow)


    DatePicker(
        dateDialogState = datePickerState,
        onDateChange = viewModel::onDateAdd
    )
}

@Composable
private fun ContactProfileScreenContent(
    uiState: ContactViewModel.ContactUiState,
    uiFlow: ContactUiFlow
) {
    val context = LocalContext.current

    GradientBox(
        brush = Brush.linearGradient(PrimaryDarkGradient),
        modifier = Modifier.fillMaxSize()
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    vertical = 10.dp,
                    horizontal = 10.dp
                ),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween,
        ) {
            CloseButton(
                onClick = uiFlow.onCloseClick,
            )
            BlockButton(
                onClick = {
                    uiState.contact?.let {
                        uiFlow.onUserBlock(it)
                        Toast
                            .makeText(context, "${uiState.contact.name} " + R.string.blocked, Toast.LENGTH_SHORT)
                            .show()
                        uiFlow.onCloseClick()
                    }
                }
            )
        }

        Box(
            Modifier
                .fillMaxSize()
                .padding(horizontal = 25.dp, vertical = 20.dp)
        ) {
            ProfileInfo(
                uiState = uiState,
                modifier = Modifier
                    .align(Alignment.TopCenter)
            )

            ButtonsSection(
                uiFlow = uiFlow,
                modifier = Modifier.align(Alignment.BottomCenter)
            )
        }
    }
}

@Composable
private fun ButtonsSection(
    modifier: Modifier = Modifier,
    uiFlow: ContactUiFlow
) {
    Column(
        modifier = modifier.fillMaxWidth()
    ) {
        FilledButton(
            icon = Icons.Default.Reply,
            text = stringResource(R.string.contact_share_test),
            onClick = uiFlow.onShareLatestTest,
        )
        Spacer(Modifier.height(10.dp))

        TextButton(
            onClick = uiFlow.onDateAdd,
            modifier = Modifier.fillMaxWidth()
        ) {
            Icon(
                imageVector = Icons.Default.Add,
                contentDescription = null,
                tint = Color.White,
                modifier = Modifier.size(24.dp)
            )
            Text(
                text = stringResource(R.string.contact_add_date),
                fontSize = 16.sp,
                fontWeight = FontWeight.Bold,
                color = Color.White
            )
        }
    }
}

@Composable
private fun ProfileInfo(
    modifier: Modifier = Modifier,
    uiState: ContactViewModel.ContactUiState
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = modifier
    ) {
        Spacer(modifier = Modifier.height(20.dp))
        UserPhotoAndDate(uiState)
        Spacer(Modifier.height(10.dp))
        Text(
            text = uiState.contact?.name ?: "",
            fontFamily = mulishFonts,
            fontWeight = FontWeight.Bold,
            fontSize = 16.sp,
            color = Color.White
        )

        Spacer(Modifier.height(20.dp))


        uiState.latestTestSet?.let { TestSetDetails(testSet = it) }
    }
}

@Composable
private fun UserPhotoAndDate(uiState: ContactViewModel.ContactUiState) {
    val text = uiState.lastDateLabel
    val color = uiState.lastDateLabelColor

    Box {
        CircleClickableUserPhoto(
            photoURL = uiState.contact?.photoURL,
            sizeDp = 160.dp,
            modifier = Modifier.padding(top = 20.dp)
        )
        if (color != null && text != null) {
            Box(
                modifier = Modifier
                    .offset(x = 110.dp, y = 10.dp)
                    .background(
                        shape = MaterialTheme.shapes.small,
                        color = color
                    )
                    .padding(horizontal = 15.dp, vertical = 4.dp),
                contentAlignment = Alignment.CenterStart
            ) {
                Text(
                    text = text,
                    fontFamily = mulishFonts,
                    fontSize = 14.sp,
                )
            }
        }
    }
}

@Preview(showSystemUi = true)
@Composable
private fun ContactProfileScreen_Preview() {
    MakeSureTheme {
        ContactProfileScreenContent(
            uiState = ContactViewModel.ContactUiState(
                latestTestSet = previewTestSet,
                contact = previewUser,
                lastDateLabel = "123",
                lastDateLabelColor =  Color(0xFFBCBCBC)
            ),
            uiFlow = ContactUiFlow()
        )
    }
}
