package com.makesure.presentation.screens.main.bottomNavScreens.home

import android.net.Uri
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.makesure.R
import com.makesure.app.theme.MakeSureTheme
import com.makesure.app.theme.mulishFonts
import com.makesure.data.dto.post.Post
import com.makesure.domain.entities.OfflinePost
import com.makesure.presentation.components.previewTips
import com.makesure.presentation.components.previewUser
import com.makesure.presentation.components.views.ErrorScreen
import com.makesure.presentation.components.views.ProfileCard
import com.makesure.presentation.components.views.ShimmerItem
import com.makesure.presentation.screens.main.MainViewModel

@Composable
fun HomeScreen(
    modifier: Modifier = Modifier,
    uiState: MainViewModel.MainUiState,
    onSelectPhoto: (Uri) -> Unit,
) {
    if (uiState.hasError) {
        return ErrorScreen()
    }

    Column(
        modifier = modifier
            .padding(top = 16.dp, start = 16.dp, end = 16.dp)
            .verticalScroll(rememberScrollState())
    ) {
        ShimmerItem(
            isLoading = uiState.isLoading,
            height = 140.dp,
            contentAfterLoading = {
                uiState.user?.let {
                    ProfileCard(
                        onSelectPhoto = onSelectPhoto,
                        user = uiState.user,
                        testCount = uiState.allTests.size
                    )
                }
            }
        )
        Spacer(Modifier.height(20.dp))

        TipsTitle()
        Spacer(Modifier.height(10.dp))

        BlogItems(uiState)
    }
}

@Composable
private fun BlogItems(uiState: MainViewModel.MainUiState) {
    val gridGap = 10.dp

    Column {
        Row(
            Modifier
                .fillMaxWidth()
                .height(236.dp)
        ) {
            Column(Modifier.weight(1 / 3f)) {
                OfflinePostItem(
                    post = uiState.offlinePosts[0],
                    modifier = Modifier.weight(1f)
                )
                Spacer(Modifier.height(gridGap))
                OfflinePostItem(
                    post = uiState.offlinePosts[1],
                    modifier = Modifier.weight(1f)
                )
            }
            Spacer(Modifier.width(gridGap))

            OfflinePostItem(
                post = uiState.offlinePosts[2],
                modifier = Modifier.weight(2 / 3f)
            )
        }
        Spacer(Modifier.height(gridGap))

        OfflinePostItem(
            post = uiState.offlinePosts[3],
            modifier = Modifier
                .fillMaxWidth()
                .height(uiState.offlinePosts[3].height.dp)
        )
        Spacer(Modifier.height(gridGap))

        Row(Modifier.height(100.dp)) {
            OfflinePostItem(
                post = uiState.offlinePosts[4],
                modifier = Modifier.weight(1f)
            )
            Spacer(Modifier.width(gridGap))
            OfflinePostItem(
                post = uiState.offlinePosts[5],
                modifier = Modifier.weight(1f)
            )
        }
        Spacer(Modifier.height(gridGap))

        OfflinePostItem(
            post = uiState.offlinePosts[6],
            modifier = Modifier.height(133.dp)
        )
    }
}

@Composable
private fun OfflinePostItem(
    modifier: Modifier = Modifier,
    post: OfflinePost,
) {
    Card(
        shape = MaterialTheme.shapes.small,
        modifier = modifier
            .clip(MaterialTheme.shapes.small)
    ) {
        Box(
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.BottomStart,
        ) {
            Image(
                painter = painterResource(id = post.imageURI),
                contentScale = ContentScale.Crop,
                contentDescription = null,
                modifier = Modifier.fillMaxSize()
            )
            Text(
                text = post.title,
                softWrap = true,
                fontFamily = mulishFonts,
                fontWeight = FontWeight.Medium,
                fontSize = 12.sp,
                color = Color.White,
                modifier = Modifier
                    .padding(8.dp)
            )
        }
    }
}

@Composable
private fun PostItem(
    modifier: Modifier = Modifier,
    post: Post,
    onTipClick: () -> Unit,
) {
    val uriHandler = LocalUriHandler.current

    Card(
        shape = MaterialTheme.shapes.small,
        modifier = modifier
            .fillMaxWidth()
            .clip(MaterialTheme.shapes.small)
            .clickable {
                onTipClick()
                uriHandler.openUri(post.link)
            }
    ) {
        Box(
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.Center,
        ) {
            AsyncImage(
                model = ImageRequest.Builder(LocalContext.current)
                    .data(post.imageURL)
                    .crossfade(true)
                    .build(),
                contentDescription = post.type,
                contentScale = ContentScale.Crop,
                modifier = Modifier.fillMaxSize()
            )
            Text(
                text = post.title,
                softWrap = true,
                fontFamily = mulishFonts,
                fontWeight = FontWeight.Medium,
                fontSize = 32.sp,
                color = Color.White,
                modifier = Modifier
                    .fillMaxSize()
                    .padding(8.dp)
            )
        }
    }
}

@Composable
private fun TipsTitle(
    modifier: Modifier = Modifier
) {
    Text(
        text = stringResource(id = R.string.blog_title),
        fontFamily = mulishFonts,
        fontWeight = FontWeight.Bold,
        fontSize = 25.sp,
        color = MaterialTheme.colors.primary,
        modifier = modifier.fillMaxWidth()
    )
}

@Preview(showSystemUi = true)
@Composable
private fun HomeScreen_Preview() {
    MakeSureTheme {
        HomeScreen(
            uiState = MainViewModel.MainUiState(
                user = previewUser,
                posts = previewTips,
                isLoading = false
            ),
        ) {}
    }
}

@Preview(showSystemUi = true)
@Composable
private fun HomeScreen_Preview_Loading() {
    MakeSureTheme {
        HomeScreen(
            uiState = MainViewModel.MainUiState(
                user = previewUser,
                posts = previewTips,
            ),
        ) {}
    }
}
