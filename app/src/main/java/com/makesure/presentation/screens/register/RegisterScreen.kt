package com.makesure.presentation.screens.register

import android.net.Uri
import android.widget.Toast
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.makesure.R
import com.makesure.app.theme.MakeSureTheme
import com.makesure.data.dto.user.Sex
import com.makesure.presentation.components.views.userInput.AgreementContent
import com.makesure.presentation.components.views.userInput.BirthdayEnterContent
import com.makesure.presentation.components.views.userInput.CodeEnterContent
import com.makesure.presentation.components.views.userInput.EmailEnterContent
import com.makesure.presentation.components.views.userInput.NameEnterContent
import com.makesure.presentation.components.views.userInput.PhoneEnterContent
import com.makesure.presentation.navigation.flows.CodeUiFlow
import com.makesure.presentation.navigation.flows.EmailUiFlow
import com.makesure.presentation.navigation.flows.PhoneUiFlow
import org.koin.androidx.compose.koinViewModel

@Composable
fun RegisterScreen(
    navController: NavHostController,
    viewModel: RegisterViewModel = koinViewModel()
) {
    val uiState = viewModel.uiState
    val context = LocalContext.current

    RegisterScreenContent(
        uiState,
        RegisterUiFlow(
            phone = PhoneUiFlow(
                updatePhone = viewModel::updatePhoneNumber,
                updateCountryCode = viewModel::updateCountryCode,
                onPhoneEntered = viewModel::phoneEntered
            ),
            code = CodeUiFlow(
                updateCode = viewModel::updateCode,
                onCodeEntered = {
                    if (!uiState.isCodeCorrect)
                        Toast.makeText(
                            context,
                            context.getString(R.string.incorrect_code),
                            Toast.LENGTH_SHORT
                        ).show()
                    else
                        viewModel.codeEntered()
                },
                onResendCode = viewModel::sendSmsWithCode
            ),
            email = EmailUiFlow(
                updateEmail = viewModel::updateEmail,
                onEmailEntered = viewModel::emailEntered
            ),
            name = NameUiFlow(
                updateName = viewModel::updateName,
                onNameEntered = viewModel::nameEntered
            ),
            birthday = BirthdayUiFlow(
                updateBirthday = viewModel::updateBirthday,
                onBirthdayEntered = viewModel::birthdayEntered
            ),
            onBack = {
                viewModel.backPressed(navController)
            },
            onAgreementAccept = {
                viewModel.registerNewUser(navController)
            },
            onSkip = viewModel::skipPressed
        )
    )
}

private data class RegisterUiFlow(
    val phone: PhoneUiFlow = PhoneUiFlow(),
    val code: CodeUiFlow = CodeUiFlow(),
    val email: EmailUiFlow = EmailUiFlow(),
    val name: NameUiFlow = NameUiFlow(),
    val birthday: BirthdayUiFlow = BirthdayUiFlow(),
    val sex: SexUiFlow = SexUiFlow(),
    val photo: PhotoUiFlow = PhotoUiFlow(),
    val onAgreementAccept: () -> Unit = {},
    val onRegisterUser: () -> Unit = {},
    val onError: () -> Unit = {},
    val onBack: () -> Unit = {},
    val onSkip: () -> Unit = {},
)

private data class NameUiFlow(
    val updateName: (String) -> Unit = {},
    val onNameEntered: () -> Unit = {},
)

private data class BirthdayUiFlow(
    val updateBirthday: (String) -> Unit = {},
    val onBirthdayEntered: () -> Unit = {},
)

private data class SexUiFlow(
    val onSexChosen: (Sex) -> Unit = {},
)

private data class PhotoUiFlow(
    val onPhotoSelected: () -> Unit = {},
    val uploadPhoto: (Uri?) -> Unit = {},
    val removePhoto: () -> Unit = {},
)

@Composable
private fun RegisterScreenContent(
    uiState: RegisterViewModel.RegisterUiState,
    uiFlow: RegisterUiFlow = RegisterUiFlow()
) {
    Surface {
        Column {
            if (uiState.progress > 0) {
                LinearProgressIndicator(
                    progress = uiState.progress,
                    color = MaterialTheme.colors.primary,
                    modifier = Modifier
                        .fillMaxWidth()
                )
            } else {
                Spacer(Modifier.height(5.dp))
            }

            when (uiState.state) {
                RegisterViewModel.RegisterState.NUMBER -> {
                    PhoneEnterContent(
                        onContinue = uiFlow.phone.onPhoneEntered,
                        onBackClick = uiFlow.onBack,
                        updatePhone = uiFlow.phone.updatePhone,
                        updateCountryCode = uiFlow.phone.updateCountryCode,
                        shouldContinue = uiState.shouldContinue
                    )
                }
                RegisterViewModel.RegisterState.CODE -> {
                    CodeEnterContent(
                        number = uiState.fullNumber,
                        code = uiState.code,
                        onContinue = uiFlow.code.onCodeEntered,
                        onBackClick = uiFlow.onBack,
                        updateCode = uiFlow.code.updateCode,
                        onResend = uiFlow.code.onResendCode,
                        shouldContinue = uiState.shouldContinue,
                        isResendEnabled = uiState.isResendEnabled,
                    )
                }
                RegisterViewModel.RegisterState.EMAIL -> {
                    EmailEnterContent(
                        email = uiState.email,
                        onContinue = uiFlow.email.onEmailEntered,
                        onBackClick = uiFlow.onBack,
                        updateEmail = uiFlow.email.updateEmail,
                        onSkip = uiFlow.onSkip,
                        shouldContinue = uiState.shouldContinue
                    )
                }
                RegisterViewModel.RegisterState.FIRST_NAME -> {
                    NameEnterContent(
                        name = uiState.firstName,
                        onBackClick = uiFlow.onBack,
                        onContinue = uiFlow.name.onNameEntered,
                        onNameChange = uiFlow.name.updateName,
                        shouldContinue = uiState.shouldContinue
                    )
                }
                RegisterViewModel.RegisterState.BIRTHDAY -> {
                    BirthdayEnterContent(
                        birthday = uiState.birthdayString,
                        onBirthdayChange = uiFlow.birthday.updateBirthday,
                        onBackClick = uiFlow.onBack,
                        onContinue = uiFlow.birthday.onBirthdayEntered,
                        shouldContinue = uiState.shouldContinue
                    )
                }
                RegisterViewModel.RegisterState.AGREEMENT -> {
                    AgreementContent(
                        onContinue = uiFlow.onAgreementAccept,
                        onBackClick = uiFlow.onBack
                    )
                }
            }
        }
    }
}

@Preview(showSystemUi = true)
@Composable
private fun RegisterScreen_Number_Preview() {
    MakeSureTheme {
        RegisterScreenContent(
            uiState = RegisterViewModel.RegisterUiState(
                state = RegisterViewModel.RegisterState.NUMBER,
                progress = 0.125f,
                firstName = ""
            )
        )
    }
}

@Preview(showSystemUi = true)
@Composable
private fun RegisterScreen_Code_Preview() {
    MakeSureTheme {
        RegisterScreenContent(
            uiState = RegisterViewModel.RegisterUiState(
                state = RegisterViewModel.RegisterState.CODE,
                progress = 0.25f,
                phoneNumber = "9996915605",
                code = "123456",
                firstName = ""
            )
        )
    }
}

@Preview(showSystemUi = true)
@Composable
private fun RegisterScreen_Email_Preview() {
    MakeSureTheme {
        RegisterScreenContent(
            uiState = RegisterViewModel.RegisterUiState(
                state = RegisterViewModel.RegisterState.EMAIL,
                progress = 0.25f,
                email = "123@gmail.com",
                firstName = ""
            )
        )
    }
}

@Preview(showSystemUi = true)
@Composable
private fun RegisterScreen_Name_Preview() {
    MakeSureTheme {
        RegisterScreenContent(
            uiState = RegisterViewModel.RegisterUiState(
                state = RegisterViewModel.RegisterState.FIRST_NAME,
                progress = 0.6f,
                firstName = "Ivan"
            )
        )
    }
}

@Preview(showSystemUi = true)
@Composable
private fun RegisterScreen_Birthday_Preview() {
    MakeSureTheme {
        RegisterScreenContent(
            uiState = RegisterViewModel.RegisterUiState(
                state = RegisterViewModel.RegisterState.BIRTHDAY,
                progress = 0.7f,
            )
        )
    }
}
