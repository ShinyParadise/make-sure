package com.makesure.presentation.screens.share

import android.graphics.Bitmap
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.makesure.R
import com.makesure.data.dto.user.User
import com.makesure.presentation.components.previewUser
import com.makesure.presentation.components.views.CircleClickableUserPhoto
import com.makesure.app.theme.MakeSureTheme
import com.makesure.app.theme.PrimaryGradient
import org.koin.androidx.compose.koinViewModel

@Composable
fun ShareScreen(
    modifier: Modifier = Modifier,
    viewModel: ShareViewModel = koinViewModel(),
    onCancelClick: () -> Unit,
) {
    val uiState = viewModel.uiState

    ShareProfileContent(
        uiState = uiState,
        onQrClick = viewModel::onQrClicked,
        onShareClick = viewModel::onShareClicked,
        onCancelClick = onCancelClick,
        modifier = modifier
    )
}

@Composable
private fun ShareProfileContent(
    modifier: Modifier = Modifier,
    uiState: ShareViewModel.ShareProfileUiState,
    onQrClick: () -> Unit,
    onShareClick: () -> Unit,
    onCancelClick: () -> Unit,
) {
    Surface(Modifier.fillMaxSize()) {
        Box(
            modifier = modifier
                .background(Brush.linearGradient(PrimaryGradient))
                .padding(horizontal = 28.dp)
        ) {
            Column {
                TopButtons(
                    onShare = onShareClick,
                    onCancel = onCancelClick
                )
                Spacer(Modifier.height(50.dp))

                uiState.qrBitmap?.let {
                    uiState.user?.let { it1 -> QrImage(it1, it, onQrClick) }
                }
            }
        }
    }
}

@Composable
private fun QrImage(
    user: User,
    bitmap: Bitmap,
    onQrClick: () -> Unit
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxWidth()
    ) {
        CircleClickableUserPhoto(
            photoURL = user.photoURL,
            sizeDp = 100.dp,
        )
        Spacer(Modifier.height(10.dp))

        Box(
            modifier = Modifier
                .clip(MaterialTheme.shapes.medium)
                .background(Color.White)
        ) {
            Image(
                bitmap = bitmap.asImageBitmap(),
                contentDescription = null,
                modifier = Modifier
                    .clickable { onQrClick() }
                    .padding(45.dp)
            )

            Text(
                text = user.name,
                fontWeight = FontWeight.SemiBold,
                fontSize = 22.sp,
                modifier = Modifier
                    .align(Alignment.BottomCenter)
                    .padding(5.dp)
            )
        }
    }
}

@Composable
private fun TopButtons(
    modifier: Modifier = Modifier,
    onShare: () -> Unit,
    onCancel: () -> Unit,
) {
    Row(
        modifier = modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        IconButton(onClick = onCancel) {
            Icon(
                imageVector = Icons.Default.Close,
                contentDescription = null,
                tint = Color.White,
                modifier = Modifier.size(32.dp)
            )
        }
        IconButton(onClick = onShare) {
            Icon(
                painter = painterResource(id = R.drawable.ios_share_24),
                contentDescription = null,
                tint = Color.White,
                modifier = Modifier.size(32.dp)
            )
        }
    }
}

@Preview(showSystemUi = true)
@Composable
private fun ShareProfileScreen_Preview() {
    MakeSureTheme {
        ShareProfileContent(
            onQrClick = {},
            onShareClick = {},
            onCancelClick = {},
            uiState = ShareViewModel.ShareProfileUiState(
                qrBitmap = generateQrCode("makesure://open/${previewUser.id}"),
                user = previewUser
            )
        )
    }
}
