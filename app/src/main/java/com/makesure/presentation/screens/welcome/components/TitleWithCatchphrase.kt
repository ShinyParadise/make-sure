package com.makesure.presentation.screens.welcome.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.makesure.R

@Composable
fun MakeSureTitleWithCatchPhrase() {
    Column(Modifier.fillMaxWidth()) {
        Text(
            text = stringResource(id = R.string.welcome_catchphrase),
            color = MaterialTheme.colors.onPrimary,
            textAlign = TextAlign.Right,
            fontSize = 20.sp,
            modifier = Modifier.align(Alignment.End)
        )
        Spacer(Modifier.height(5.dp))

        Image(
            painter = painterResource(id = R.drawable.welcome_title),
            contentScale = ContentScale.FillBounds,
            contentDescription = null,
            modifier = Modifier
                .fillMaxWidth()
                .height(200.dp)
        )
    }
}
