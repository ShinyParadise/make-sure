package com.makesure.presentation.screens.main.bottomNavScreens.scan

import androidx.camera.core.ImageCapture.OnImageCapturedCallback
import androidx.camera.view.LifecycleCameraController
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Button
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Cameraswitch
import androidx.compose.material.icons.filled.PhotoCamera
import androidx.compose.material.icons.filled.QrCodeScanner
import androidx.compose.material.icons.filled.RestartAlt
import androidx.compose.material.icons.filled.Send
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.content.ContextCompat
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.PermissionState
import com.google.accompanist.permissions.isGranted
import com.google.accompanist.permissions.rememberPermissionState
import com.makesure.R
import com.makesure.utils.rotate
import com.makesure.presentation.components.views.camera.CameraView
import com.makesure.presentation.components.views.camera.QrScannerView
import com.makesure.presentation.components.views.buttons.CloseButton
import com.makesure.presentation.navigation.flows.ScanUiFlow
import com.makesure.presentation.screens.main.bottomNavScreens.scan.ScanViewModel.ScanState.*
import com.makesure.app.theme.MakeSureTheme
import com.makesure.app.theme.PrimaryGradient
import org.koin.androidx.compose.koinViewModel

@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun ScanScreen(
    modifier: Modifier = Modifier,
    viewModel: ScanViewModel = koinViewModel(),
    navController: NavHostController,
    onCloseClick: () -> Unit,
) {
    val cameraPermissionState = rememberPermissionState(android.Manifest.permission.CAMERA)

    val uiState by viewModel.uiState.collectAsState()

    val uiFlow = ScanUiFlow(
        onRetry = viewModel::onRetryClicked,
        onSend = viewModel::onSendClicked,
        onResultReceived = {
            viewModel.resultReceived(navController)
        },
        onQrCodeScanned = viewModel::onQrCodeScanned,
        selectQrScanner = viewModel::selectQrScanner,
        selectTestScanner = viewModel::selectTestScanner
    )

    Box(modifier.fillMaxSize()) {
        ButtonsAtTheTop(
            onCloseClick = onCloseClick,
            modifier = Modifier.align(Alignment.TopCenter)
        )

        if (cameraPermissionState.status.isGranted) {
            ScanScreenContent(
                modifier = Modifier
                    .align(Alignment.BottomCenter)
                    .padding(bottom = 30.dp),
                imageCapturedCallback = viewModel.imageCapturedCallback,
                uiState = uiState,
                uiFlow = uiFlow,
                navController = navController
            )
        } else {
            RequestPermissionScreen(cameraPermissionState)
        }
    }
}

@Composable
private fun ScanScreenContent(
    modifier: Modifier = Modifier,
    imageCapturedCallback: OnImageCapturedCallback,
    uiState: ScanViewModel.ScanUiState,
    uiFlow: ScanUiFlow,
    navController: NavHostController
) {
    val context = LocalContext.current
    val lifecycleOwner = LocalLifecycleOwner.current

    when (uiState.state) {
        DEFAULT -> {
            val mainExecutor = ContextCompat.getMainExecutor(context)
            val cameraController = remember { LifecycleCameraController(context) }

            CameraView(
                lifecycleOwner = lifecycleOwner,
                controller = cameraController,
                modifier = Modifier.fillMaxSize()
            )

            Row(modifier = modifier) {
                CaptureButton {
                    cameraController.takePicture(mainExecutor, imageCapturedCallback)
                }
                Spacer(Modifier.width(10.dp))

                SelectQrScannerButton {
                    uiFlow.selectQrScanner()
                }
            }
        }

        QR_SCANNER -> {
            QrScannerView(
                onScanResult = {
                    uiFlow.onQrCodeScanned(it, navController)
                },
                modifier = Modifier.fillMaxSize()
            )

            SelectTestScannerButton(
                modifier = modifier,
                onClick = uiFlow.selectTestScanner
            )
        }

        IMAGE_TAKEN -> { ImagePreview(uiState, uiFlow, modifier) }

        ERROR -> { ScanErrorScreen(onRetry = uiFlow.onRetry) }
        LOADING -> { LoadingScreen() }
        SUCCESS -> uiFlow.onResultReceived()
    }
}

@Composable
private fun SelectTestScannerButton(modifier: Modifier, onClick: () -> Unit) {
    GradientCircleButton(onClick = onClick, modifier = modifier) {
        Icon(
            imageVector = Icons.Default.Cameraswitch,
            tint = MaterialTheme.colors.onPrimary,
            contentDescription = null,
            modifier = Modifier.align(Alignment.Center)
        )
    }
}

@Composable
private fun SelectQrScannerButton(onClick: () -> Unit) {
    GradientCircleButton(onClick = onClick) {
        Icon(
            imageVector = Icons.Default.QrCodeScanner,
            tint = MaterialTheme.colors.onPrimary,
            contentDescription = null,
            modifier = Modifier.align(Alignment.Center)
        )
    }
}


@Composable
private fun ImagePreview(
    uiState: ScanViewModel.ScanUiState,
    uiFlow: ScanUiFlow,
    modifier: Modifier
) {
    uiState.image
        ?.rotate(90f)
        ?.asImageBitmap()
        ?.let {
            Box(Modifier.fillMaxSize()) {
                Image(
                    modifier = Modifier.fillMaxSize(),
                    bitmap = it,
                    contentDescription = null,
                    contentScale = ContentScale.Fit
                )

                Row(modifier = Modifier.align(Alignment.BottomCenter)) {
                    GradientCircleButton(onClick = uiFlow.onSend, modifier = modifier) {
                        Icon(
                            imageVector = Icons.Default.Send,
                            contentDescription = null,
                            tint = Color.White,
                            modifier = Modifier.align(Alignment.Center)
                        )
                    }
                    Spacer(Modifier.width(10.dp))

                    GradientCircleButton(onClick = uiFlow.onRetry,) {
                        Icon(
                            imageVector = Icons.Default.RestartAlt,
                            contentDescription = null,
                            tint = Color.White,
                            modifier = Modifier.align(Alignment.Center)
                        )
                    }
                }
            }
        }
}

@Composable
private fun LoadingScreen() {
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        CircularProgressIndicator(
            strokeWidth = 15.dp,
            modifier = Modifier.size(300.dp)
        )
    }
}

@Composable
fun ScanErrorScreen(
    modifier: Modifier = Modifier,
    onRetry: () -> Unit,
) {
    Box(
        modifier = modifier
            .fillMaxSize()
            .padding(40.dp),
        contentAlignment = Alignment.Center
    ) {
        Text(
            text = stringResource(R.string.scan_error),
            fontSize = 25.sp
        )

        GradientCircleButton(
            onClick = onRetry,
            modifier = Modifier.align(Alignment.BottomCenter)
        ) {
            Icon(
                imageVector = Icons.Default.RestartAlt,
                contentDescription = null,
                tint = Color.White,
                modifier = Modifier.align(Alignment.Center)
            )
        }
    }
}

@Composable
private fun GradientCircleButton(
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
    content: @Composable BoxScope.() -> Unit
) {
    Box(
        modifier = modifier
            .size(60.dp)
            .clip(CircleShape)
            .background(shape = CircleShape, brush = Brush.linearGradient(PrimaryGradient))
            .clickable { onClick() }
    ) {
        content()
    }
}

@OptIn(ExperimentalPermissionsApi::class)
@Composable
private fun RequestPermissionScreen(cameraPermissionState: PermissionState) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 50.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Text(
            text = stringResource(R.string.camera_permission_rationale),
            textAlign = TextAlign.Center
        )

        Button(onClick = { cameraPermissionState.launchPermissionRequest() }) {
            Text(stringResource(R.string.camera_permission_request))
        }
    }
}

@Composable
private fun CaptureButton(
    modifier: Modifier = Modifier,
    onCaptureClick: () -> Unit
) {
    GradientCircleButton(onClick = onCaptureClick, modifier = modifier) {
        Icon(
            imageVector = Icons.Default.PhotoCamera,
            tint = Color.White,
            contentDescription = null,
            modifier = Modifier.align(Alignment.Center)
        )
    }
}

@Composable
private fun ButtonsAtTheTop(
    modifier: Modifier = Modifier,
    onCloseClick: () -> Unit
) {
    Row(
        modifier
            .fillMaxWidth()
            .padding(horizontal = 40.dp)
    ) {
        CloseButton(onClick = onCloseClick)
    }
}

@Preview(showSystemUi = true)
@Composable
private fun ScanScreen_Preview() {
    MakeSureTheme {
        ScanScreenContent(
            imageCapturedCallback = object : OnImageCapturedCallback() {},
            uiState = ScanViewModel.ScanUiState(state = ERROR),
            uiFlow = ScanUiFlow(),
            navController = rememberNavController()
        )
    }
}
