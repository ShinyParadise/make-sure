package com.makesure.presentation.screens.welcome.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.BlendMode
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import com.makesure.R
import com.makesure.app.theme.MakeSureTheme

@Composable
fun WelcomeBackgroundImage() {
    Box(Modifier.fillMaxSize()) {
        Image(
            painter = painterResource(id = R.drawable.welcome_bg),
            contentDescription = null,
            contentScale = ContentScale.FillBounds,
            modifier = Modifier.matchParentSize(),
            colorFilter = ColorFilter.tint(
                Color(0xFF924DFF),
                BlendMode.Multiply
            )
        )
    }
}

@Preview
@Composable
private fun BackgroundImage_Preview() {
    MakeSureTheme {
        WelcomeBackgroundImage()
    }
}
