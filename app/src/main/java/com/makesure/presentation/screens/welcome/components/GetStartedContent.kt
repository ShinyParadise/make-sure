package com.makesure.presentation.screens.welcome.components

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.makesure.R
import com.makesure.presentation.components.views.SmallText
import com.makesure.app.theme.MakeSureTheme

@Composable
fun GetStartedContent(
    modifier: Modifier = Modifier,
    onGetStartedClicked: () -> Unit,
    onSignInClicked: () -> Unit
) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = modifier.fillMaxSize().padding(horizontal = 25.dp)
    ) {
        MakeSureTitleWithCatchPhrase()

        Spacer(Modifier.height(130.dp))

        Column(modifier = Modifier
            .align(Alignment.BottomCenter)
            .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            GetStartedButton(onClick = onGetStartedClicked)

            SignInButton(onClick = onSignInClicked)
            Spacer(Modifier.height(40.dp))

            SmallText(
                text = stringResource(id = R.string.get_started_terms_conditions),
                color = Color.White,
                textAlign = TextAlign.Center
            )
        }
    }
}

@Preview(showBackground = true, backgroundColor = 0xFF000000)
@Composable
private fun GetStartedContent_Preview() {
    MakeSureTheme {
        GetStartedContent(onGetStartedClicked = {}) {}
    }
}

@Preview(showBackground = true, backgroundColor = 0xFF000000, locale = "RU")
@Composable
private fun GetStartedContent_Preview_RU() {
    MakeSureTheme {
        GetStartedContent(onGetStartedClicked = {}) {}
    }
}
