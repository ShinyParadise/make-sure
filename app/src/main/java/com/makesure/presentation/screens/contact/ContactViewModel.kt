package com.makesure.presentation.screens.contact

import android.annotation.SuppressLint
import android.content.Context
import android.os.Parcelable
import android.widget.Toast
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.graphics.Color
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.SavedStateHandleSaveableApi
import androidx.lifecycle.viewmodel.compose.saveable
import com.makesure.R
import com.makesure.data.dto.user.User
import com.makesure.data.serializers.ColorParceler
import com.makesure.domain.entities.TestSet
import com.makesure.domain.interactors.MeetingsInteractor
import com.makesure.domain.interactors.TestsInteractor
import com.makesure.domain.interactors.UserInteractor
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.WriteWith
import java.time.LocalDate

@OptIn(SavedStateHandleSaveableApi::class)
@SuppressLint("StaticFieldLeak")
class ContactViewModel(
    private val userInteractor: UserInteractor,
    private val meetingsInteractor: MeetingsInteractor,
    private val testsInteractor: TestsInteractor,
    private val mainDispatcher: CoroutineDispatcher,
    private val bgDispatcher: CoroutineDispatcher,
    private val context: Context,
    savedStateHandle: SavedStateHandle
) : ViewModel() {
    private val contactId: String = checkNotNull(savedStateHandle["contactId"])

    var uiState by savedStateHandle.saveable { mutableStateOf(ContactUiState()) }
        private set

    @Parcelize
    data class ContactUiState(
        val user: User? = null,
        val contact: User? = null,
        val lastDateLabel: String? = null,
        val lastDateLabelColor: @WriteWith<ColorParceler> Color? = null,
        val latestTestSet: TestSet? = null,
    ) : Parcelable

    init {
        viewModelScope.launch(bgDispatcher) {
            val user = userInteractor.getUser()
            val contact = userInteractor.getById(contactId)!!
            val contactTestSet = testsInteractor.getContactLatestTestSet(contactId)

            val props = meetingsInteractor.getPropertiesForSingleContact(contact)

            withContext(mainDispatcher) {
                uiState = uiState.copy(
                    user = user,
                    latestTestSet = contactTestSet,
                    contact = contact,
                    lastDateLabel = props.first,
                    lastDateLabelColor = props.second
                )
            }
        }
    }

    fun onDateAdd(date: LocalDate) {
        viewModelScope.launch(bgDispatcher) {
            try {
                val now = LocalDate.now()
                if (date > now) return@launch

                meetingsInteractor.addMeeting(date, uiState.contact!!)
            } catch (_: Exception) {
                Toast.makeText(
                    context,
                    context.getString(R.string.meeting_error),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    fun shareLatestTest() {
        // TODO
    }

    fun blockUser(contact: User) {
        viewModelScope.launch(bgDispatcher) {
            val user = userInteractor.blockContact(contact.id)

            withContext(mainDispatcher) {
                uiState = uiState.copy(user = user)
            }
        }
    }
}
