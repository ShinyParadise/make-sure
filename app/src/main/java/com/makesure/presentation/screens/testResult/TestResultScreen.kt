package com.makesure.presentation.screens.testResult

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.CheckCircle
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.makesure.R
import com.makesure.data.dto.user.User
import com.makesure.presentation.components.views.ContactTile
import com.makesure.presentation.components.views.buttons.MainButton
import com.makesure.presentation.navigation.flows.ResultUiFlow
import com.makesure.presentation.screens.testResult.TestResultViewModel.TestResultState.ALERT
import com.makesure.presentation.screens.testResult.TestResultViewModel.TestResultState.GOT_NEGATIVE
import com.makesure.presentation.screens.testResult.TestResultViewModel.TestResultState.GOT_POSITIVE
import com.makesure.presentation.screens.testResult.TestResultViewModel.TestResultState.GO_BACK
import com.makesure.presentation.screens.testResult.TestResultViewModel.TestResultState.NOTIFY
import com.makesure.presentation.screens.testResult.TestResultViewModel.TestResultState.TIPS
import com.makesure.app.theme.MakeSureTheme
import com.makesure.app.theme.mulishFonts
import org.koin.androidx.compose.koinViewModel


@Composable
fun TestResultScreen(
    navController: NavHostController,
    viewModel: TestResultViewModel = koinViewModel()
) {
    val uiState = viewModel.uiState
    val uiFlow = ResultUiFlow(
        onResultReceived = {
            viewModel.onResultReceived(navController)
        },
        onTipsContinue = viewModel::onTipsContinue,
        onSendNotificationsClick = viewModel::onSendNotificationsClick,
        onGoBackToNotifications = {
            viewModel.onBackClick(navController)
        },
        onDismissNotifications = viewModel::onDismissNotifications,
        onAlertReceived = {
            viewModel.onBackClick(navController)
        }
    )

    BackHandler {
        viewModel.onBackClick(navController)
    }

    TestResultScreenContent(uiState = uiState, uiFlow = uiFlow)
}

@Composable
private fun TestResultScreenContent(
    uiState: TestResultViewModel.TestResultUiState,
    uiFlow: ResultUiFlow
) {
    TestResultContainer {
        when (uiState.state) {
            GOT_POSITIVE, GOT_NEGATIVE -> {
                ShowResultScreen(
                    uiState = uiState,
                    uiFlow = uiFlow
                )
            }

            TIPS -> {
                TipsScreen(uiFlow = uiFlow)
            }

            NOTIFY -> {
                NotifyScreen(uiState, uiFlow)
            }
            GO_BACK -> {
                GoBackScreen(uiFlow)
            }
            ALERT -> {
                AlertScreen(uiFlow)
            }
        }
    }
}

@Composable
private fun AlertScreen(uiFlow: ResultUiFlow) {
    Box(Modifier.fillMaxSize()) {
        Text(
            text = stringResource(R.string.result_visit_doctor),
            fontWeight = FontWeight.SemiBold,
            fontSize = 30.sp,
            modifier = Modifier
                .padding(20.dp)
                .align(Alignment.Center)
        )

        MainButton(
            text = stringResource(R.string.got_it),
            onClick = { uiFlow.onAlertReceived() },
            modifier = Modifier
                .padding(10.dp)
                .align(Alignment.BottomCenter)
        )
    }
}

@Composable
private fun GoBackScreen(uiFlow: ResultUiFlow) {
    Box(Modifier.fillMaxSize()) {
        Text(
            text = stringResource(R.string.result_confirm_notifications),
            fontWeight = FontWeight.SemiBold,
            fontSize = 30.sp,
            modifier = Modifier
                .padding(20.dp)
                .align(Alignment.Center)
        )

        Column(
            modifier = Modifier
                .padding(10.dp)
                .align(Alignment.BottomCenter)
        ) {
            MainButton(
                text = stringResource(id = R.string.btn_go_back),
                onClick = { uiFlow.onGoBackToNotifications() }
            )
            Spacer(modifier = Modifier.height(10.dp))

            TextButton(
                onClick = uiFlow.onDismissNotifications,
                shape = MaterialTheme.shapes.medium,
                modifier = Modifier
                    .fillMaxWidth()
                    .height(60.dp)
            ) {
                Text(
                    text = stringResource(id = R.string.btn_continue),
                    fontSize = 21.sp,
                    fontWeight = FontWeight(600)
                )
            }
        }
    }
}

@Composable
private fun NotifyScreen(
    uiState: TestResultViewModel.TestResultUiState,
    uiFlow: ResultUiFlow
) {
    val selectedUsers = remember { mutableListOf<User>() }
    val interactionSource = remember { MutableInteractionSource() }

    Box(Modifier.fillMaxSize()) {
        Column(
            Modifier
                .fillMaxWidth()
                .padding(top = 40.dp)
        ) {
            HighRiskSection(uiState, interactionSource)
            Spacer(Modifier.height(30.dp))

            if (uiState.possibleRiskUsers.isNotEmpty()) {
                PossibleRiskSection(
                    uiState = uiState,
                    interactionSource = interactionSource,
                    onAddUser = { selectedUsers.add(it) }
                ) { selectedUsers.remove(it) }
            }
        }

        MainButton(
            text = stringResource(R.string.test_result_send),
            onClick = {
                uiFlow.onSendNotificationsClick(selectedUsers)
            },
            modifier = Modifier
                .padding(10.dp)
                .align(Alignment.BottomCenter)
        )
    }
}

@Composable
private fun PossibleRiskSection(
    interactionSource: MutableInteractionSource,
    uiState: TestResultViewModel.TestResultUiState,
    onAddUser: (User) -> Unit,
    onRemoveUser: (User) -> Unit,
) {
    Column(
        modifier = Modifier.padding(horizontal = 16.dp)
    ) {
        Text(
            text = stringResource(R.string.result_possible_risk),
            fontFamily = mulishFonts,
            fontWeight = FontWeight.Bold,
            fontSize = 23.sp,
        )
        Text(
            text = stringResource(R.string.result_possible_risk_descritption),
            fontFamily = mulishFonts,
            fontWeight = FontWeight.Normal,
            fontSize = 14.sp,
        )

        Column(modifier = Modifier.fillMaxWidth()) {
            for (user in uiState.possibleRiskUsers) {
                var isSelected by remember { mutableStateOf(false) }

                SelectableUser(
                    modifier = Modifier
                        .fillMaxWidth()
                        .clip(MaterialTheme.shapes.small)
                        .clickable {
                            isSelected = !isSelected
                            if (isSelected) onAddUser(user)
                            else onRemoveUser(user)
                        },
                    user = user,
                    isSelected = isSelected,
                    interactionSource = interactionSource,
                )
            }
        }
    }
}

@Composable
private fun SelectableUser(
    modifier: Modifier = Modifier,
    user: User,
    isSelected: Boolean,
    interactionSource: MutableInteractionSource,
) {
    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically
    ) {
        ContactTile(
            contact = user,
            modifier = Modifier.padding(10.dp),
            interactionSource = interactionSource,
        )
        Spacer(modifier = Modifier.weight(1f))

        if (isSelected) FilledCheckbox()
        else EmptyCheckbox()
    }
}

@Composable
private fun FilledCheckbox() {
    Icon(
        imageVector = Icons.Default.CheckCircle,
        tint = Color(0xFF5FE986),
        contentDescription = null,
        modifier = Modifier.size(24.dp)
    )
}

@Composable
private fun EmptyCheckbox() {
    Box(
        modifier = Modifier
            .size(22.dp)
            .border(
                width = 1.dp,
                color = MaterialTheme.colors.primary,
                shape = CircleShape,
            )
    )
}

@Composable
private fun HighRiskSection(
    uiState: TestResultViewModel.TestResultUiState,
    interactionSource: MutableInteractionSource
) {
    Column(
        modifier = Modifier.padding(horizontal = 16.dp)
    ) {
        Text(
            text = stringResource(R.string.result_high_risk),
            fontFamily = mulishFonts,
            fontWeight = FontWeight.Bold,
            fontSize = 23.sp,
        )
        Text(
            text = stringResource(R.string.result_high_risk_descritption),
            fontFamily = mulishFonts,
            fontWeight = FontWeight.Normal,
            fontSize = 14.sp,
        )
    }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .background(
                shape = MaterialTheme.shapes.medium,
                color = Color(0xFFE6E6E6)
            )
    ) {
        for (user in uiState.highRiskUsers) {
            ContactTile(
                modifier = Modifier.padding(10.dp),
                contact = user,
                interactionSource = interactionSource,
            )
        }
    }
}

@Composable
private fun TipsScreen(
    uiFlow: ResultUiFlow
) {
    Box(Modifier.fillMaxSize()) {
        Text(
            text = stringResource(R.string.test_result_tips_title),
            fontWeight = FontWeight.SemiBold,
            fontSize = 30.sp,
            modifier = Modifier.padding(20.dp)
        )

        MainButton(
            text = stringResource(id = R.string.btn_continue),
            onClick = { uiFlow.onTipsContinue() },
            modifier = Modifier
                .padding(10.dp)
                .align(Alignment.BottomCenter)
        )
    }
}

@Composable
private fun ShowResultScreen(
    modifier: Modifier = Modifier,
    uiState: TestResultViewModel.TestResultUiState,
    uiFlow: ResultUiFlow
) {
    Box(modifier.fillMaxSize()) {
        Text(
            text = uiState.result,
            fontWeight = FontWeight.SemiBold,
            fontSize = 30.sp,
            modifier = Modifier.align(Alignment.Center)
        )

        Column(
            modifier = Modifier
                .padding(10.dp)
                .align(Alignment.BottomCenter)
        ) {
            MainButton(
                text = stringResource(id = R.string.btn_continue),
                onClick = { uiFlow.onResultReceived() }
            )
            Spacer(modifier = Modifier.height(10.dp))

            TextButton(
                onClick = uiFlow.onResultErrorClick,
                border = BorderStroke(2.dp, color = MaterialTheme.colors.primary),
                shape = MaterialTheme.shapes.medium,
                modifier = Modifier
                    .fillMaxWidth()
                    .height(60.dp)
            ) {
                Text(
                    text = stringResource(R.string.title_disagree_with_result),
                    fontSize = 21.sp,
                    fontWeight = FontWeight(600)
                )
            }
        }
    }
}

@Composable
private fun TestResultContainer(
    content: @Composable BoxScope.() -> Unit
) {
    Surface(
        modifier = Modifier.fillMaxSize(),
        color = MaterialTheme.colors.secondary,
        contentColor = Color.Black
    ) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(horizontal = 12.dp, vertical = 20.dp)
                .background(
                    color = MaterialTheme.colors.onSecondary,
                    shape = MaterialTheme.shapes.medium
                )
        ) {
            content()
        }
    }
}

@Preview
@Composable
private fun TestResult_Negative_Preview() {
    MakeSureTheme {
        TestResultScreenContent(
            uiState = TestResultViewModel.TestResultUiState(
                result = "Negative"
            ),
            uiFlow = ResultUiFlow()
        )
    }
}

@Preview
@Composable
private fun TestResult_Tips_Preview() {
    MakeSureTheme {
        TestResultScreenContent(
            uiState = TestResultViewModel.TestResultUiState(
                result = "Negative",
                state = TIPS
            ),
            uiFlow = ResultUiFlow()
        )
    }
}

@Preview
@Composable
private fun TestResult_Notify_Preview() {
    MakeSureTheme {
        TestResultScreenContent(
            uiState = TestResultViewModel.TestResultUiState(
                result = "Negative",
                state = NOTIFY
            ),
            uiFlow = ResultUiFlow()
        )
    }
}

@Preview
@Composable
private fun TestResult_GoBack_Preview() {
    MakeSureTheme {
        TestResultScreenContent(
            uiState = TestResultViewModel.TestResultUiState(
                result = "Negative",
                state = GO_BACK
            ),
            uiFlow = ResultUiFlow()
        )
    }
}

