package com.makesure.presentation.screens.settings

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Switch
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.makesure.R
import com.makesure.app.theme.MakeSureTheme
import com.makesure.app.theme.mulishFonts

private val settingsFontSize = 22.sp

@Composable
fun SettingsButton(
    modifier: Modifier = Modifier,
    subtitleText: String? = null,
    onSubtitleClick: () -> Unit = {},
    content: @Composable () -> Unit = {},
    settingsText: String,
    painter: Painter,
    sizeDp: Dp = 24.dp,
    onClick: () -> Unit,
) {
    val interactionSource = remember { MutableInteractionSource() }
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .fillMaxWidth()
            .clickable(
                interactionSource = interactionSource,
                indication = null
            ) { onClick() }
    ) {
        Icon(
            painter = painter,
            contentDescription = null,
            modifier = Modifier.size(sizeDp),
            tint = Color.White
        )
        Spacer(Modifier.width(18.dp))

        Column {
            Text(
                text = settingsText,
                fontFamily = mulishFonts,
                fontSize = settingsFontSize,
                fontWeight = FontWeight.Normal,
                color = Color.White
            )

            subtitleText?.let {
                Text(
                    text = subtitleText,
                    fontFamily = mulishFonts,
                    fontSize = 12.sp,
                    fontWeight = FontWeight.Normal,
                    color = Color.White.copy(alpha = 0.6f),
                    modifier = Modifier.clickable { onSubtitleClick() }
                )
            }
        }
        Spacer(modifier = Modifier.weight(1f))
        content()
    }
}

@Composable
fun SettingsButton(
    modifier: Modifier = Modifier,
    subtitleText: String? = null,
    onSubtitleClick: () -> Unit = {},
    content: @Composable () -> Unit = {},
    settingsText: String,
    imageVector: ImageVector,
    sizeDp: Dp = 24.dp,
    onClick: () -> Unit,
) {
    val interactionSource = remember { MutableInteractionSource() }
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .fillMaxWidth()
            .clickable(
                interactionSource = interactionSource,
                indication = null
            ) { onClick() }
    ) {
        Icon(
            imageVector = imageVector,
            contentDescription = null,
            modifier = Modifier.size(sizeDp),
            tint = Color.White
        )
        Spacer(Modifier.width(18.dp))

        Column {
            Text(
                text = settingsText,
                fontFamily = mulishFonts,
                fontSize = settingsFontSize,
                fontWeight = FontWeight.Normal,
                color = Color.White
            )

            subtitleText?.let {
                Text(
                    text = subtitleText,
                    fontFamily = mulishFonts,
                    fontSize = 12.sp,
                    fontWeight = FontWeight.Normal,
                    color = Color.White.copy(alpha = 0.6f),
                    modifier = Modifier.clickable { onSubtitleClick() }
                )
            }
        }
        Spacer(modifier = Modifier.weight(1f))
        content()
    }
}

@Composable
fun SettingsButtonWithToggle(
    modifier: Modifier = Modifier,
    subtitleText: String? = null,
    settingsText: String,
    painter: Painter,
    sizeDp: Dp,
    isChecked: Boolean,
    onClick: () -> Unit,
) {
    var checked by remember { mutableStateOf(isChecked) }

    SettingsButton(
        settingsText = settingsText,
        painter = painter,
        sizeDp = sizeDp,
        modifier = modifier,
        subtitleText = subtitleText,
        onClick = {},
        content = {
            Switch(
                checked = checked,
                onCheckedChange = {
                    checked = !checked
                    onClick()
                }
            )
        }
    )
}

@Composable
fun SettingsButtonWithDropdownMenu(
    modifier: Modifier = Modifier,
    subtitleText: String? = null,
    settingsText: String,
    painter: Painter,
    sizeDp: Dp,
    chosenValue: String,
    onValueChoose: () -> Unit = {}
) {
    SettingsButton(
        settingsText = settingsText,
        painter = painter,
        sizeDp = sizeDp,
        modifier = modifier,
        subtitleText = subtitleText,
        onClick = {},
        content = {
            Box(
                contentAlignment = Alignment.Center,
                modifier = Modifier
                    .size(width = 70.dp, height = 26.dp)
                    .background(
                        color = MaterialTheme.colors.primaryVariant,
                        shape = RoundedCornerShape(25.dp)
                    )
                    .clip(RoundedCornerShape(25.dp))
                    .clickable { onValueChoose() },
            ) {
                Text(
                    text = chosenValue,
                    fontFamily = mulishFonts,
                    fontWeight = FontWeight.Bold,
                    fontSize = 16.sp,
                    color = Color.White
                )
            }
        }
    )
}

@Preview
@Composable
private fun SettingsButton_Preview() {
    MakeSureTheme {
        SettingsButton(
            settingsText = "Settings text",
            subtitleText = "subtitle",
            painter = painterResource(id = R.drawable.icon_privacy),
            sizeDp = 21.dp,
            onClick = {}
        )
    }
}

@Preview
@Composable
private fun SettingsButtonWithToggle_Preview() {
    MakeSureTheme {
        SettingsButtonWithToggle(
            settingsText = "Settings text",
            painter = painterResource(id = R.drawable.icon_privacy),
            sizeDp = 21.dp,
            onClick = {},
            isChecked = true
        )
    }
}

@Preview
@Composable
private fun SettingsButtonWithDropdown_Preview() {
    MakeSureTheme {
        SettingsButtonWithDropdownMenu(
            settingsText = "Settings text",
            painter = painterResource(id = R.drawable.icon_privacy),
            sizeDp = 21.dp,
            chosenValue = "EN"
        )
    }
}

