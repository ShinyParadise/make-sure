package com.makesure.presentation.screens.signIn

import android.annotation.SuppressLint
import android.content.Context
import android.os.Parcelable
import android.util.Log
import android.widget.Toast
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.SavedStateHandleSaveableApi
import androidx.lifecycle.viewmodel.compose.saveable
import androidx.navigation.NavHostController
import com.makesure.R
import com.makesure.domain.entities.CountryCode
import com.makesure.domain.interactors.AuthInteractor
import com.makesure.domain.interactors.UserInteractor
import com.makesure.presentation.components.countryCodes
import com.makesure.presentation.navigation.graphs.AuthGraph
import com.makesure.presentation.navigation.graphs.Graphs
import com.makesure.presentation.screens.signIn.SignInViewModel.SignInState.*
import com.makesure.utils.popUpToTop
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.parcelize.Parcelize

@OptIn(SavedStateHandleSaveableApi::class)
@SuppressLint("StaticFieldLeak")
class SignInViewModel(
    private val userInteractor: UserInteractor,
    private val authInteractor: AuthInteractor,
    private val context: Context,
    private val mainDispatcher: CoroutineDispatcher,
    private val bgDispatcher: CoroutineDispatcher,
    savedStateHandle: SavedStateHandle
) : ViewModel() {
    enum class SignInState {
        NUMBER, CODE
    }

    @Parcelize
    data class SignInUiState(
        val state: SignInState = NUMBER,
        val phoneNumber: String = "",
        val countryCode: CountryCode = countryCodes.first(),
        val code: String = "",
        val shouldContinue: Boolean = false,
        val isCodeCorrect: Boolean = false,
        val isResendEnabled: Boolean = true,
    ) : Parcelable {
        val fullNumber: String
            get() = countryCode.code + phoneNumber
    }

    var uiState by savedStateHandle.saveable { mutableStateOf(SignInUiState()) }
        private set

    fun updatePhoneNumber(phoneNumber: String) {
        if (phoneNumber.length > 10) return

        uiState = uiState.copy(
            phoneNumber = phoneNumber,
            shouldContinue = phoneNumber.length == 10
        )
    }

    fun phoneEntered() {
        viewModelScope.launch(bgDispatcher) {
            val user = userInteractor.getByPhone(uiState.fullNumber)

            if (user == null) {
                withContext(mainDispatcher) {
                    Toast.makeText(context, context.getString(R.string.no_user), Toast.LENGTH_SHORT).show()
                    resetUiState()
                }
            } else {
                uiState = uiState.copy(
                    state = CODE,
                    shouldContinue = false,
                )
                sendSmsWithCode()
            }
        }
    }

    fun sendSmsWithCode() {
        viewModelScope.launch(bgDispatcher) {
           authInteractor.sendVerificationCode(uiState.fullNumber)
        }
    }

    fun updateCountryCode(countryCode: CountryCode) {
        uiState = uiState.copy(countryCode = countryCode)
    }

    fun updateCode(code: String) {
        if (code.length > 6) return
        uiState = uiState.copy(
            code = code,
            shouldContinue = code.length == 6,
            isCodeCorrect = checkCode(code)
        )
    }

    fun onBackClick(navController: NavHostController) {
        when (uiState.state) {
            NUMBER -> {
                navController.navigate(AuthGraph.WELCOME) { popUpToTop(navController) }
            }
            CODE -> resetUiState()
        }
    }

    fun loginUser(navController: NavHostController) {
        viewModelScope.launch(bgDispatcher) {
            try {
                val user = userInteractor.getByPhone(uiState.fullNumber)
                    ?: throw Exception("User not found")

                authInteractor.login(user.id)

                navigateToHome(navController)
            } catch (e: Exception) {
                Log.d("SignInViewModel", "loginUser: ${e.localizedMessage}")
            }
        }
    }

    private fun navigateToHome(navController: NavHostController) {
        navController.navigate(Graphs.BOTTOM_NAV_ROUTE) {
            popUpTo(navController.graph.id) { inclusive = true }
            launchSingleTop = true
        }
    }

    private fun checkCode(code: String): Boolean {
        return if (code.isNotBlank()) code.toInt() == authInteractor.randomCode
        else false
    }

    private fun resetUiState() { uiState = SignInUiState() }
}
