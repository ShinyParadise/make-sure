package com.makesure.presentation.screens.welcome

import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import com.makesure.presentation.screens.welcome.components.GetStartedContent
import com.makesure.presentation.screens.welcome.components.WelcomeBackgroundImage
import com.makesure.app.theme.MakeSureTheme

@Composable
fun WelcomeScreen(
    onGetStarted: () -> Unit,
    onSignWithNumberClicked: () -> Unit
) {
    WelcomeScreenContent(
        uiFlow = WelcomeUIFlow(
            onGetStartedClicked = onGetStarted,
            onSignClicked = onSignWithNumberClicked
        )
    )
}

private data class WelcomeUIFlow(
    val onGetStartedClicked: () -> Unit = {},
    val onSignClicked: () -> Unit = {}
)

@Composable
private fun WelcomeScreenContent(uiFlow: WelcomeUIFlow = WelcomeUIFlow()) {
    WelcomeBackgroundImage()

    GetStartedContent(
        onSignInClicked = uiFlow.onSignClicked,
        onGetStartedClicked = uiFlow.onGetStartedClicked
    )
}

@Preview(showSystemUi = true)
@Composable
private fun WelcomeScreen_Preview() {
    MakeSureTheme {
        WelcomeScreenContent()
    }
}
