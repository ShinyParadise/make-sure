package com.makesure.presentation.screens.notifications

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.hapticfeedback.HapticFeedbackType
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalHapticFeedback
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.makesure.R
import com.makesure.app.theme.MakeSureTheme
import com.makesure.app.theme.mulishFonts
import com.makesure.data.dto.notification.Notification
import com.makesure.presentation.components.previewNotifications
import com.makesure.presentation.navigation.flows.NotificationUiFlow
import org.koin.androidx.compose.koinViewModel

@Composable
fun NotificationsScreen(
    modifier: Modifier = Modifier,
    viewModel: NotificationsViewModel = koinViewModel(),
) {
    val uiState = viewModel.uiState
    val uiFlow = NotificationUiFlow(
        onNotificationDelete = viewModel::deleteNotification
    )

    NotificationsScreenContent(uiState, uiFlow, modifier)
}

@Composable
private fun NotificationsScreenContent(
    uiState: NotificationsViewModel.NotificationsUiState,
    uiFlow: NotificationUiFlow,
    modifier: Modifier = Modifier,
) {
    LazyColumn(
        modifier
            .padding(top = 16.dp)
            .padding(horizontal = 17.dp)
    ) {
        items(
            items = uiState.notifications,
            { it.id }
        ) { notification ->
            NotificationItem(notification, uiFlow)
        }
    }
}

@Composable
private fun NotificationsActionsMenu(
    onDelete: () -> Unit,
    isMenuVisible: Boolean,
    onDismissSheet: () -> Unit,
) {
    DropdownMenu(
        expanded = isMenuVisible,
        onDismissRequest = onDismissSheet
    ) {
        DropdownMenuItem(
            onClick = {
                onDelete()
                onDismissSheet()
            }
        ) {
            Column {
                Text(
                    text = stringResource(id = R.string.delete_notification),
                    fontFamily = mulishFonts,
                    fontSize = 14.sp,
                    color = Color(0xFFFF3226)
                )
            }
        }
    }
}

@Composable
private fun NotificationItem(
    item: Notification,
    uiFlow: NotificationUiFlow,
) {
    var isMenuVisible by remember { mutableStateOf(false) }
    val haptics = LocalHapticFeedback.current

    Box(
        Modifier.pointerInput(Unit) {
            detectTapGestures(
                onTap = { uiFlow.onNotificationTap(item) },
                onLongPress = {
                    haptics.performHapticFeedback(HapticFeedbackType.LongPress)
                    isMenuVisible = true
                }
            )
        }
    ) {
        NotificationTile(item)

        NotificationsActionsMenu(
            onDelete = { uiFlow.onNotificationDelete(item) },
            onDismissSheet = { isMenuVisible = false },
            isMenuVisible = isMenuVisible
        )
    }
}

@Composable
private fun NotificationTile(
    item: Notification,
    modifier: Modifier = Modifier
) {
    Row(
        modifier = modifier
            .padding(bottom = 12.dp),
        verticalAlignment = Alignment.Bottom
    ) {
        Image(
            painter = painterResource(id = R.drawable.notification_make_sure_icon),
            contentDescription = null,
            modifier = Modifier.size(41.dp)
        )
        Spacer(modifier = Modifier.width(8.dp))

        item.description?.let {
            Box(
                Modifier
                    .background(
                        color = Color(0xFFF0F0F0),
                        shape = MaterialTheme.shapes.small
                    )
                    .padding(12.dp)
                    .fillMaxWidth()
            ) {
                Text(it, fontFamily = mulishFonts, fontSize = 16.sp)
            }
        }
    }
}

@Preview(showSystemUi = true)
@Composable
private fun NotificationsScreen_Preview() {
    MakeSureTheme {
        NotificationsScreenContent(
            uiState = NotificationsViewModel.NotificationsUiState(
                notifications = previewNotifications
            ),
            uiFlow = NotificationUiFlow()
        )
    }
}

@Preview(locale = "RU", showSystemUi = true)
@Composable
private fun NotificationsScreen_PreviewRU() {
    MakeSureTheme {
        NotificationsScreenContent(
            uiState = NotificationsViewModel.NotificationsUiState(
                notifications = previewNotifications
            ),
            uiFlow = NotificationUiFlow()
        )
    }
}
