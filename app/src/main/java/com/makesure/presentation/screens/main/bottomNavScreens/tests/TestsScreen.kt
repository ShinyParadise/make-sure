package com.makesure.presentation.screens.main.bottomNavScreens.tests

import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.LinearOutSlowInEasing
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.CalendarToday
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.makesure.R
import com.makesure.app.theme.MakeSureTheme
import com.makesure.app.theme.PrimaryDarkGradient
import com.makesure.app.theme.interFonts
import com.makesure.app.theme.mulishFonts
import com.makesure.domain.entities.TestSet
import com.makesure.presentation.components.previewTest
import com.makesure.presentation.components.views.GreenDot
import com.makesure.presentation.components.views.TestSetDetails
import com.makesure.presentation.components.views.Title
import com.makesure.presentation.screens.main.MainViewModel
import com.makesure.utils.getDayMonthAndYear

@Composable
fun TestsScreen(
    modifier: Modifier = Modifier,
    uiState: MainViewModel.MainUiState,
    onCalendarClick: () -> Unit,
) {
    Box(
        modifier = modifier
            .fillMaxSize()
            .background(Brush.linearGradient(PrimaryDarkGradient))
            .padding(top = 26.dp, start = 16.dp, end = 16.dp)
    ) {
        LazyColumn {
            item {
                Box(Modifier.padding(vertical = 15.dp)) {
                    TestsUpperSection(onCalendarClick)
                }
            }

            if (uiState.testSets.isNotEmpty()) {
                itemsIndexed(items = uiState.testSets) { index, tests ->
                    GroupedTestsItem(
                        testSet = tests,
                        index = index + 1,
                        modifier = Modifier.padding(bottom = 15.dp),
                        isFirstGroup = index == 0
                    )
                }
            } else {
                item {
                    Text(
                        text = stringResource(R.string.empty_tests),
                        fontFamily = mulishFonts,
                        fontWeight = FontWeight(600),
                        fontSize = 16.sp,
                        color = Color.White.copy(alpha = 0.52f)
                    )
                }
            }
        }
    }
}

@Composable
fun GroupedTestsItem(
    modifier: Modifier = Modifier,
    testSet: TestSet,
    isFirstGroup: Boolean = false,
    index: Int,
) {
    var isExpanded by rememberSaveable { mutableStateOf(isFirstGroup) }

    Column(
        modifier = modifier
            .animateContentSize(
                animationSpec = tween(
                    durationMillis = 300,
                    easing = LinearOutSlowInEasing
                )
            )
    ) {
        TestCard(
            testSet = testSet,
            isExpanded = isExpanded,
            index = index,
            modifier = Modifier
                .fillMaxWidth()
                .clickable { isExpanded = !isExpanded }
        )

        if (isExpanded) {
            TestSetDetails(
                testSet = testSet,
                modifier = Modifier
                    .padding(horizontal = 14.dp)
            )
        }
    }
}

@Composable
private fun TestCard(
    modifier: Modifier = Modifier,
    testSet: TestSet,
    index: Int,
    isExpanded: Boolean
) {
    Card(
        shape = MaterialTheme.shapes.small,
        backgroundColor = MaterialTheme.colors.onPrimary,
        contentColor = Color(0xFF2C2626),
        elevation = 0.dp,
        modifier = modifier
    ) {
        Box(
            Modifier
                .fillMaxSize()
                .padding(vertical = 15.dp, horizontal = 15.dp)
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                GreenDot(size = 16.dp)
                Spacer(Modifier.width(10.dp))

                Text(
                    text = stringResource(R.string.std_test) + " $index",
                    fontFamily = interFonts,
                    fontSize = 14.sp,
                    fontWeight = FontWeight.Medium
                )
            }

            if (!isExpanded) {
                Text(
                    text = testSet.date.getDayMonthAndYear(),
                    modifier = Modifier.align(Alignment.TopEnd),
                    fontSize = 11.sp,
                    fontFamily = interFonts
                )
            }
        }
    }
}

@Composable
private fun TestsUpperSection(
    onCalendarClick: () -> Unit
) {
    Column {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Title(
                text = stringResource(R.string.tests_title),
                fontSize = 24.sp,
                color = MaterialTheme.colors.onPrimary
            )
            IconButton(onClick = onCalendarClick) {
                Icon(
                    imageVector = Icons.Default.CalendarToday,
                    tint = Color.White,
                    contentDescription = null
                )
            }
        }
    }
}


@Preview(showSystemUi = true)
@Composable
private fun Tests_Preview() {
    MakeSureTheme {
        TestsScreen(
            onCalendarClick = {},
            uiState =
            MainViewModel.MainUiState(
                testSets = listOf(TestSet(tests = listOf(previewTest), date = previewTest.dateTime))
            ),
        )
    }
}

@Preview(showSystemUi = true)
@Composable
private fun TestsEmpty_Preview() {
    MakeSureTheme {
        TestsScreen(
            onCalendarClick = {},
            uiState =
            MainViewModel.MainUiState(
                testSets = listOf()
            ),
        )
    }
}
