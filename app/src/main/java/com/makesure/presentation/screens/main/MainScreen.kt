package com.makesure.presentation.screens.main

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.BottomNavigation
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavDestination
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.makesure.app.theme.MakeSureTheme
import com.makesure.app.theme.homeScreenPrimaryColor
import com.makesure.presentation.components.appbars.HomeTopBar
import com.makesure.presentation.components.views.calendar.CalendarPopup
import com.makesure.presentation.navigation.flows.MainUiFlow
import com.makesure.presentation.navigation.flows.TopBarUiFlow
import com.makesure.presentation.navigation.graphs.MainBottomBar
import com.makesure.presentation.navigation.graphs.MainRoutes
import com.makesure.presentation.navigation.graphs.RootGraph
import org.koin.androidx.compose.koinViewModel

@Composable
fun MainScreen(
    navController: NavHostController = rememberNavController(),
    viewModel: MainViewModel = koinViewModel()
) {
    var openCalendar by remember { mutableStateOf(false) }

    MainScaffoldContent(
        uiState = viewModel.uiState,
        navController = navController,
        topBarUiFlow = TopBarUiFlow(
            onQrClick = { viewModel.navigateToQrScreen(navController) },
            onSettingsClick = { viewModel.navigateToSettings(navController) },
            onBellClick = { viewModel.navigateToNotifications(navController) },
            onEditClick = {},
            onCalendarClick = {
                openCalendar = true
            }
        ),
        mainUiFlow = MainUiFlow(
            onContactActionSelected = viewModel::handleContactAction,
            onContactClick = {
                navController.navigate("${MainRoutes.CONTACT}/${it.id}")
            },
            onCalendarClick = {
                openCalendar = true
            },
            onLogin = viewModel::loginUser,
            onLinkClick = viewModel::getProfileLink,
            onReport = viewModel::sendReport,
            onSelectPhoto = viewModel::uploadPhoto,
            onChangeSort = viewModel::changeSort
        )
    )

    if (openCalendar) {
        CalendarPopup(navController = navController) {
            openCalendar = false
        }
    }
}

@Composable
private fun MainScaffoldContent(
    uiState: MainViewModel.MainUiState,
    topBarUiFlow: TopBarUiFlow,
    mainUiFlow: MainUiFlow,
    navController: NavHostController
) {
    val scaffoldState = rememberScaffoldState()

    Scaffold(
        topBar = {
            HomeTopBar(
                navController = navController,
                uiFlow = topBarUiFlow
            )
        },
        bottomBar = { MainBottomBar(navController = navController) },
        scaffoldState = scaffoldState
    ) {
        Box(Modifier.padding(it)) {
            RootGraph(
                navController = navController,
                uiState = uiState,
                uiFlow = mainUiFlow
            )
        }
    }
}

@Composable
private fun MainBottomBar(navController: NavHostController) {
    val items = listOf(
        MainBottomBar.Home,
        MainBottomBar.Tests,
        MainBottomBar.Scan,
        MainBottomBar.Contacts
    )
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentDestination = navBackStackEntry?.destination

    val shouldShowBottomBar = items.any { it.route == currentDestination?.route }
    if (shouldShowBottomBar) {
        BottomNavigation(
            backgroundColor = MaterialTheme.colors.background,
            contentColor = homeScreenPrimaryColor,
            elevation = 0.dp
        ) {
            items.forEach { screen ->
                AddBottomNavItem(screen, currentDestination, navController)
            }
        }
    }
}

@Composable
private fun RowScope.AddBottomNavItem(
    screen: MainBottomBar,
    currentDestination: NavDestination?,
    navController: NavHostController
) {
    BottomNavigationItem(
        icon = {
            Icon(
                painter = painterResource(id = screen.iconId),
                contentDescription = null,
                modifier = Modifier
                    .defaultMinSize(minHeight = 24.dp, minWidth = 24.dp)
            )
        },
        selected = currentDestination?.hierarchy?.any { it.route == screen.route } == true,
        onClick = {
            navController.navigate(screen.route) {
                popUpTo(navController.graph.findStartDestination().id) {
                    saveState = true
                }
                launchSingleTop = true
                restoreState = true
            }
        }
    )
}


@Preview
@Composable
private fun MainScreen_Preview() {
    MakeSureTheme {
        MainScaffoldContent(
            uiState = MainViewModel.MainUiState(),
            topBarUiFlow = TopBarUiFlow(),
            navController = rememberNavController(),
            mainUiFlow = MainUiFlow()
        )
    }
}
