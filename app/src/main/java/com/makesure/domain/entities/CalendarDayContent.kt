package com.makesure.domain.entities

import android.os.Parcelable
import com.makesure.data.dto.user.User
import kotlinx.parcelize.Parcelize
import java.time.LocalDate

@Parcelize
data class CalendarDayContent(
    val date: LocalDate,
    val testSet: TestSet? = null,
    val testResult: Boolean? = null,
    val meetings: List<User> = emptyList()
) : Parcelable
