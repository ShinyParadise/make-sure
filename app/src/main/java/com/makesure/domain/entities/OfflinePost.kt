package com.makesure.domain.entities

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
data class OfflinePost(
    val title: String,
    val ruTitle: String = "",
    val imageURI: Int,
    val width: Int = 104,
    val height: Int = 104,
) : Parcelable
