package com.makesure.domain.entities

import android.content.Context
import androidx.datastore.preferences.core.edit
import com.makesure.BuildConfig
import com.makesure.data.datastore.DataStoreKeys
import com.makesure.data.datastore.dataStore
import com.makesure.data.dto.user.User
import com.makesure.data.repositories.UserRepository
import com.makesure.domain.interactors.exceptions.NotAuthenticatedException
import com.onesignal.OneSignal
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class UserSession(
    private val userRepository: UserRepository,
    private val bgDispatcher: CoroutineDispatcher,
    private val scope: CoroutineScope,
    private val context: Context,
) {
    init {
        scope.launch(bgDispatcher) {
            context.dataStore.data.collect {
                userId = it[DataStoreKeys.userId]
            }
        }
    }

    suspend fun getUser(): User {
        return fetchUser()
    }

    var userId: String? = null
        set(value) {
            updateId(value)
            field = value
        }

    private fun updateId(userId: String?) {
        scope.launch(bgDispatcher) {
            context.dataStore.edit { it[DataStoreKeys.userId] = userId ?: "" }

            if (userId.isNullOrEmpty())
                OneSignal.logout()
            else {
                loginToOneSignal(userId)
                fetchUser()
            }
        }
    }

    private suspend fun fetchUser(): User {
        val userId = userId ?: throw NotAuthenticatedException()
        return userRepository.getById(userId).getOrThrow()
    }

    private fun loginToOneSignal(userId: String) {
        OneSignal.initWithContext(context, BuildConfig.ONESIGNAL_APP_ID)
        OneSignal.login(userId)
    }
}
