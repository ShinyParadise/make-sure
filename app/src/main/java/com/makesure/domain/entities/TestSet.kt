package com.makesure.domain.entities

import android.os.Parcelable
import com.makesure.data.dto.test.Test
import com.makesure.data.serializers.ZonedDateTimeParceler
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.WriteWith
import java.time.ZonedDateTime

@Parcelize
class TestSet(
    val tests: List<Test>,
    val date: @WriteWith<ZonedDateTimeParceler> ZonedDateTime
) : Parcelable {
    fun isPositive(): Boolean {
        return tests.all { it.result?.lowercase() == "positive" }
    }
}

