package com.makesure.domain.interactors

import com.makesure.BuildConfig
import com.makesure.data.dto.link.Link
import com.makesure.data.dto.link.NewLink
import com.makesure.data.repositories.LinksRepository
import com.makesure.domain.entities.UserSession

class LinksInteractor(
    private val linksRepository: LinksRepository,
    private val userSession: UserSession,
) {
    suspend fun getCurrentUserLink(): String {
        try {
            val user = userSession.getUser()

            val oldLink = linksRepository.getByUserId(user.id).getOrNull()

            if (oldLink != null) {
                removeLink(oldLink)
                return formDeeplink(oldLink)
            }

            val link = linksRepository.insert(NewLink(user.id)).getOrThrow()
            return formDeeplink(link)
        } catch (_: Exception) {
            throw GetLinkException()
        }
    }

    suspend fun getById(inviteId: String): Link {
        try {
            return linksRepository.getById(inviteId).getOrThrow()
        } catch (_: Exception) {
            throw GetLinkException()
        }
    }

    private suspend fun removeLink(link: Link) {
        linksRepository.delete(link)
    }


    private fun formDeeplink(link: Link): String {
        return "${BuildConfig.DEEPLINK_BASE_URI}/link/${link.id}"
    }
}

class GetLinkException(message: String = "There was an error getting link"): Exception(message)
