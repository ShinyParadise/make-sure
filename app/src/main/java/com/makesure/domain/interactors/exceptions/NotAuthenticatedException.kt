package com.makesure.domain.interactors.exceptions

class NotAuthenticatedException(message: String = "You aren't authenticated"): Exception(message)
