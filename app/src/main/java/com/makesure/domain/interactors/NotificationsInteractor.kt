package com.makesure.domain.interactors

import android.util.Log
import com.makesure.data.dto.notification.NewNotification
import com.makesure.data.dto.notification.Notification
import com.makesure.data.repositories.NotificationsRepository
import com.makesure.domain.entities.UserSession
import com.makesure.utils.formattedString
import java.time.ZonedDateTime

class NotificationsInteractor(
    private val notificationsRepository: NotificationsRepository,
    private val userSession: UserSession,
) {
    suspend fun sendNotification(
        title: String = "You may have got a disease",
        description: String = "Please, visit a doctor!"
    ): Notification {
        try {
            val user = userSession.getUser()

            val notification = NewNotification(
                title = title,
                description = description,
                userId = user.id,
                createdAt = ZonedDateTime.now().formattedString()
            )
            return notificationsRepository.insert(notification).getOrThrow()
        } catch (_: Exception) {
            throw SendNotificationException()
        }
    }

    suspend fun getUserNotifications(): List<Notification> {
        try {
            val user = userSession.getUser()
            return notificationsRepository.getByUserId(user.id).getOrThrow()
        } catch (_: Exception) {
            throw GetNotificationException()
        }
    }

    suspend fun delete(notification: Notification) {
        try {
            notificationsRepository.delete(notification)
        } catch (e: Exception) {
            Log.d(TAG, "delete: error deleting notification, reason: ${e.printStackTrace()}")
        }
    }

    companion object {
        const val TAG = "NotificationsInteractor"
    }
}

class SendNotificationException(message: String = "Error sending notification"): Exception(message)
class GetNotificationException(message: String = "Error getting notification"): Exception(message)
