package com.makesure.domain.interactors

import android.content.Context
import android.util.Log
import androidx.compose.ui.graphics.Color
import com.makesure.R
import com.makesure.data.dto.meeting.Meeting
import com.makesure.data.dto.meeting.NewMeeting
import com.makesure.data.dto.user.User
import com.makesure.data.repositories.MeetingsRepository
import com.makesure.data.repositories.UserRepository
import com.makesure.domain.entities.UserSession
import com.makesure.domain.interactors.exceptions.GetUserException
import com.makesure.utils.formattedString
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit

class MeetingsInteractor(
    private val userSession: UserSession,
    private val userRepository: UserRepository,
    private val meetingsRepository: MeetingsRepository,
    private val context: Context,
) {
    suspend fun getAllUserMeetings(): Map<User, List<Meeting>> {
        try {
            val user = getUser()
            val contacts = user.contacts
                ?.mapNotNull { userRepository.getById(it).getOrNull() }
                ?: return emptyMap()
            val meetings = meetingsRepository.getByUserId(user.id).getOrThrow()

            return contacts.associateWith { contact ->
                val contactMeetings = meetings.filter { it.secondUserId == contact.id }
                contactMeetings
            }
        } catch (e: Exception) {
            Log.d(TAG, "getAllUserMeetings: ${e.printStackTrace()}")
            return emptyMap()
        }
    }

    suspend fun getUserMeetingsByDate(): Map<LocalDate, List<User>> {
        try {
            val user = getUser()
            val meetings = meetingsRepository.getByUserId(user.id).getOrThrow()

            val groupedMeetings = meetings.groupBy { it.meetingDate.toLocalDate() }

            return groupedMeetings.map { entry ->
                val contacts = entry.value.map { userRepository.getById(it.secondUserId).getOrThrow() }
                entry.key to contacts
            }.toMap()
        } catch (e: Exception) {
            Log.d(TAG, "getUserMeetingsByDate: ${e.printStackTrace()}")
            return emptyMap()
        }
    }

    suspend fun getPropertiesForUserMeetings(): Map<User, Pair<String?, Color?>> {
        return try {
            val lastMeetings = getLastUserMeetings()
            lastMeetings
                .map {
                    val label = getLabelForDate(it.value)
                    val color = getColorForDate(it.value)
                    it.key to Pair(label, color)
                }
                .toMap()
        } catch (e: Exception) {
            Log.d(TAG, "getPropertiesForUserMeetings: ${e.printStackTrace()}")
            emptyMap()
        }
    }

    suspend fun addMeeting(date: LocalDate, contact: User) {
        try {
            val user = getUser()

            val localDateTime = LocalDateTime.of(date, LocalTime.now())
            val zonedDateTime = ZonedDateTime.of(localDateTime, ZoneId.systemDefault())

            val meeting = NewMeeting(
                userId = user.id,
                meetingDate = zonedDateTime.formattedString(),
                secondUserId = contact.id,
            )

            meetingsRepository.insert(meeting).getOrThrow()
        } catch (e: Exception) {
            Log.d(TAG, "addMeeting: ${e.printStackTrace()}")
        }
    }

    suspend fun getPropertiesForSingleContact(contact: User): Pair<String?, Color?> {
        return try {
            val lastMeetings = getLastUserMeetings()
            val meeting = lastMeetings[contact]!!

            val label = getLabelForDate(meeting)
            val color = getColorForDate(meeting)
            return label to color
        } catch (e: Exception) {
            Log.d(TAG, "getPropertiesForUserMeetings: ${e.printStackTrace()}")
            null to null
        }
    }

    private suspend fun getLastUserMeetings(): Map<User, Meeting> {
        try {
            val user = getUser()
            val contacts = user.contacts
                ?.mapNotNull { userRepository.getById(it).getOrNull() }
                ?: return emptyMap()
            val meetings = meetingsRepository.getByUserId(user.id).getOrThrow()

            return contacts.associateWith { contact ->
                val contactMeetings = meetings.filter { it.secondUserId == contact.id }
                contactMeetings.maxBy { it.meetingDate }
            }
        } catch (e: Exception) {
            Log.d(TAG, "getLastUserMeetings: ${e.printStackTrace()}")
            return emptyMap()
        }
    }

    private suspend fun getUser(): User {
        try {
            return userSession.getUser()
        } catch (_: Exception) {
            throw GetUserException()
        }
    }

    private fun getColorForDate(date: Meeting): Color? {
        return try {
            val now = ZonedDateTime.now()
            val meetingDate = date.meetingDate

            when (ChronoUnit.DAYS.between(now, meetingDate).toInt()) {
                in 0..14 -> Color(0xFFFF409C)
                in 15..30 -> Color(0xFFF7D5FF)
                in 31..179 -> Color(0xFFCCC7FF)
                else -> Color(0xFFB5E4FF)
            }
        } catch (e: Exception) {
            Log.d(TAG, "getColorForDate: ${e.localizedMessage}")
            null
        }
    }

    private fun getLabelForDate(date: Meeting): String? {
        return try {
            val now = ZonedDateTime.now()
            val meetingDate = date.meetingDate
            val difDays = ChronoUnit.DAYS.between(now, meetingDate).toInt()

            if (difDays < 1)
                context.getString(R.string.meeting_today)
            else if (difDays == 1)
                context.getString(R.string.meeting_yesterday)
            else if (difDays in 2..6 || difDays in 8..13)
                difDays.toString() + context.getString(R.string.meeting_days_ago)
            else if (difDays == 7)
                context.getString(R.string.meeting_week_ago)
            else if (difDays == 14)
                context.getString(R.string.meeting_two_weeks_ago)
            else if (difDays in 15..29)
                context.getString(R.string.meeting_this_month)
            else if (difDays == 30)
                context.getString(R.string.meeting_month_ago)
            else if (difDays in 31..179)
                context.getString(R.string.meeting_less_than_3_months)
            else if (difDays == 180)
                context.getString(R.string.meeting_three_months_ago)
            else {
                context.getString(R.string.meeting_more_than_3_months_ago)
            }
        } catch (e: Exception) {
            Log.d(TAG, "getLabelForDate: ${e.localizedMessage}")
            null
        }
    }

    companion object {
        private const val TAG = "MeetingsInteractor"
    }
}
