package com.makesure.domain.interactors

import android.graphics.Bitmap
import android.util.Log
import com.makesure.data.dto.user.User
import com.makesure.data.repositories.ImageRepository
import com.makesure.data.repositories.MeetingsRepository
import com.makesure.data.repositories.UserRepository
import com.makesure.domain.entities.UserSession
import com.makesure.domain.interactors.exceptions.GetUserException
import com.makesure.utils.hashString
import io.github.jan.supabase.postgrest.query.Order
import java.io.ByteArrayOutputStream


class UserInteractor(
    private val userSession: UserSession,
    private val userRepository: UserRepository,
    private val imageRepository: ImageRepository,
    private val meetingsRepository: MeetingsRepository,
) {
    suspend fun getUser(): User {
        try {
            return userSession.getUser()
        } catch (_: Exception) {
            throw GetUserException()
        }
    }

    suspend fun updateEmail(email: String): User? {
        if (email.isEmpty()) return null

        try {
            var user = getUser()
            user = user.copy(email = email)

            return userRepository.update(user).getOrThrow()
        } catch (e: Exception) {
            Log.d(TAG, "updateEmail: ${e.printStackTrace()}")
            throw UserUpdateException()
        }
    }

    suspend fun updatePhone(phone: String): User? {
        if (phone.isEmpty()) return null

        try {
            var user = getUser()
            user = user.copy(phone = phone)

            return userRepository.update(user).getOrThrow()
        } catch (e: Exception) {
            Log.d(TAG, "updatePhone: ${e.printStackTrace()}")
            throw UserUpdateException()
        }
    }

    suspend fun blockContact(contactId: String): User {
        try {
            val user = getUser()

            val blockedUsers = user.blockedUsers ?: emptyList()
            val contacts = user.contacts ?: emptyList()

            val updatedUser = user.copy(
                blockedUsers = blockedUsers.plus(contactId),
                contacts = contacts.minus(contactId)
            )

            return userRepository.update(updatedUser).getOrThrow()
        } catch (_: Exception) {
            throw BlockContactException()
        }
    }

    suspend fun getByPhone(phoneNumber: String): User? {
        return userRepository.getByPhone(phoneNumber).getOrNull()
    }

    suspend fun getById(userId: String): User? {
        return userRepository.getById(userId).getOrNull()
    }

    suspend fun unblockContact(contactId: String): User {
        try {
            val user = getUser()
            val blockedUsers = user.blockedUsers ?: emptyList()
            val contacts = user.contacts ?: emptyList()

            val updatedUser = user.copy(
                blockedUsers = blockedUsers.minus(contactId),
                contacts = contacts.plus(contactId)
            )

            return userRepository.update(updatedUser).getOrThrow()
        } catch (_: Exception) {
            throw BlockContactException()
        }
    }

    suspend fun addContacts(vararg contactId: String): User {
        try {
            val user = getUser()
            val contacts = user.contacts ?: emptyList()

            val updatedUser = user.copy(contacts = contacts.plus(contactId))

            return userRepository.update(updatedUser).getOrThrow()
        } catch (_: Exception) {
            throw AddContactException()
        }
    }

    suspend fun deleteContact(contactId: String): User {
        try {
            val user = getUser()

            val blockedUsers = user.blockedUsers ?: emptyList()
            val contacts = user.contacts ?: emptyList()

            val updatedUser = user.copy(
                blockedUsers = blockedUsers.minus(contactId),
                contacts = contacts.minus(contactId)
            )

            return userRepository.update(updatedUser).getOrThrow()
        } catch (_: Exception) {
            throw BlockContactException()
        }
    }

    suspend fun getUserContacts(): List<User> {
        try {
            val user = getUser()

            val contactIds = user.contacts
            if (contactIds.isNullOrEmpty()) return emptyList()

            return contactIds.map {
                userRepository.getById(it).getOrThrow()
            }
        } catch (_: Exception) {
            throw GetUserException("Couldn't get user contacts")
        }
    }

    // TODO: Test sorting
    suspend fun getContactsSortedByLastDateAscending(): List<User> {
        try {
            return getSortedContacts(Order.ASCENDING)
        } catch (e: Exception) {
            Log.d(TAG, "getContactsSortedByLastDateAscending: ${e.printStackTrace()}")
            throw GetUserException("Couldn't get contacts")
        }
    }

    suspend fun getContactsSortedByLastDateDescending(): List<User> {
        try {
            return getSortedContacts(Order.DESCENDING)
        } catch (e: Exception) {
            Log.d(TAG, "getContactsSortedByLastDateDescending: ${e.printStackTrace()}")
            throw GetUserException("Couldn't get contacts")
        }
    }

    suspend fun getBlockedContacts(): List<User> {
        try {
            val user = getUser()

            val contactIds = user.blockedUsers
            if (contactIds.isNullOrEmpty()) return emptyList()

            return contactIds.map {
                userRepository.getById(it).getOrThrow()
            }
        } catch (_: Exception) {
            throw GetUserException("Couldn't get user blocked contacts")
        }
    }

    suspend fun uploadPhoto(bitmap: Bitmap): User {
        try {
            var user = getUser()

            val byteArrayBitmapStream = ByteArrayOutputStream()
            bitmap.compress(
                Bitmap.CompressFormat.JPEG,
                COMPRESSION_QUALITY,
                byteArrayBitmapStream
            )

            val path = imageRepository.uploadImage(
                bitmap.hashString() + ".jpeg",
                byteArrayBitmapStream.toByteArray()
            ).getOrThrow()

            val url = imageRepository.getPublicUrl(path.removePrefix("user_images/")).getOrThrow()
            user = user.copy(photoURL = url)

            return userRepository.update(user).getOrThrow()
        } catch (e: Exception) {
            Log.d(TAG, "uploadPhoto: ${e.printStackTrace()}")
            throw UploadPhotoException()
        }
    }

    suspend fun deleteProfile() {
        try {
            val user = getUser()
            userRepository.delete(user)
        } catch (e: Exception) {
            Log.d(TAG, "deleteProfile: ${e.printStackTrace()}")
        }
    }

    private suspend fun getSortedContacts(order: Order): List<User> {
        try {
            val user = getUser()

            val contactIds = user.contacts
            if (contactIds.isNullOrEmpty()) return emptyList()

            val contacts = contactIds.map {
                userRepository.getById(it).getOrThrow()
            }

            val meetings = meetingsRepository.getByUserId(user.id, order).getOrThrow()
            val contactMeeting = contacts
                .map { contact ->
                    val lastMeeting =  meetings
                        .filter { it.secondUserId == contact.id }
                        .maxByOrNull { it.meetingDate }
                    contact to lastMeeting
                }
                .sortedBy { it.second?.meetingDate }
                .map { it.first }
            return contactMeeting
        } catch (_: Exception) {
            throw GetUserException("Couldn't get user contacts")
        }
    }

    companion object {
        private const val TAG = "UserInteractor"
        private const val COMPRESSION_QUALITY = 80
    }
}

class BlockContactException(message: String = "There was an error blocking user") : Exception(message)
class AddContactException(message: String = "There was an error adding user") : Exception(message)
class UploadPhotoException(message: String = "There was an error uploading photo") : Exception(message)
class UserUpdateException(message: String = "There was an error updating user") : Exception(message)
