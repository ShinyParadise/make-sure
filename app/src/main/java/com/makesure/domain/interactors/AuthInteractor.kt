package com.makesure.domain.interactors

import android.util.Log
import com.makesure.data.dto.user.NewUser
import com.makesure.data.dto.user.User
import com.makesure.data.gateways.SmsGateway
import com.makesure.data.repositories.UserRepository
import com.makesure.domain.entities.UserSession
import kotlin.random.Random

class AuthInteractor(
    private val userSession: UserSession,
    private val userRepository: UserRepository,
    private val smsGateway: SmsGateway,
) {
    var randomCode: Int = 0
        private set

    suspend fun login(userId: String): User {
        try {
            userSession.userId = userId
            return userSession.getUser()
        } catch (e: Exception) {
            throw LoginException()
        }
    }

    suspend fun register(newUser: NewUser): User {
        try {
            val user = userRepository.insert(newUser).getOrThrow()

            userSession.userId = user.id

            return user
        } catch (e: Exception) {
            Log.d(TAG, "register: ${e.printStackTrace()}")
            throw RegisterException()
        }
    }

    suspend fun sendVerificationCode(phone: String) {
        randomCode = Random.nextInt(from = 100000, until = 999999)
        println(randomCode)
        smsGateway.sendSms("Your code is: $randomCode", phone)
    }

    fun logout() {
        userSession.userId = null
    }

    companion object {
        private const val TAG = "AuthInteractor"
    }
}

class LoginException(message: String = "Couldn't log you in, try again"): Exception(message)
class RegisterException(message: String = "Couldn't create account, try again"): Exception(message)
