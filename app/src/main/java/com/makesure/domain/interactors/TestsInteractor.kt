package com.makesure.domain.interactors

import android.util.Log
import com.makesure.data.dto.test.Test
import com.makesure.data.repositories.TestRepository
import com.makesure.domain.entities.TestSet
import com.makesure.domain.entities.UserSession

class TestsInteractor(
    private val testRepository: TestRepository,
    private val userSession: UserSession,
) {
    suspend fun getUserTestSets(): List<TestSet> {
        try {
            val user = userSession.getUser()
            return testRepository.getByUserId(user.id)
                .getOrThrow()
                .toTestSets()
        } catch (_: Exception) {
            throw GetTestsException()
        }
    }

    suspend fun getUserTests(): List<Test> {
        try {
            val user = userSession.getUser()
            return testRepository.getByUserId(user.id).getOrThrow()
        } catch (_: Exception) {
            throw GetTestsException()
        }
    }

    suspend fun getContactLatestTestSet(contactId: String): TestSet? {
        return testRepository.getByUserId(contactId)
            .getOrElse {
                Log.d(TAG, "getContactLatestTestSet: ${it.localizedMessage}")
                emptyList()
            }
            .toTestSets()
            .maxByOrNull { it.date }
    }

    companion object {
        private const val TAG: String = "TestsInteractor"
    }
}

private fun List<Test>.toTestSets(): List<TestSet> {
    return this
        .groupBy { it.dateTime.toLocalDate() }
        .map {
            TestSet(
                tests = it.value,
                date = it.value.first().dateTime
            )
        }
}

class GetTestsException(message: String = "There was an error fetching tests"): Exception(message)
