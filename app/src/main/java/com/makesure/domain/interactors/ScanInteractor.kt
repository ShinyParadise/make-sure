package com.makesure.domain.interactors

import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import com.makesure.data.gateways.ResultsDetectionBody
import com.makesure.data.gateways.ScanGateway
import com.makesure.utils.rotate
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File
import java.io.FileOutputStream

class ScanInteractor(
    private val scanGateway: ScanGateway,
    private val context: Context,
    private val scope: CoroutineScope,
    private val bgDispatcher: CoroutineDispatcher,
) {
    suspend fun getScanResult(scan: Bitmap): ResultsDetectionBody {
        try {
            val file = createScanFile(scan)

            val imageBody = file.asRequestBody(contentType = "image/jpg".toMediaType())
            val formData = MultipartBody.Part.createFormData("img", file.name, imageBody)

            return scanGateway.testResultsDetection(image = formData)
        } catch (e: Exception) {
            Log.d(TAG, "getScanResult: ${e.printStackTrace()}")
            throw ScanException("Error while receiving result")
        }
    }

    fun cleanup() {
        scope.launch(bgDispatcher) {
            try {
                context.cacheDir.delete()
            } catch (e: Exception) {
                Log.d(TAG, "cleanup: ${e.printStackTrace()}")
            }
        }
    }

    private fun createScanFile(bitmap: Bitmap): File {
        val cacheDir = context.cacheDir
        val file = File(cacheDir, FILE_NAME)

        val out = FileOutputStream(file)
        val rotatedBitmap = bitmap.rotate(90f)
        if (rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, IMG_QUALITY, out)) {
            out.flush()
            out.close()
        }

        return file
    }


    companion object {
        private const val TAG = "ScanInteractor"
        private const val FILE_NAME = "img.jpg"
        const val IMG_QUALITY = 90
    }
}

class ScanException(message: String) : Exception(message)
