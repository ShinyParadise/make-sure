package com.makesure.domain.interactors.exceptions


class GetUserException(message: String = "There was an error fetching user"): Exception(message)
