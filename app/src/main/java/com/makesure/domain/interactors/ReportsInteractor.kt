package com.makesure.domain.interactors

import android.util.Log
import com.makesure.data.dto.report.NewReport
import com.makesure.data.repositories.ReportsRepository
import com.makesure.domain.entities.UserSession

class ReportsInteractor(
    private val userSession: UserSession,
    private val reportsRepository: ReportsRepository,
) {
    suspend fun reportUser(userId: String, text: String) {
        try {
            val currentUser = userSession.getUser()
            reportsRepository.insert(NewReport(currentUser.id, text, userId))
        } catch (e: Exception) {
            Log.d(TAG, "reportUser: couldn't send report. Reason: ${e.printStackTrace()}")
        }
    }

    companion object {
        const val TAG = "ReportsInteractor"
    }
}
